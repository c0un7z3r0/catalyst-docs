import * as types from './actionTypes'

export function updateIntersectionData(payload) {
  return {
    type: types.UPDATE_INTERSECTION_DATA,
    payload,
  }
}

export function flushIntersectionData() {
  return {
    type: types.FLUSH_INTERSECTION_DATA,
  }
}

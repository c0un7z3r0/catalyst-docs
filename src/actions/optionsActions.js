import * as types from './actionTypes'

export function selectOs(selectedOS) {
  return {
    type: types.SELECT_OS,
    selectedOS: selectedOS,
  }
}

import { hot } from 'react-hot-loader/root'
import React, { Suspense, lazy } from 'react'
import { Switch, Route } from 'react-router-dom'

import RootView from './components/rootView/rootView'

const Home = lazy(() => import(/* webpackChunkName: "Home"*/ './components/home/home'))
const QuickStart = lazy(() =>
  import(/* webpackChunkName: "QuickStart"*/ './components/quickStart/quickStart')
)
const Features = lazy(() =>
  import(/* webpackChunkName: "Features"*/ './components/features/features')
)
const Tutorial1 = lazy(() =>
  import(/* webpackChunkName: "Tutorial1"*/ './components/tutorial/tutorial1')
)
const Tutorial2 = lazy(() =>
  import(/* webpackChunkName: "Tutorial2"*/ './components/tutorial/tutorial2')
)
const Dependencies = lazy(() =>
  import(/* webpackChunkName: "Dependencies"*/ './components/dependencies/dependencies')
)
const Colours = lazy(() =>
  import(/* webpackChunkName: "Colours"*/ './components/common/colours/coloursView')
)
import PageLoadingSpinner from './components/rootView/pageLoadSpinner/pageLoadSpinner'
import FourOhFour from './components/rootView/fourOhFour/404'

import './public/css/normalize.css'
import './public/css/scrollbars.scss'
import './public/css/typography.scss'
import './global.scss'
import GoogleAnalytics from './components/common/analytics/analytics'

/**
 * The Application Root Component
 *
 * @component
 * @example
 * return (
 *  <App/>
 * )
 */

const App = () => {
  return (
    <RootView>
      <GoogleAnalytics measurementId={'G-7FHS03RG5J'} />
      <Suspense fallback={<PageLoadingSpinner />}>
        <Switch>
          <Route exact path="/" component={(props) => <Home {...props} />} />
          <Route exact path="/QuickStart" component={(props) => <QuickStart {...props} />} />
          <Route exact path="/Features" component={(props) => <Features {...props} />} />
          <Route exact path="/TutorialPart1" component={(props) => <Tutorial1 {...props} />} />
          <Route exact path="/TutorialPart2" component={(props) => <Tutorial2 {...props} />} />
          <Route exact path="/Dependencies" component={(props) => <Dependencies {...props} />} />
          <Route exact path="/Colours" component={(props) => <Colours {...props} />} />
          <Route exact path="*" component={(props) => <FourOhFour {...props} />} />
        </Switch>
      </Suspense>
    </RootView>
  )
}

export default hot(App)

import React, { useRef } from 'react'
import IntersectionWrapper from '../common/intersectionWrapper/intersectionWrapper'
import ScrollIntoView from '../common/scrollIntoView/scrollIntoView'
import styles from './home.module'
import PageHeadingSplash from '../common/pageHeadingSplash/pageHeadingSplash'
// import CodeSplashImage from '../../public/images/splash/code.jpg'
import CodeSplashImageXS from '../../public/images/splash/_codeXS_400.jpg'
import CodeSplashImageSM from '../../public/images/splash/_codeSM_600.jpg'
import CodeSplashImageMD from '../../public/images/splash/_codeMD_900.jpg'
import CodeSplashImageLG from '../../public/images/splash/_codeLG_1200.jpg'
import CodeSplashImageXL from '../../public/images/splash/_codeXL_1920.jpg'

import SectionHeading from '../common/sectionHeading/sectionHeading'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import ReactIcon from './Icons/reactIcon'
import HtmlIcon from './Icons/htmlIcon'
import ScssIcon from './Icons/scssIcon'
import WebpackIcon from './Icons/webpackIcon'
import JavascriptIcon from './Icons/javascriptIcon'
import CssIcon from './Icons/cssIcon'

import { isScreenLargerThan, isScreenSmallerThan } from '../../utilities/utilities'
/**
 * A Content component for the 'Home' Page
 *
 * @component
 * @example
 * return (
 *  <Home />
 * )
 */

const Home = () => {
  const imageSet = {
    xs: { url: CodeSplashImageXS },
    sm: { url: CodeSplashImageSM },
    md: { url: CodeSplashImageMD },
    lg: { url: CodeSplashImageLG },
    xl: { url: CodeSplashImageXL },
    default: { url: CodeSplashImageMD },
  }

  return (
    <>
      <PageHeadingSplash imageSet={imageSet}>
        <h1>
          <em>
            <sup>Introduction to</sup> <strong>React Catalyst</strong> <sup>v2.0.11</sup>
          </em>
        </h1>
      </PageHeadingSplash>
      <div className={styles.contentContainer}>
        {isScreenLargerThan('md') && (
          <FlexContainer
            justifyContent="center"
            alignItems="center"
            alignContent="center"
            wrap="wrap"
            style={{ width: '400px', float: 'right' }}
          >
            <FlexCell xs={6} sm={6} md={6} lg={6} xl={6} p={6}>
              <HtmlIcon />
            </FlexCell>
            <FlexCell xs={6} sm={6} md={6} lg={6} xl={6} p={6}>
              <CssIcon />
            </FlexCell>
            <FlexCell xs={6} sm={6} md={6} lg={6} xl={6} p={6}>
              <ScssIcon />
            </FlexCell>
            <FlexCell xs={6} sm={6} md={6} lg={6} xl={6} p={6}>
              <WebpackIcon />
            </FlexCell>
            <FlexCell xs={6} sm={6} md={6} lg={6} xl={6} p={6}>
              <JavascriptIcon />
            </FlexCell>
            <FlexCell xs={6} sm={6} md={6} lg={6} xl={6} p={6}>
              <ReactIcon />
            </FlexCell>
          </FlexContainer>
        )}
        <ScrollIntoView>
          <IntersectionWrapper id="Purpose">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/#Purpose"
            >
              Purpose
            </SectionHeading>
            <p>
              React Catalyst is a web application development environment designed to enable the
              rapid production and optimised bundling of React.js web applications. It utilises
              Webpack to package modern ES6 Javascript into minified browser-compliant ES5 code for
              maximum compatibility and the fastest possible time-to-render.
            </p>
            <p>
              This documentation will provide details of the usage of a broad range of the features
              provided by this development environment.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="SupportedTools">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/#SupportedTools"
            >
              Supported Tools
            </SectionHeading>
            <p>
              React Catalyst is specifically designed for web applications and despite being
              entirely customisable it is opinionated about the commonly used tools that come
              pre-packaged in order to best provide a rapid development process.
            </p>
            <p>
              It has out-of-the-box support for a huge range of development tools and libraries
              including React, Redux, ReactRouter, CSS Modules, Autoprefixer, SCSS, Layout,
              Animation, Typography, Linting, Testing and more.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="UsefulLinks">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/#UsefulLinks"
            >
              Useful Links
            </SectionHeading>
            <p>
              This development environment includes a number of open source libraries for everything
              from testing, styling, state management, bundling and more. A great deal of what these
              libraries can do are outside of the scope of this documentation. Please find below
              some links to help you learn more about each of the libraries that make up React
              Catalyst:
            </p>
            <ul>
              <li>
                <a href="https://reactjs.org/docs/getting-started.html" target="_blank">
                  React.js
                </a>
              </li>
              <li>
                <a href="https://reactrouter.com/web/guides/quick-start" target="_blank">
                  ReactRouter
                </a>
              </li>
              <li>
                <a href="https://webpack.js.org/concepts/" target="_blank">
                  Webpack
                </a>
              </li>
              <li>
                <a href="https://sass-lang.com/documentation" target="_blank">
                  SCSS
                </a>
              </li>
              <li>
                <a href="https://github.com/css-modules/css-modules" target="_blank">
                  CSS Modules
                </a>
              </li>
              <li>
                <a href="https://redux.js.org/introduction/getting-started" target="_blank">
                  Redux
                </a>
              </li>
            </ul>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        {isScreenSmallerThan('lg') && (
          <FlexContainer
            justifyContent="center"
            alignItems="center"
            alignContent="center"
            wrap="wrap"
          >
            <FlexCell xs={4} sm={2} md={2} lg={2} xl={6} p={6}>
              <HtmlIcon />
            </FlexCell>
            <FlexCell xs={4} sm={2} md={2} lg={2} xl={6} p={6}>
              <CssIcon />
            </FlexCell>
            <FlexCell xs={4} sm={2} md={2} lg={2} xl={6} p={6}>
              <ScssIcon />
            </FlexCell>
            <FlexCell xs={4} sm={2} md={2} lg={2} xl={6} p={6}>
              <WebpackIcon />
            </FlexCell>
            <FlexCell xs={4} sm={2} md={2} lg={2} xl={6} p={6}>
              <JavascriptIcon />
            </FlexCell>
            <FlexCell xs={4} sm={2} md={2} lg={2} xl={6} p={6}>
              <ReactIcon />
            </FlexCell>
          </FlexContainer>
        )}
      </div>
    </>
  )
}

export default Home

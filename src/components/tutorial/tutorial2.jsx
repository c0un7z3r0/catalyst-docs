import React, { useEffect } from 'react'
import Code from '@perpetualsummer/catalyst-elements/Code'
import Lightbox from '@perpetualsummer/catalyst-elements/Lightbox'
import IntersectionWrapper from '../common/intersectionWrapper/intersectionWrapper'
import ScrollIntoView from '../common/scrollIntoView/scrollIntoView'
import SectionHeading from '../common/sectionHeading/sectionHeading'
import PageHeadingSplash from '../common/pageHeadingSplash/pageHeadingSplash'
import Tutorial2DeskXS from '../../public/images/splash/Tutorial2_Desk_XS_400.jpg'
import Tutorial2DeskSM from '../../public/images/splash/Tutorial2_Desk_SM_600.jpg'
import Tutorial2DeskMD from '../../public/images/splash/Tutorial2_Desk_MD_900.jpg'
import Tutorial2DeskLG from '../../public/images/splash/Tutorial2_Desk_LG_1200.jpg'
import Tutorial2DeskXL from '../../public/images/splash/Tutorial2_Desk_XL_1920.jpg'
import HomeThemeDarkScreenshot from '../../public/images/Tutorial_HomeThemeDark.jpg'
import HomeThemeDarkScreenshotThumb from '../../public/images/Tutorial_HomeThemeDark_thumb.jpg'
import HomeThemeDarkTodosScreenshot from '../../public/images/Tutorial_HomeThemeDarkTodos.jpg'
import HomeThemeDarkTodosScreenshotThumb from '../../public/images/Tutorial_HomeThemeDarkTodos_thumb.jpg'
import HomeThemeLightTodosScreenshot from '../../public/images/Tutorial_HomeThemeLightTodos.jpg'
import HomeThemeLightTodosScreenshotThumb from '../../public/images/Tutorial_HomeThemeLightTodos_thumb.jpg'
import FiltersAddedDarkScreenshot from '../../public/images/Tutorial_FiltersAdded_Dark.jpg'
import FiltersAddedDarkScreenshotThumb from '../../public/images/Tutorial_FiltersAdded_Dark_thumb.jpg'
import FiltersAddedLightScreenshot from '../../public/images/Tutorial_FiltersAdded_Light.jpg'
import FiltersAddedLightScreenshotThumb from '../../public/images/Tutorial_FiltersAdded_Light_thumb.jpg'
import styles from './tutorial2.module'

const Tutorial2 = () => {
  const imageSet = {
    xs: { url: Tutorial2DeskXS },
    sm: { url: Tutorial2DeskSM },
    md: { url: Tutorial2DeskMD },
    lg: { url: Tutorial2DeskLG },
    xl: { url: Tutorial2DeskXL },
    default: { url: Tutorial2DeskXL },
  }

  return (
    <>
      <PageHeadingSplash imageSet={imageSet}>
        <h1>
          <strong>Tutorial Part 2</strong>
        </h1>
      </PageHeadingSplash>
      <div className={styles.contentContainer}>
        <ScrollIntoView>
          <IntersectionWrapper id="SettingsRoute">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial2#SettingsRoute"
            >
              Creating A Settings Page With ReactRouter
            </SectionHeading>
            <p>
              In Part One of this tutorial we created a simple To-Do app that enables the user to
              Create, Delete, Set as "Done" and Archive To-Do Items. The state of the app is
              Maintained with Redux and persisted with the browser localStorage API.
            </p>
            <p>
              In this second part of the Tutorial we will add the ability to filter the To-Dos based
              on their "Done" status or whether they are Archived. We will also create a theme
              option in our settings page to demonstrate a use-case of React's Context API. But to
              start with we are going to add a setting page to demonstrate an example of the usage
              of ReactRouter which comes pre-bundled with React-Catalyst.
            </p>
            <p>
              The first step will be to create a new view file. Create a folder called{' '}
              <code>settingsView</code> in your <code>src/components</code> folder. In there create
              a new file called <code>settingsView.jsx</code>. Paste the following code:
            </p>
            <Code filename="src/components/settingsView/settingsView.jsx" language="JSX">
              {`
+import React from 'react'
+import { useHistory } from 'react-router-dom'
+import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
+import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
+import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
+import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
+import Button from '@perpetualsummer/catalyst-elements/Button'
+import styles from './settingsView.module'
+
+const SettingsView = () => {
+  const history = useHistory()
+  const backToHome = () => {
+    history.push('/')
+  }
+
+  return (
+    <FlexContainer
+      justifyContent="center"
+      alignItems="flex-end"
+      alignContent="center"
+      wrap="wrap"
+      p={0}
+    >
+      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
+        <GridContainer
+          grid="grid"
+          columns="1fr"
+          rows="100px 1fr"
+          gap="10px"
+          style={{
+            backgroundColor: 'white',
+            height: '100vh',
+          }}
+        >
+          <GridCell columnStart="1" columnEnd="1" rowStart="1" rowEnd="2">
+            <header className={styles.headerContainer}>
+              <h1 className={styles.headerText}>
+                <strong>SETTINGS</strong>
+              </h1>
+              <div className={styles.closeSettingsButtonContainer}>
+                <Button className={styles.closeSettingsButton} warning={true} onClick={() => backToHome()}>
+                  &times;
+                </Button>
+              </div>
+            </header>
+          </GridCell>
+          <GridCell columnStart="1" columnEnd="1" rowStart="2" rowEnd="3" padding="0 20px"></GridCell>
+        </GridContainer>
+      </FlexCell>
+    </FlexContainer>
+  )
+}
+
+export default SettingsView
              `}
            </Code>
            <p>
              There are a couple of new things going on here we should cover. First you will note
              that we import a hook function called <code>useHistory</code>. This is a ReactRouter
              hook specifically for use in browsers imported from the <code>react-router-dom</code>{' '}
              library. In our case we are using it to programatically create a link to the homepage
              by binding it to a <code>history</code> variable then calling the{' '}
              <code>history.push</code> method to redirect people to the homepage. This happens when
              a user clicks the close button.
            </p>
            <p>
              We need to add a few styles to this so create a stylesheet called{' '}
              <code>settingsView.module.scss</code> in the <code>src/components/settingsView</code>{' '}
              folder with the following styles:
            </p>
            <Code filename="src/components/settingsView/settingsView.module.scss" language="SCSS">
              {`
+.headerContainer{
+  display:flex;
+  background: #c0c0c0; 
+  height: 100%;
+}
+
+.headerText{
+  flex: 1 1 auto;
+  margin: 7px 30px;
+  text-align: left;
+  color:rgb(82, 82, 82);
+  text-shadow: 1px 1px 3px rgba(0,0,0,0.3), -1px -1px 3px rgba(255,255,255,0.4);
+}
+
+.closeSettingsButtonContainer{
+  flex: 0 0 auto;
+  width: 100px;
+  display: flex;
+  align-items: center;
+  justify-content: center;
+}
+
+.closeSettingsButton{
+  height: 60px;
+  width:60px;
+  border-radius: 50%;
+  font-size: 40px;
+  font-weight: bolder;
+}
              `}
            </Code>
            <p>
              So far we only have a Settings Page that does not yet have any actual settings we can
              change, We'll put that aside and add those later. For now, we have a component with a
              basic layout for our settings page, next we will add a new route for it in{' '}
              <code>app.jsx</code> so we can navigate to it. We do that like so:
            </p>
            <Code filename="src/app.jsx" language="JSX">
              {`
import { hot } from 'react-hot-loader/root'
import React from 'react'
import { Switch, Route } from 'react-router-dom'

import TodoView from './components/todoView/todoView'
+import SettingsView from './components/settingsView/settingsView'

import './public/css/normalize.css'
import './public/css/scrollbars.scss'
import './public/css/typography.scss'
import './global.scss'

const App = () => {
  return (
    <Switch>
      <Route exact path="/" component={TodoView} />
+      <Route exact path="/settings" component={SettingsView} />
    </Switch>
  )
}

export default hot(App)
              `}
            </Code>
            <p>
              The ReactRouter component <code>Route</code> takes the <code>exact</code> prop to tell
              it to only load the settings page when the browser address is the path which we have
              specified is <code>/settings</code>. The <code>component</code> prop tells it which
              component to load under those conditions, in this case <code>settingsView</code> which
              we import at the top of the file.
            </p>
            <p>
              If you manually browse to this address by typing the path into your browsers address
              bar (<code>localhost:3000/settings</code>) you should see the basic layout.
            </p>
            <p>
              Now that we have our route, we need a way to get to it. Lets add a settings icon to
              our <code>appHeader.jsx</code> component.
            </p>
            <Code filename="src/appHeader/appHeader.jsx" language="JSX">
              {`
import React from 'react'
+import { useHistory } from 'react-router-dom'
+import Button from '@perpetualsummer/catalyst-elements/Button'
import styles from './appHeader.module'

const AppHeader = () => {
+  const history = useHistory()
+  const gotToSettings = () => {
+    history.push('/settings')
+  }

  return (
    <header className={styles.headerContainer}>
      <h1 className={styles.headerText}>
        <strong>TODO APP</strong>
      </h1>
+      <div className={styles.openSettingsButtonContainer}>
+        <Button className={styles.openSettingsButton} onClick={() => gotToSettings()}>
+          <svg
+            xmlns="http://www.w3.org/2000/svg"
+            fill="white"
+            width="50"
+            height="50"
+            viewBox="0 0 24 24"
+          >
+            <path fill="none" d="M0 0h24v24H0V0z"></path>
+            <path d="M19.14 12.94c.04-.3.06-.61.06-.94 0-.32-.02-.64-.07-.94l2.03-1.58a.49.49 0 00.12-.61l-1.92-3.32a.488.488 0 00-.59-.22l-2.39.96c-.5-.38-1.03-.7-1.62-.94l-.36-2.54a.484.484 0 00-.48-.41h-3.84c-.24 0-.43.17-.47.41l-.36 2.54c-.59.24-1.13.57-1.62.94l-2.39-.96c-.22-.08-.47 0-.59.22L2.74 8.87c-.12.21-.08.47.12.61l2.03 1.58c-.05.3-.09.63-.09.94s.02.64.07.94l-2.03 1.58a.49.49 0 00-.12.61l1.92 3.32c.12.22.37.29.59.22l2.39-.96c.5.38 1.03.7 1.62.94l.36 2.54c.05.24.24.41.48.41h3.84c.24 0 .44-.17.47-.41l.36-2.54c.59-.24 1.13-.56 1.62-.94l2.39.96c.22.08.47 0 .59-.22l1.92-3.32c.12-.22.07-.47-.12-.61l-2.01-1.58zM12 15.6c-1.98 0-3.6-1.62-3.6-3.6s1.62-3.6 3.6-3.6 3.6 1.62 3.6 3.6-1.62 3.6-3.6 3.6z"></path>
+          </svg>
+        </Button>
+      </div>
    </header>
  )
}

export default AppHeader

              `}
            </Code>
            <p>
              And with the changes we just made, we also need to make some adjustments and additions
              to the <code>appHeader.module.scss</code> stylesheet. The SVG code is a gear icon to
              represent the settings link.
            </p>
            <Code filename="src/appHeader/appHeader.modules.scss" language="SCSS">
              {`
.headerContainer{
  display:flex;
  background: #c0c0c0; 
  height: 100%;
}

.headerText{
+  flex: 1 1 auto;
  margin: 7px 30px;
  text-align: left;
  color:rgb(82, 82, 82);
  text-shadow: 1px 1px 3px rgba(0,0,0,0.3), -1px -1px 3px rgba(255,255,255,0.4);
}

+.openSettingsButtonContainer{
+  flex: 0 0 auto;
+  width: 100px;
+  display: flex;
+  align-items: center;
+  justify-content: center;
+}
+
+.openSettingsButton{
+  height: 60px;
+  width:60px;
+  border-radius: 50%;
+  font-size: 40px;
+  font-weight: bolder;
+  padding: 5px;
+}
              `}
            </Code>
            <p>
              As you can see, the additions here are much the same as the features we added to{' '}
              <code>settingsView.jsx</code>. We added a button to the header which, in this case,
              calls a function that redirects the user to the <code>/settings</code> Route.
            </p>
            <p>
              We now have the ability to visit the <code>/settings</code> page by clicking the
              button in the top right of our To-Do app page then return to the To-Do app page by
              clicking the close icon on the settings page.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="ThemeContext">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial2#ThemeContext"
            >
              Switching Theme With React Context API
            </SectionHeading>
            <p>
              In this section we are going to add the ability to switch between light and dark
              themes using a setting on our settings page.{' '}
            </p>
            <p>
              I should point out at this stage that due to the fact that we are already using Redux
              that it would normally make sense to use Redux to manage the toggling of the theme as
              we have already integrated it into our application, but the purpose of using it in
              this instance is to teach you how to use the Context API. Context API and Redux do
              largely the same job - allow you to manage state and provide that state where it is
              needed - but they are implemented in different ways.
            </p>
            <p>
              You might ask why use Redux at all if React comes with Context API, the short answer
              is that while Redux has more dependencies and boilerplate it is more performant in
              terms of re-rendering so if you have a portion of state that is going to update
              regularly then go with Redux, if your state is going to be updated occasionally then
              go with Context API.
            </p>
            <p>
              This stack overflow answer details the strengths of Redux and Context API and its
              worth a read:{' '}
              <a
                href="https://stackoverflow.com/a/49569183/3626334"
                target="_blank"
                rel="noreferrer"
              >
                https://stackoverflow.com/a/49569183/3626334
              </a>
            </p>
            <p>
              This solution has been inspired / plagarised by an article on Better Programming by
              Alec Kan, you can find that here:
              <a
                href="https://betterprogramming.pub/react-context-api-part-1-dark-theme-3f00666cbacb"
                target="_blank"
                rel="noreferrer"
              >
                https://betterprogramming.pub/react-context-api-part-1-dark-theme-3f00666cbacb
              </a>
              . Our implementation is based on the solution provided in that article but is somewhat
              simplified as we wont be using CSS variables for our app we dont need to inject those,
              we just need a simple boolean to toggle our CSS module classes.
            </p>
            <p>
              With that being said, lets proceed with using the Context API to manage our theme.
            </p>
            <p>
              The first step is to write create our context and write a provider. We'll do that in a
              file called <code>themeContextProvider.js</code> in our <code>src/context</code>{' '}
              folder.
            </p>
            <Code filename="src/context/themeContextProvider.jsx" language="JSX">
              {`
+import React, { useState, useLayoutEffect } from 'react'
+import propTypes from 'prop-types'
+
+export const ThemeContext = React.createContext({
+  dark: false,
+  toggleThemeDark: () => {},
+})
+
+const ThemeContextProvider = ({ children }) => {
+  const [dark, setDark] = useState(window.localStorage.getItem('darkTheme'))
+
+  const toggleThemeDark = () => {
+    setDark(!dark)
+    window.localStorage.setItem('darkTheme', !dark)
+  }
+
+  useLayoutEffect(() => {
+    const lastTheme = window.localStorage.getItem('darkTheme')
+    if (lastTheme === 'true') {
+      setDark(true)
+    }
+    if (!lastTheme || lastTheme === 'false') {
+      setDark(false)
+    }
+  }, [dark])
+
+  return (
+    <ThemeContext.Provider
+      value={{
+        dark,
+        toggleThemeDark,
+      }}
+    >
+      {children}
+    </ThemeContext.Provider>
+  )
+}
+ThemeContextProvider.propTypes = {
+  children: propTypes.node.isRequired,
+}
+export default ThemeContextProvider
              `}
            </Code>
            <p>
              After our imports we first create the context called <code>ThemeContext</code> which
              we export. It has two params, the <code>dark</code> boolean (which defaults to false)
              and <code>toggleThemeDark</code> which is initially set to an empty function.
            </p>
            <p>
              Next we create the <code>ThemeContextProvider</code> which is going to be used to wrap
              our app at the highest level in order to pass down the theme prop and setter function
              to any child components that need it.
            </p>
            <p>
              The ThemeContextProvider has a useState hook that tracks whether the dark theme is
              selected, You'll note that we are using localStorage again here, the default value is
              taken from localStorage. Below that we have a function called{' '}
              <code>toggleThemeDark</code> which unsurprisingly toggles the dark theme on and off in
              useState and also sets it in localStorage.
            </p>
            <p>
              We also use <code>useLayoutEffect</code> which you can read more on{' '}
              <a
                href="https://reactjs.org/docs/hooks-reference.html#uselayouteffect"
                target="_blank"
                rel="noreferrer"
              >
                here
              </a>
              . In short, this code block is run whenever the <code>dark</code> useState value
              changes or when the component mounts. It gets the localStorage value of the dark theme
              and uses that to set the state in our app, if it does not exist in localStorage then
              it sets it to false.{' '}
            </p>
            <p>
              Finally, we return the <code>ThemeContext.Provider</code> with our <code>dark</code>{' '}
              boolean value and our <code>toggleThemeDark</code> setter function. This wraps the
              children (which will essentially be every component of our app) so we can subscribe
              any of them to use this Context as needed.
            </p>
            <p>
              Lets use our new ThemeContextProvider component at the root of our app in{' '}
              <code>src/index.js</code>
            </p>
            <Code filename="src/index.js" language="Javascript">
              {`
              // WEB BUILD Entry point
// Used for both 'dev' mode and 'production' mode builds
import React from 'react'
import { render } from 'react-dom'

// APPLICATION ROOT COMPONENT
import App from './app'

// REACT ROUTER DOM
import { BrowserRouter } from 'react-router-dom'

// REDUX
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
import { saveState } from './store/localStorage'
const store = configureStore()

store.subscribe(() => {
  saveState(store.getState())
})

// CONTEXT
import ViewportContextProvider from './context/viewportContextProvider'
+import ThemeContextProvider from './context/themeContextProvider'

render(
  <Provider store={store}>
    <BrowserRouter>
      <ViewportContextProvider>
+        <ThemeContextProvider>
          <App />
+        </ThemeContextProvider>
      </ViewportContextProvider>
    </BrowserRouter>
  </Provider>,
  document.getElementById('application')
)
              `}
            </Code>
            <p>
              Now any component that is a child of <code>{`<App />`}</code> - which is to say, all
              of them - can subscribe to this Context value and setter function. Lets give that a
              try on the <code>settingsView.jsx</code> component.
            </p>
            <Code filename="src/components/settingsView/settingsView.jsx" language="JSX">
              {`
-import React from 'react'
+import React, { useContext } from 'react'
import { useHistory } from 'react-router-dom'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
import Button from '@perpetualsummer/catalyst-elements/Button'
+import InputSwitch from '@perpetualsummer/catalyst-elements/InputSwitch'
+import { ThemeContext } from '../../context/themeContextProvider'
import styles from './settingsView.module'

const SettingsView = () => {
+  const { dark, toggleThemeDark } = useContext(ThemeContext)

  const history = useHistory()
  const backToHome = () => {
    history.push('/')
  }

  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-end"
      alignContent="center"
      wrap="wrap"
      p={0}
    >
      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
        <GridContainer
          grid="grid"
          columns="1fr"
          rows="100px 1fr"
          gap="10px"
          style={{
            backgroundColor: 'white',
            height: '100vh',
          }}
        >
          <GridCell columnStart="1" columnEnd="1" rowStart="1" rowEnd="2">
            <header className={styles.headerContainer}>
              <h1 className={styles.headerText}>
                <strong>SETTINGS</strong>
              </h1>
              <div className={styles.closeSettingsButtonContainer}>
                <Button
                  className={styles.closeSettingsButton}
                  warning={true}
                  onClick={() => backToHome()}
                >
                  &times;
                </Button>
              </div>
            </header>
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="2" rowEnd="3" padding="0 20px">
+            <InputSwitch 
+              label={dark ? 'Theme: Dark' : 'Theme: Light'}
+              onChange={() => toggleThemeDark()} 
+              checked={dark} 
+              alert={!dark} 
+            />
          </GridCell>
        </GridContainer>
      </FlexCell>
    </FlexContainer>
  )
}

export default SettingsView
            `}
            </Code>
            <p>
              Here we import the <code>useContext</code> hook from react, <code>InputSwitch</code>{' '}
              from the Catalyst-Elements Library and import the <code>ThemeContext</code> from the
              provider file we just made.
            </p>
            <p>
              We then use destructuring to bind the ThemeContext value <code>dark</code> and the
              setter function <code>toggleThemeDark</code> using the useContext hook here.
            </p>
            <code>{`const {(dark, toggleThemeDark)} = useContext(ThemeContext)`}</code>
            <p>
              Then we add the InputSwitch to the body of the settings view so that we can toggle its
              value here:
            </p>
            <code>
              <pre>{`<InputSwitch 
  label={dark ? 'Theme: Dark' : 'Theme: Light'}
  onChange={() => toggleThemeDark()} 
  checked={dark} 
  alert={!dark} 
/>`}</pre>
            </code>
            <p>
              And with that we should now have the ability to toggle the Context value{' '}
              <code>dark</code> to true or false which we can use to style our components.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="ThemeStyles">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial2#ThemeStyles"
            >
              Refactor Styles To Use Theme Context
            </SectionHeading>
            <p>
              To take advantage of our new Theme Toggle Feature we will need to subscribe some
              components to our Theme Context and make some style changes. Lets start from the{' '}
              <code>settingsView.jsx</code> component so we can see the changes as we toggle them.
              Open <code>settingsView.jsx</code> and make the following changes:
            </p>
            <Code filename="src/components/settingsView/settingsView.jsx" language="JSX">
              {`
import React, { useContext } from 'react'
import { useHistory } from 'react-router-dom'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
import Button from '@perpetualsummer/catalyst-elements/Button'
import InputSwitch from '@perpetualsummer/catalyst-elements/InputSwitch'
import { ThemeContext } from '../../context/themeContextProvider'
import styles from './settingsView.module'

const SettingsView = () => {
  const { dark, toggleThemeDark } = useContext(ThemeContext)

  const history = useHistory()
  const backToHome = () => {
    history.push('/')
  }

  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-end"
      alignContent="center"
      wrap="wrap"
      p={0}
+      className={dark ? styles.theme_dark : styles.theme_light}
    >
      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
        <GridContainer
          grid="grid"
          columns="1fr"
          rows="100px 1fr"
          gap="10px"
          style={{
-            backgroundColor: 'white',
            height: '100vh',
          }}
        >
          <GridCell columnStart="1" columnEnd="1" rowStart="1" rowEnd="2">
            <header className={styles.headerContainer}>
              <h1 className={styles.headerText}>
                <strong>SETTINGS</strong>
              </h1>
              <div className={styles.closeSettingsButtonContainer}>
                <Button
                  className={styles.closeSettingsButton}
                  warning={true}
                  onClick={() => backToHome()}
                >
                  &times;
                </Button>
              </div>
            </header>
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="2" rowEnd="3" padding="0 20px">
            <InputSwitch
              label={dark ? 'Theme: Dark' : 'Theme: Light'}
              onChange={() => toggleThemeDark()}
              checked={dark}
              alert={!dark}
            />
          </GridCell>
        </GridContainer>
      </FlexCell>
    </FlexContainer>
  )
}

export default SettingsView
              `}
            </Code>
            <p>
              Here we are adding a class to the uppermost FlexContainer that has the name{' '}
              <code>.theme_dark</code> or <code>theme_light</code> depending on which theme is
              selected. We also remove the inline background-colour style on the GridContainer as we
              are going to handle that in our stylesheet.
            </p>
            <p>
              Its a little jarring to have the colour change so drastically in an instant so lets
              add a little transition to make it easier on the eyes of our users, open{' '}
              <code>settingsView.module.scss</code> and add the following:
            </p>
            <Code filename="src/components/settingsView/settingsView.module.scss" language="SCSS">
              {`
.headerContainer{
  display:flex;
-  background: #c0c0c0; 
  height: 100%;
}

.headerText{
  flex: 1 1 auto;
  margin: 7px 30px;
  text-align: left;
-  color:rgb(82, 82, 82);
  text-shadow: 1px 1px 3px rgba(0,0,0,0.3), -1px -1px 3px rgba(255,255,255,0.4);
}

.closeSettingsButtonContainer{
  flex: 0 0 auto;
  width: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
}

.closeSettingsButton{
  height: 60px;
  width:60px;
  border-radius: 50%;
  font-size: 40px;
  font-weight: bolder;
}

+.theme_dark{
+  color:ghostwhite;
+  background-color: rgb(31, 31, 31);
+  transition: background-color 0.5s linear;
+
+  .gridContainer{
+    background-color: rgb(64, 64, 64);
+    transition: background-color 0.5s linear;
+  }
+  .headerContainer{
+    background-color: #313131; 
+    transition: background-color 0.5s linear;
+  }
+  .headerText{
+    color:rgb(209, 153, 0);
+  }
+}
+.theme_light{
+  color:black;
+  background-color: rgb(209, 209, 209);
+  transition: background-color 0.5s linear;
+  
+  .gridContainer{
+    background-color: white;
+    transition: background-color 0.5s linear;
+  }
+  .headerContainer{
+    background-color: #c0c0c0; 
+    transition: background-color 0.5s linear;
+  }
+  .headerText{
+    color:rgb(82, 82, 82);
+  }
+}
              `}
            </Code>
            <p>
              Now when we toggle our theme we should see a smooth transition of the
              background-colour. Lets apply the same pattern to some of our other components so they
              can utilise this theme change functionality.
            </p>
            <p>
              Our <code>todoView.jsx</code> component is the top of our App and is similar to the
              settingsView.jsx component we just worked on so lets start there, the implementation
              will be largely the same.
            </p>
            <Code filename="src/components/todoView/todoView.jsx" language="JSX">
              {`
-import React from 'react'
+import React, { useContext } from 'react'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
import AppHeader from '../appHeader/appHeader'
import AddTodo from '../addTodo/addTodo'
import TodoList from '../todoList/todoList'
import TodoItem from '../todoList/todoItem/todoItem'
import { useSelector } from 'react-redux'
+import { ThemeContext } from '../../context/themeContextProvider'
+import styles from './todoView.module.scss'

const TodoView = () => {
+  const { dark } = useContext(ThemeContext)
  const todoItems = useSelector((state) => state.todoList)
  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-end"
      alignContent="center"
      wrap="wrap"
      p={0}
+      className={dark ? styles.theme_dark : styles.theme_light}
    >
      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
        <GridContainer
          grid="grid"
          columns="1fr"
          rows="100px 1fr 150px"
          gap="10px"
          style={{
-            backgroundColor: 'white',
            height: '100vh',
          }}
+          className={styles.gridContainer}
        >
          <GridCell columnStart="1" columnEnd="1" rowStart="1" rowEnd="2">
-            <AppHeader />
+            <AppHeader dark={dark} />
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="2" rowEnd="3">
            <TodoList>
              {todoItems.map((itemData, i) => (
-                <TodoItem key={\`todoItem_$\{i}\`} todoData={itemData} />
+                <TodoItem key={\`todoItem_$\{i}\`} todoData={itemData} dark={dark} />
              ))}
            </TodoList>
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="3" rowEnd="4">
            <AddTodo />
          </GridCell>
        </GridContainer>
      </FlexCell>
    </FlexContainer>
  )
}

export default TodoView
              `}
            </Code>
            <p>
              As you can see we are importing <code>useContext</code> hook from react, importing the
              ThemeContext and adding a stylesheet. Next we use useContext to assign the{' '}
              <code>dark</code> value from context to a variable called 'dark'. In the flexContainer
              we use this to toggle a class, then in the grid container we remove the hand coded
              style and add a class name <code>.gridContainer</code> so that we can style that in a
              stylesheet we will create next.
            </p>
            <p>
              We also pass in the <code>dark</code> variable as a prop to the <code>AppHeader</code>{' '}
              and <code>TodoItem</code> components. We could use the useContext pattern we have used
              in this component but its more performant to just pass these in as props as their are
              the immediate children of this component.
            </p>
            <p>
              Lets create a stylesheet for our TodoView component. In the{' '}
              <code>src/components/todoView</code> folder, create a file called{' '}
              <code>todoView.module.scss</code> with the following code:
            </p>
            <Code filename="src/components/todoView/todoView.module.scss" language="SCSS">
              {`
+.theme_dark{
+  color:ghostwhite;
+  background-color: rgb(31, 31, 31);
+
+  .gridContainer{
+    background-color: rgb(64, 64, 64);
+  }
+}
+.theme_light{
+  color:black;
+  background-color: rgb(209, 209, 209);
+  
+  .gridContainer{
+    background-color: white;
+  }
+}
              `}
            </Code>
            <p>
              As you can see, these styles are very similar to the ones we made for settingView.jsx.
              We will continue this pattern in the components we have passed the <code>dark</code>{' '}
              prop to.
            </p>
            <p>Lets start with the AppHeader component:</p>
            <Code filename="src/components/appHeader/appHeader.jsx" language="JSX">
              {`
import React from 'react'
+import propTypes from 'prop-types'
import { useHistory } from 'react-router-dom'
import Button from '@perpetualsummer/catalyst-elements/Button'
import styles from './appHeader.module'

-const AppHeader = () => {
+const AppHeader = ({ dark }) => {
  const history = useHistory()
  const gotToSettings = () => {
    history.push('/settings')
  }

  return (
-    <header className={styles.headerContainer}>
+    <header className={dark ? styles.headerContainer_dark : styles.headerContainer_light}>
      <h1 className={styles.headerText}>
        <strong>TODO APP</strong>
      </h1>
      <div className={styles.openSettingsButtonContainer}>
        <Button className={styles.openSettingsButton} onClick={() => gotToSettings()}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="white"
            width="50"
            height="50"
            viewBox="0 0 24 24"
          >
            <path fill="none" d="M0 0h24v24H0V0z"></path>
            <path d="M19.14 12.94c.04-.3.06-.61.06-.94 0-.32-.02-.64-.07-.94l2.03-1.58a.49.49 0 00.12-.61l-1.92-3.32a.488.488 0 00-.59-.22l-2.39.96c-.5-.38-1.03-.7-1.62-.94l-.36-2.54a.484.484 0 00-.48-.41h-3.84c-.24 0-.43.17-.47.41l-.36 2.54c-.59.24-1.13.57-1.62.94l-2.39-.96c-.22-.08-.47 0-.59.22L2.74 8.87c-.12.21-.08.47.12.61l2.03 1.58c-.05.3-.09.63-.09.94s.02.64.07.94l-2.03 1.58a.49.49 0 00-.12.61l1.92 3.32c.12.22.37.29.59.22l2.39-.96c.5.38 1.03.7 1.62.94l.36 2.54c.05.24.24.41.48.41h3.84c.24 0 .44-.17.47-.41l.36-2.54c.59-.24 1.13-.56 1.62-.94l2.39.96c.22.08.47 0 .59-.22l1.92-3.32c.12-.22.07-.47-.12-.61l-2.01-1.58zM12 15.6c-1.98 0-3.6-1.62-3.6-3.6s1.62-3.6 3.6-3.6 3.6 1.62 3.6 3.6-1.62 3.6-3.6 3.6z"></path>
          </svg>
        </Button>
      </div>
    </header>
  )
}

+AppHeader.propTypes = {
+  dark: propTypes.bool.isRequired,
+}

export default AppHeader
              `}
            </Code>
            <p>
              Because we are now passing in the <code>dark</code> prop it is best practice that we
              use the propTypes library to check that we are passing in the correct kind of prop
              value, in this case a boolean (true or false) value.
            </p>
            <p>
              We then pass in the <code>dark</code> prop in the component declaration and we use it
              in the header class. We need to update our styles accordingly:
            </p>
            <Code filename="src/appHeader/appHeader.modules.scss" language="SCSS">
              {`
.headerContainer{
  display:flex;
-  background: #c0c0c0; 
  height: 100%;
}

+.headerContainer_dark{
+  composes: headerContainer;
+  background-color: #313131; 
+
+  .headerText{
+    color:rgb(209, 153, 0);
+  }
+}
+.headerContainer_light{
+  composes: headerContainer;
+  background: #c0c0c0; 
+  
+  .headerText{
+    color:rgb(82, 82, 82);
+  }
+}

.headerText{
  flex: 1 1 auto;
  margin: 7px 30px;
  text-align: left;
-  color:rgb(82, 82, 82);
  text-shadow: 1px 1px 3px rgba(0,0,0,0.3), -1px -1px 3px rgba(255,255,255,0.4);
}

.openSettingsButtonContainer{
  flex: 0 0 auto;
  width: 100px;
  display: flex;
  align-items: center;
  justify-content: center;
}

.openSettingsButton{
  height: 60px;
  width:60px;
  border-radius: 50%;
  font-size: 40px;
  font-weight: bolder;
  padding: 5px;
}
              `}
            </Code>

            <Lightbox image={HomeThemeDarkScreenshot}>
              <img className={styles.homeThemeDarkScreenshot} src={HomeThemeDarkScreenshotThumb} />
            </Lightbox>
            <p>
              As you can see, we have removed the background-color and colour values from the{' '}
              <code>.headerContainer</code> and <code>.headerText</code> classes respectively. Then
              we add two parent classes <code>.headerContainer_dark</code> and{' '}
              <code>.headerContainer_light</code> which use the composes feature to include the{' '}
              <code>.headerContainer</code> classes but with additional colours depending on the
              theme selected.
            </p>
            <p>Now our header should look consistent with the header on our settings page.</p>
            <p>
              All that remains to do to complete the theme feature is to repeat the process above
              for the <code>TodoItem</code> component.{' '}
            </p>
            <p>
              Open the <code>todoItem.jsx</code> file in the <code>src/components/todoItem</code>{' '}
              folder and make the following changes:
            </p>
            <Code filename="src/components/todoList/todoItem/todoItem.jsx" language="JSX">
              {`
import React from 'react'
import propTypes from 'prop-types'
import InputCheckbox from '@perpetualsummer/catalyst-elements/InputCheckbox'
import Button from '@perpetualsummer/catalyst-elements/Button'
import { useDispatch } from 'react-redux'
import { toggleTodoDone, toggleTodoArchived, deleteTodo } from '../../../actions/todoListActions'
import styles from './todoItem.module.scss'

-const TodoItem = ({ todoData }) => {
+const TodoItem = ({ todoData, dark }) => {
  const dispatch = useDispatch()
  const { ToDoText, isDone, isArchived } = todoData

  return (
-    <li className={styles.todoListItem}>
+    <li className={dark ? styles.todoListItem_dark : styles.todoListItem_light}>
      <div className={styles.checkboxContainer}>
-        <InputCheckbox checked={isDone} onChange={() => dispatch(toggleTodoDone(ToDoText))} />
+        <InputCheckbox
+          checked={isDone}
+          onChange={() => dispatch(toggleTodoDone(ToDoText))}
+          valid={dark}
+        />
      </div>
      <p className={isDone ? styles.todoText_done : styles.todoText}>{ToDoText}</p>
      <div className={styles.deleteButtonContainer}>
        <Button
          className={styles.deleteButton}
          warning={true}
          onClick={() => dispatch(deleteTodo(ToDoText))}
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            fill="white"
            width="40"
            height="40"
            viewBox="0 0 24 24"
          >
            <path fill="none" d="M0 0h24v24H0z"></path>
            <path fill="none" d="M0 0h24v24H0V0z"></path>
            <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zm2.46-7.12l1.41-1.41L12 12.59l2.12-2.12 1.41 1.41L13.41 14l2.12 2.12-1.41 1.41L12 15.41l-2.12 2.12-1.41-1.41L10.59 14l-2.13-2.12zM15.5 4l-1-1h-5l-1 1H5v2h14V4z"></path>
          </svg>
        </Button>
      </div>
      <div className={styles.archiveButtonContainer}>
        <Button
          className={styles.archiveButton}
          warning={isArchived}
          onClick={() => dispatch(toggleTodoArchived(ToDoText))}
        >
          {isArchived ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="white"
              width="40"
              height="40"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0H24V24H0z"></path>
              <path d="M20.55 5.22l-1.39-1.68A1.51 1.51 0 0018 3H6c-.47 0-.88.21-1.15.55L3.46 5.22C3.17 5.57 3 6.01 3 6.5V19a2 2 0 002 2h14c1.1 0 2-.9 2-2V6.5c0-.49-.17-.93-.45-1.28zM12 9.5l5.5 5.5H14v2h-4v-2H6.5L12 9.5zM5.12 5l.82-1h12l.93 1H5.12z"></path>
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="white"
              width="40"
              height="40"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0h24v24H0z"></path>
              <path d="M20.54 5.23l-1.39-1.68C18.88 3.21 18.47 3 18 3H6c-.47 0-.88.21-1.16.55L3.46 5.23C3.17 5.57 3 6.02 3 6.5V19c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V6.5c0-.48-.17-.93-.46-1.27zM12 17.5L6.5 12H10v-2h4v2h3.5L12 17.5zM5.12 5l.81-1h12l.94 1H5.12z"></path>
            </svg>
          )}
        </Button>
      </div>
    </li>
  )
}

TodoItem.propTypes = {
  todoData: propTypes.shape({
    ToDoText: propTypes.string.isRequired,
    isDone: propTypes.bool.isRequired,
    isArchived: propTypes.bool.isRequired,
  }).isRequired,
+  dark: propTypes.bool.isRequired,
}

export default TodoItem
              `}
            </Code>
            <p>
              We've added the <code>dark</code> prop to the component declaration and used it in the
              uppermost class, in this case the <code>{`<li>`}</code> tag replacing the{' '}
              <code>styles.todoListItem</code> with a switch{' '}
              <code>dark ? styles.todoListItem_dark : styles.todoListItem_light</code>. We'll need
              to adjust our styles to accomodate this change.
            </p>
            <p>
              Also note that we have added the <code>{`valid={dark}`}</code> prop to the
              InputCheckbox component, this is to give it more contrast against the dark background
              we are going to use for the TodoItem.
            </p>
            <p>
              Again, because we've added a prop we need to ensure that it has a propType value so
              we've also added that at the bottom.
            </p>
            <p>
              Lets update our styles, open <code>todoItem.module.scss</code> and change the
              following:
            </p>
            <Code filename="src/components/todoList/todoItem/todoItem.module.scss" language="SCSS">
              {`
.todoListItem{
  list-style-type: none;
  min-height:50px;
  display: flex;
  align-items: center;
  padding:6px 12px;
  margin-bottom:5px;
  background-color: gainsboro;
  border-radius: 15px;
}
+.todoListItem_dark{
+  composes: todoListItem;
+  background-color: rgb(126, 126, 126);
+  
+  .todoText_done{
+    color:rgb(206, 206, 206);
+  }
+}
+.todoListItem_light{
+  composes: todoListItem;
+  background-color: gainsboro;
+  
+  .todoText_done{
+    color:gray;
+  }
+}

.checkboxContainer{
  flex: 0 0 auto;
  display:flex;
  justify-content: center;
  width: 50px;
}
.todoText{
  flex: 1 1 auto;
  font-size: 30px;
  margin:0;
  padding-left:10px;
}
.todoText_done{
  composes: todoText;
-  color:gray;
  text-decoration: line-through;
}
.deleteButtonContainer
.archiveButtonContainer{
  flex: 0 0 auto;
  display:flex;
  justify-content: center;
  width: 50px;
}
.deleteButton,
.archiveButton{
  min-width:60px;
  padding:0;
}
.deleteButton{
  margin-right:8px;
}
`}
            </Code>
            <Lightbox image={HomeThemeDarkTodosScreenshot}>
              <img
                className={styles.toggleThemeScreenshot}
                src={HomeThemeDarkTodosScreenshotThumb}
              />
            </Lightbox>
            <Lightbox image={HomeThemeLightTodosScreenshot}>
              <img
                className={styles.toggleThemeScreenshot}
                src={HomeThemeLightTodosScreenshotThumb}
              />
            </Lightbox>
            <p>
              Again we use <code>composes</code> to include the styles from the class we replaced (
              <code>.todoListItem</code>) and we added a background colour to each of the light and
              dark themes. We have also used slightly different colours for the text colour of
              "done" To-Dos as the original did not contrast well with the dark theme background.
            </p>
            <p>
              And, just like that, we have implemented our Dark / Light Theme across our
              application. Any subsequent components we create will have to accommodate this if they
              have styles but now you know how to leverage ContextAPI to provide data across your
              application without having to prop-drill it through components that do not need it.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
        <ScrollIntoView>
          <IntersectionWrapper id="AddingFilters">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial2#AddingFilters"
            >
              Adding Filters To Hide & Show To-Dos
            </SectionHeading>
            <p>
              At present, when we Archive a To-Do all it does is change the look of the button.
              Similarly, when we set a To-Do to "done" all it does is change the styles to grey it
              out and strike-through the text. In this section of the tutorial we will introduce
              some filters to the app to enable us to hide or show Archived or Done To-Dos.
            </p>
            <p>
              We will use Redux to track which filters have been selected, lets build our Redux
              state, actions, types and reducers. We'll start with the initial state. Open{' '}
              <code>src/reducers/initialState.js</code>:
            </p>
            <Code filename="src/reducers/initialState.js" language="Javascript">
              {`
export default {
  todoList: [],
+  filters: {
+    todoVisible: true,
+    doneVisible: true,
+    archivedVisible: false,
+  },
}
`}
            </Code>
            <p>
              As you can see, we have added a <code>filter</code> object with a series of booleans:{' '}
              <code>todoVisible</code>, <code>doneVisible</code> and <code>archivedVisible</code>.
              Next we need to add some action types.
            </p>
            <Code filename="src/actions/actionTypes.js" language="Javascript">
              {`
export const ADD_TODO = 'ADD_TODO'
export const DELETE_TODO = 'DELETE_TODO'
export const TOGGLE_TODO_DONE = 'TOGGLE_TODO_DONE'
export const TOGGLE_TODO_ARCHIVED = 'TOGGLE_TODO_ARCHIVED'

+export const TOGGLE_TODO_FILTER = 'TOGGLE_TODO_FILTER'
+export const TOGGLE_DONE_FILTER = 'TOGGLE_DONE_FILTER'
+export const TOGGLE_ARCHIVE_FILTER = 'TOGGLE_ARCHIVE_FILTER'
`}
            </Code>
            <p>
              And we will also need to add some actions to trigger. Create a new actions file called{' '}
              <code>filterActions.js</code> in the <code>src/actions</code> directory, add the
              following code:
            </p>
            <Code filename="src/actions/filterActions.js" language="Javascript">
              {`
+import * as types from './actionTypes'
+
+export function toggleTodoFilter() {
+  return {
+    type: types.TOGGLE_TODO_FILTER,
+  }
+}
+
+export function toggleDoneFilter() {
+  return {
+    type: types.TOGGLE_DONE_FILTER,
+  }
+}
+
+export function toggleArchiveFilter() {
+  return {
+    type: types.TOGGLE_ARCHIVE_FILTER,
+  }
+}
`}
            </Code>
            <p>
              These actions will of course require reducers to change the state accordingly. Create
              a reducer file called <code>filterReducer.js</code> in the <code>src/reducers</code>{' '}
              folder with the following code:
            </p>
            <Code filename="src/reducer/filterReducer.js" language="Javascript">
              {`
+import * as types from '../actions/actionTypes'
+import initialState from './initialState'
+
+export default function filterReducer(state = initialState.filters, action) {
+  switch (action.type) {
+    case types.TOGGLE_TODO_FILTER: {
+      return {
+        ...state,
+        todoVisible: !state.todoVisible,
+      }
+    }
+
+    case types.TOGGLE_DONE_FILTER: {
+      return {
+        ...state,
+        doneVisible: !state.doneVisible,
+      }
+    }
+
+    case types.TOGGLE_ARCHIVE_FILTER: {
+      return {
+        ...state,
+        archivedVisible: !state.archivedVisible,
+      }
+    }
+
+    default:
+      return state
+  }
+}
`}
            </Code>
            <p>
              Each of these actions and the reducers they call will update the relevant item in the
              new <code>filters</code> object in our Redux state. Now that we have our Redux actions
              ready, lets write a filter component for our UI to enable us to toggle these.
            </p>
            <p>
              Our filter component will be a series of switches that sit along the top of the
              TodoView component, above where our To-Do Items are. Create a new folder in{' '}
              <code>src/components</code> called <code>todoFilters</code> and inside that create a
              component file called <code>todoFilters.jsx</code>. Add the following code:
            </p>
            <Code filename="src/components/todoFilters/todoFilters.jsx" language="JSX">
              {`
+import React from 'react'
+import propTypes from 'prop-types'
+import { useDispatch } from 'react-redux'
+import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
+import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
+import InputSwitch from '@perpetualsummer/catalyst-elements/InputSwitch'
+import {
+  toggleTodoFilter,
+  toggleDoneFilter,
+  toggleArchivedFilter,
+} from '../../actions/filterActions'
+import styles from './todoFilters.module'
+
+const TodoFilters = ({ dark, todoVisible, doneVisible, archivedVisible }) => {
+  const dispatch = useDispatch()
+
+  return (
+    <GridContainer grid="grid" columns="1fr 1fr 1fr" rows="64px" gap="10px" padding="0 10px 10px">
+      <GridCell
+        columnStart="1"
+        columnEnd="2"
+        className={dark ? styles.filterCell_dark : styles.filterCell_light}
+      >
+        <InputSwitch
+          label="To Do"
+          name="InputName"
+          onChange={() => dispatch(toggleTodoFilter())}
+          checked={todoVisible}
+          valid={todoVisible}
+          warning={!todoVisible}
+        />
+      </GridCell>
+      <GridCell
+        columnStart="2"
+        columnEnd="3"
+        className={dark ? styles.filterCell_dark : styles.filterCell_light}
+      >
+        <InputSwitch
+          label="Done"
+          name="InputName"
+          onChange={() => dispatch(toggleDoneFilter())}
+          checked={doneVisible}
+          valid={doneVisible}
+          warning={!doneVisible}
+        />
+      </GridCell>
+      <GridCell
+        columnStart="3"
+        columnEnd="4"
+        className={dark ? styles.filterCell_dark : styles.filterCell_light}
+      >
+        <InputSwitch
+          label="Archived"
+          name="InputName"
+          onChange={() => dispatch(toggleArchivedFilter())}
+          checked={archivedVisible}
+          valid={archivedVisible}
+          warning={!archivedVisible}
+        />
+      </GridCell>
+    </GridContainer>
+  )
+}
+
+TodoFilters.propTypes = {
+  dark: propTypes.bool.isRequired,
+  todoVisible: propTypes.bool.isRequired,
+  doneVisible: propTypes.bool.isRequired,
+  archivedVisible: propTypes.bool.isRequired,
+}
+
+export default TodoFilters
              `}
            </Code>
            <p>
              In this new component we are passing in the <code>dark</code> prop which tells the
              component which theme is being used and three other props <code>todoVisible</code>,{' '}
              <code>doneVisible</code> and <code>archivedVisible</code> which tells us which filters
              are active. All four props are boolean values.
            </p>
            <p>
              We are importing and binding the <code>useDispatch</code> hook as we will need to call
              the redux actions when the toggle switches are clicked by the user.
            </p>
            <p>
              In the return block we are using GridContainer and GridCell to produce a row of three
              cells each housing a InputSwitch.
            </p>
            <p>
              When the switch is pressed an action is dispatched according to what that switch
              toggles, either <code>toggleTodoFilter</code>, <code>toggleDoneFilter</code> or{' '}
              <code>toggleArchivedFilter</code>
            </p>
            <p>
              You'll note that we are using the <code>dark</code> prop to toggle classes for each of
              our themes much like we did in the previous section of this tutorial. We will need a
              style sheet for that too so make a file called <code>todoFilters.module.scss</code> in
              the <code>src/components/todoFilter</code> folder.
            </p>
            <Code filename="src/components/todoFilters/todoFilters.module.scss" language="SCSS">
              {`
.filterCell {
  display:flex;
  justify-content: center;
}

.filterCell_dark{
  composes:filterCell;
  background-color: rgb(126,126,126);
}
.filterCell_light{
  composes:filterCell;
  background-color: rgb(220,220,220);
}
              `}
            </Code>
            <p>
              This will ensure the switch is centrally aligned in each GridCell with the appropriate
              colour provided depending on which theme is selected.
            </p>
            <p>
              Now we have our Filter Switch component made, lets incorporate that into our app in{' '}
              <code>todoView.jsx</code> with the following changes:
            </p>
            <Code filename="src/components/todoView/todoView.jsx" language="JSX">
              {`
import React, { useContext } from 'react'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
import AppHeader from '../appHeader/appHeader'
import AddTodo from '../addTodo/addTodo'
import TodoList from '../todoList/todoList'
import TodoItem from '../todoList/todoItem/todoItem'
+import TodoFilters from '../todoFilters/todoFilters'
import { useSelector } from 'react-redux'
import { ThemeContext } from '../../context/themeContextProvider'
import styles from './todoView.module'

const TodoView = () => {
  const { dark } = useContext(ThemeContext)
  const todoItems = useSelector((state) => state.todoList)
+  const filters = useSelector((state) => state.filters)
+  const { todoVisible, doneVisible, archivedVisible } = filters

  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-end"
      alignContent="center"
      wrap="wrap"
      p={0}
      className={dark ? styles.theme_dark : styles.theme_light}
    >
      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
        <GridContainer
          grid="grid"
          columns="1fr"
          rows="100px 1fr 150px"
          gap="10px"
          style={{
            height: '100vh',
          }}
          className={styles.gridContainer}
        >
          <GridCell columnStart="1" columnEnd="1" rowStart="1" rowEnd="2">
            <AppHeader dark={dark} />
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="2" rowEnd="3">
+            <TodoFilters
+              dark={dark}
+              todoVisible={todoVisible}
+              doneVisible={doneVisible}
+              archivedVisible={archivedVisible}
+            />
            <TodoList>
              {todoItems.map((itemData, i) => (
                <TodoItem key={\`todoItem_$\{i}\`} todoData={itemData} dark={dark} />
              ))}
            </TodoList>
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="3" rowEnd="4">
            <AddTodo />
          </GridCell>
        </GridContainer>
      </FlexCell>
    </FlexContainer>
  )
}

export default TodoView
              `}
            </Code>
            <Lightbox image={FiltersAddedDarkScreenshot}>
              <img
                className={styles.homeThemeDarkScreenshot}
                src={FiltersAddedDarkScreenshotThumb}
              />
            </Lightbox>
            <Lightbox image={FiltersAddedLightScreenshot}>
              <img
                className={styles.homeThemeDarkScreenshot}
                src={FiltersAddedLightScreenshotThumb}
              />
            </Lightbox>
            <p>
              Here we are importing our new component. We use the <code>useSelector</code>{' '}
              react-redux hook to access the <code>filters</code> section of our redux state which
              we then destructure into the <code>todoVisible</code>, <code>doneVisible</code> &{' '}
              <code>archivedVisible</code> variables.
            </p>
            <p>
              We then pass those variables into the <code>todoFilters.jsx</code> component that we
              added in the return block, just above our todoList component which should produce the
              following results:
            </p>
            <p>
              We now have the filter values set in our redux state, we can change them with the
              filter switches, now all we need to do is apply the filters to the Todo's we have in
              our list.
            </p>
            <p>
              To utilise our new filters we are going to edit the map function in the{' '}
              <code>todoView.jsx</code> component. Open that file and make the following changes:
            </p>
            <Code filename="src/components/todoView/todoView.jsx" language="JSX">
              {`
import React, { useContext } from 'react'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
import AppHeader from '../appHeader/appHeader'
import AddTodo from '../addTodo/addTodo'
import TodoList from '../todoList/todoList'
import TodoItem from '../todoList/todoItem/todoItem'
import TodoFilters from '../todoFilters/todoFilters'
import { useSelector } from 'react-redux'
import { ThemeContext } from '../../context/themeContextProvider'
import styles from './todoView.module'

const TodoView = () => {
  const { dark } = useContext(ThemeContext)
  const todoItems = useSelector((state) => state.todoList)
  const filters = useSelector((state) => state.filters)
  const { todoVisible, doneVisible, archivedVisible } = filters

  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-end"
      alignContent="center"
      wrap="wrap"
      p={0}
      className={dark ? styles.theme_dark : styles.theme_light}
    >
      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
        <GridContainer
          grid="grid"
          columns="1fr"
          rows="100px 1fr 150px"
          gap="10px"
          style={{
            height: '100vh',
          }}
          className={styles.gridContainer}
        >
          <GridCell columnStart="1" columnEnd="1" rowStart="1" rowEnd="2">
            <AppHeader dark={dark} />
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="2" rowEnd="3">
            <TodoFilters
              dark={dark}
              todoVisible={todoVisible}
              doneVisible={doneVisible}
              archivedVisible={archivedVisible}
            />
            <TodoList>
-              {todoItems.map((itemData, i) => (
-                <TodoItem key={\`todoItem_$\{i}\`} todoData={itemData} dark={dark} />
-              ))}
+              {todoItems.map((itemData, i) => {
+                if (itemData.isArchived === true && archivedVisible === false) {
+                  return null
+                } else if (
+                  (itemData.isDone === false && todoVisible === true) ||
+                  (itemData.isDone === true && doneVisible === true)
+                ) {
+                  return <TodoItem key={\`todoItem_$\{i}\`} todoData={itemData} dark={dark} />
+                }
+              })}
            </TodoList>
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="3" rowEnd="4">
            <AddTodo />
          </GridCell>
        </GridContainer>
      </FlexCell>
    </FlexContainer>
  )
}

export default TodoView
              `}
            </Code>
            <p>
              The changes above conditionally return a ToDo based on the filters selected. First we
              want to ensure that if a To-Do is Archived then regardless of whether it is "Done" or
              not we want to hide it, we do that in the first condition:
            </p>
            <code>
              <pre>
                {`if (itemData.isArchived === true && archivedVisible === false) {
  return null
} ... `}
              </pre>
            </code>
            <p>
              next, if the To-Do is not archived we check its status, if it is not "Done" and our{' '}
              <code>todoVisible</code> filter is set to true then it should return that item as a{' '}
              <code>{`<TodoItem ...>`}</code> component to display it in the UI...
            </p>
            <code>
              <pre>
                {`... } else if (
  (itemData.isDone === false && todoVisible === true) || ...`}
              </pre>
            </code>
            <p>
              ... or if it is "Done" and the <code>doneVisible</code> filter is set to true then it
              should also return a <code>{`<TodoItem ...>`}</code> component instance.
            </p>
            <code>
              <pre>{`... (itemData.isDone === true && doneVisible === true)
  ) {
      return <TodoItem key={\`todoItem_$\{i}\`} todoData={itemData} dark={dark} />
    }`}</pre>
            </code>
            <p>
              That satisfies the criteria of all our filters. They should now work as expected.
              Success!
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
      </div>
    </>
  )
}

export default Tutorial2

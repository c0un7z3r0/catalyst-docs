import React from 'react'
import Code from '@perpetualsummer/catalyst-elements/Code'
import Lightbox from '@perpetualsummer/catalyst-elements/Lightbox'
import IntersectionWrapper from '../common/intersectionWrapper/intersectionWrapper'
import ScrollIntoView from '../common/scrollIntoView/scrollIntoView'
import SectionHeading from '../common/sectionHeading/sectionHeading'
import PageHeadingSplash from '../common/pageHeadingSplash/pageHeadingSplash'
import { Link } from 'react-router-dom'
import TutorialDeskXS from '../../public/images/splash/Tutorial_Desk_XS_400.jpg'
import TutorialDeskSM from '../../public/images/splash/Tutorial_Desk_SM_600.jpg'
import TutorialDeskMD from '../../public/images/splash/Tutorial_Desk_MD_900.jpg'
import TutorialDeskLG from '../../public/images/splash/Tutorial_Desk_LG_1200.jpg'
import TutorialDeskXL from '../../public/images/splash/Tutorial_Desk_XL_1920.jpg'
import TutorialFolderStructureImage from '../../public/images/Tutorial_FolderStructure.jpg'
import CatalystScreenshot from '../../public/images/Catalyst_StartPage.jpg'
import BasicLayoutScreenshot from '../../public/images/Tutorial_BaseLayout.jpg'
import BasicLayoutScreenshotThumb from '../../public/images/Tutorial_BaseLayout_thumb.jpg'
import addTodoScreenshot from '../../public/images/Tutorial_AddTodo.jpg'
import addTodoScreenshotThumb from '../../public/images/Tutorial_AddTodo_thumb.jpg'
import reduxDevToolsScreenshot from '../../public/images/Tutorial_ReduxDevTools.jpg'
import reduxDevToolsScreenshotThumb from '../../public/images/Tutorial_ReduxDevTools_thumb.jpg'
import todoItemAddedScreenshot from '../../public/images/Tutorial_TodoItemAdded.jpg'
import todoItemAddedScreenshotThumb from '../../public/images/Tutorial_TodoItemAdded_thumb.jpg'
import todoItemStyledScreenshot from '../../public/images/Tutorial_TodoItemStyled.jpg'
import appWithTodosScreenshot from '../../public/images/Tutorial_AppWithTodos.jpg'
import appWithTodosScreenshotThumb from '../../public/images/Tutorial_AppWithTodos_thumb.jpg'
import todoWithStrikethroughScreenshot from '../../public/images/Tutorial_DoneTodoStrikethough.jpg'
import partOneComplete from '../../public/images/Tutorial_Part1Complete.jpg'
import partOneCompleteThumb from '../../public/images/Tutorial_Part1Complete_thumb.jpg'

import styles from './tutorial1.module'

const Tutorial1 = () => {
  const imageSet = {
    xs: { url: TutorialDeskXS },
    sm: { url: TutorialDeskSM },
    md: { url: TutorialDeskMD },
    lg: { url: TutorialDeskLG },
    xl: { url: TutorialDeskXL },
    default: { url: TutorialDeskXL },
  }
  return (
    <>
      <PageHeadingSplash imageSet={imageSet}>
        <h1>
          <strong>Tutorial Part 1</strong>
        </h1>
      </PageHeadingSplash>
      <div className={styles.contentContainer}>
        <ScrollIntoView>
          <IntersectionWrapper id="Purpose">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#Purpose"
            >
              Purpose
            </SectionHeading>
            <p>
              In this tutorial we are going to make a basic to-do list application (as is tradition)
              that will illustrate a variety of features bundled with the React Catalyst Development
              Environment.
            </p>
            <p>
              We will use ReactRouter to compartmentalise the various sections of the app, React
              Context API to provide certain state elements relating to the viewport to only the
              components that need them and Redux to store and manage the To-Dos the user can create
              and manipulate.
            </p>
            <p>
              We will build a series of React.js components that we will style with SCSS/CSS Modules
              and we'll demonstrate how to use the Hot-reloading Development Mode to see our code
              changes on-the-fly and we will use the Production build features that come built into
              the React Catalyst Development Environment to create a cross-browser compatible,
              bandwidth-friendly To-do Web Application.
            </p>
            <p>
              We will also use React-Catalyst's sister library, Catalyst-Elements to provide user
              interface components, you can learn more about that library of components and their
              API{' '}
              <a
                href="https://catalyst-elements.perpetualsummer.ltd/"
                target="_blank"
                rel="noreferrer"
              >
                here
              </a>
              .
            </p>
            <p>
              If you'd prefer to read through the completed code rather than follow along the
              tutorial and write it yourself, the source can be found{' '}
              <a
                href="https://bitbucket.org/c0un7z3r0/react-catalyst-tutorial/src/master/"
                target="_blank"
                rel="noreferrer"
              >
                here
              </a>
              .
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="Installation">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#Installation"
            >
              Installation
            </SectionHeading>
            <p>
              In order to download and utilise the React-Catalyst Development Environment you will
              need Node.js and Git, you can download them from the Requirements section of the
              Getting Started Page <Link to={'QuickStart#Requirements'}>here</Link>.
            </p>
            <p>
              To get started we need to create a folder and download the React-Catalyst into it.
              Open your terminal and create a directory:
            </p>
            <code>mkdir MyTodoApp</code>
            <p>Then switch into that directory:</p>
            <code>cd MyTodoApp</code>
            <p>
              Now we need to clone React-Catalyst, we can do that with Git using the following
              terminal command:
            </p>
            <code>git clone https://bitbucket.org/c0un7z3r0/react-catalyst-v2.git .</code>
            <p>
              Once that has downloaded install the dependencies using NPM, the package manager that
              comes with node:
            </p>
            <code>npm i</code>
            <p>
              In a minute or two, once NPM has installed everything, we are ready to start. Run the
              application in development mode with the following command:
            </p>
            <code>npm run start</code>
            <p>
              The starter page should load and open automatically in your browser, you should see
              the following page open on <code>localhost:3000</code>:
            </p>
            <img className={styles.catalystStartupImage} src={CatalystScreenshot} />
            <p>
              We now have our React-Catalyst Development Environment set up and ready to code. In
              the next section we will clear out some of the base code to give ourselves a clean
              slate to build our To-Do Application.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="Preparation">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#Preparation"
            >
              Preparation
            </SectionHeading>
            <p>
              Lets start by opening our code editor. You are free to use any IDE/Code Editor you
              prefer but if you'd like a recommendation we'd suggest VSCode. You can download that{' '}
              <a href="https://code.visualstudio.com/download" target="_blank" rel="noreferrer">
                here
              </a>
              .
            </p>
            <img className={styles.folderStructureImage} src={TutorialFolderStructureImage} />
            <p>
              Open the <code>MyTodoApp</code> directory you created in your IDE/Code Editor of
              choice and you should have a folder structure something like this (right):
            </p>
            <p>
              Lets quickly summarise what each of these files and folders does, we'll go into more
              depth on most of these later in the tutorial.
            </p>
            <ul>
              <li>
                <code>catalyst/</code> Folder: Contains the dev server for Development Mode. This
                also houses the Bundle Reports that are generated when we do a production build.
              </li>
              <li>
                <code>cypress/</code> Folder: This is where you should write your end-to-end tests
                for cypress, specifically the <code>e2e</code> folder within.
              </li>
              <li>
                <code>node_modules/</code> Folder: Contains all the dependency modules we installed
                when we did <code>npm i</code>.
              </li>
              <li>
                <code>src/</code> Folder: This is where our code will be. It has a number of
                folders, again we will go into more detail on these as we progress through the
                tutorial.
              </li>
              <li>
                <code>testing/</code> Folder: This contains our configuration for the Unit testing
                frameworks utilised in React Catalyst. We use Jest and React-Testing-Library.
              </li>
              <li>
                <code>.babelrc</code> contains the configuration that enables us to use the latest
                ES6 javascript syntax and have it converted to older ES5 to provide the broadest
                browser compatability.
              </li>
              <li>
                <code>.browserlistrc</code> is used by a number of libraries to determine which
                browsers we wish to target for compatability.
              </li>
              <li>
                <code>.editorconfig</code> tells your IDE/Code Editor what your preferences are, for
                instance it comes pre-configured to use 2 spaces per tab. Feel free to change this
                to your personal preference.
              </li>
              <li>
                <code>.eslintignore</code> This document tells ESlint which files and folders it
                should not attempt to lint check.
              </li>
              <li>
                <code>.eslintrc.js</code> is the config file for ESLint. That is an invaluble
                library that provides highlighting and warnings to help you avoid errors in your
                javascript code. It comes preconfigured with react plugins.
              </li>
              <li>
                <code>.gitignore</code> tells git what files and folders should be ignored when
                committing changes.
              </li>
              <li>
                <code>.prettierrc</code> is the config file for Prettier. Another invaluble library
                that automatically formats and indents changes to your code when it is saved.
              </li>
              <li>
                <code>cypress.json</code> is the config file for Cypress, it configures the default
                route to be checked, specifies where your tests should be run from and the viewport
                dimensions.
              </li>
              <li>
                <code>jest.config.js</code> is the config file for the Unit Testing Library, Jest.
              </li>
              <li>
                <code>jsdoc.conf.json</code> is the config file for JSDoc. That is a library that
                auto-generates documentation for the files in your application.
              </li>
              <li>
                <code>package.json</code> contains the scripts and dependencies for react-catalyst.
                If we add a module to the application it is added as an entry in here.
              </li>
              <li>
                <code>package-lock.json</code> is a file autogenerated when running{' '}
                <code>npm i</code> - its not something we need to concern ourselves with.
              </li>
              <li>
                <code>postcss.config.js</code> is a configuration file that provides transforms for
                our css files. It is pre-configured to use Autoprefixer (for browser compatibility)
                and CSSnano for minification.
              </li>
              <li>
                <code>README.md</code> The readme file for react-catalyst.
              </li>
              <li>
                <code>webpack.config.dev.js</code> is the Webpack configuration file that is run
                when we run the application in development mode. It provides hot reloading, sitemaps
                and ommits minification to provide a better developer experience whilst coding your
                application.
              </li>
              <li>
                <code>webpack.config.prod.js</code> is the Webpack configuration for Production
                Bundling. This minifies the code, transforms it for backward compatibility, splits
                the dependencies into seperate modules for a better caching and loading experience
                and much more.
              </li>
            </ul>
            <hr />
            <h5>Clearing out the Welcome Page code.</h5>
            <p>
              As shown in the screenshot in the installation section above, out of the box
              React-Catalyst has a welcome screen with a spinning cube and some copytext that
              highlights some of the features included. Obviously we dont need this for our To-Do
              application so lets begin by removing the unnecessary files.
            </p>
            <p>
              We'll start by making our To-do view component, then we can replace the React-Catalyst
              welcome screen with that.
            </p>
            <p>
              Create a folder called <code>todoView</code> in the <code>src/components</code>{' '}
              folder. Inside your new folder create a file called <code>todoView.jsx</code> with the
              following code:
            </p>
            <Code filename="src/todoView/todoView.jsx" language="JSX">
              {`
import React from 'react'

const TodoView = () => {
  return (
    <div>
      <p>Our App Starts Here</p>
    </div>
  )
}

export default TodoView
              `}
            </Code>
            <p>
              We will build on this later on, for now it just enables us to remove the welcome page
              code without breaking things, so lets go ahead and remove what we dont need.
            </p>
            <p>
              In the <code>src/app.jsx</code> component we need to import the file we just created
              and replace the HomeContent component. Open <code>src/app.jsx</code> and make the
              following changes (remove lines shown in red, add lines shown in green - toggle off
              the Show Diff switch to see what you should be left with after your changes):
            </p>
            <Code filename="src/app.jsx" language="JSX">
              {`
import { hot } from 'react-hot-loader/root'
import React from 'react'
import { Switch, Route } from 'react-router-dom'

-import HomeContent from './components/home/home'
+import TodoView from './components/todoView/todoView'

import './public/css/normalize.css'
import './public/css/scrollbars.scss'
import './public/css/typography.scss'
import './global.scss'

const App = () => {
  return (
    <Switch>
-      <Route exact path="/" component={HomeContent} />
+      <Route exact path="/" component={TodoView} />
    </Switch>
  )
}

export default hot(App)
              `}
            </Code>
            <p>For now we are just stubbing in some text where our layout will be.</p>
            <p>
              With our replacement route in place we can go ahead and delete the React-Catalyst
              welcome page. Remove the <code>home</code> folder and all its contents from the{' '}
              <code>src/components</code> directory.
            </p>
            <p>
              We also no longer need the <code>svgIcon</code> component so you can delete the{' '}
              <code>common</code> folder and its contents from the <code>src/components</code>{' '}
              directory.
            </p>
            <p>
              Now we have our first component in place, the <code>todoView.jsx</code> file. We are
              ready to start writing our app.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="Layout">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#Layout"
            >
              Creating The Layout
            </SectionHeading>
            <p>
              In our application we will have a header at the top of the view with the app title and
              2 buttons: One to toggle the Filter widget, One to take the user to the Settings page.
            </p>
            <p>The body of our application will house the To-do items added by the user.</p>
            <p>
              At the bottom of the application we will have a textarea that enables the user to
              enter the to-do text and a plus button to add it to the to-do list.{' '}
            </p>
            <p>
              In order to create a layout we will use the sister library of React-Catalyst, a UI
              library called "Catalyst Elements". To Install that, in your terminal, go to your app
              root folder and type the following command:
            </p>
            <code>npm install --save @perpetualsummer/catalyst-elements</code>
            <p>
              Once NPM has installed that for you add the following to the <code>todoView.jsx</code>{' '}
              component:
            </p>
            <Code filename="src/todoView/todoView.jsx" language="JSX">
              {`
import React from 'react'
+import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
+import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
+import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
+import GridCell from '@perpetualsummer/catalyst-elements/GridCell'

const TodoView = () => {
  return (
-    <div>
-      <p>Our App Starts Here</p>
-    </div>
+    <FlexContainer 
+      justifyContent="center" 
+      alignItems="flex-end" 
+      alignContent="center"
+      wrap="wrap"
+    >
+      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
+        <GridContainer
+          grid="grid"
+          columns="1fr"
+          rows="100px 1fr 150px"
+          gap="10px"
+          padding="10px"
+          style={{ border: '3px solid red', height: '100vh' }}
+        >
+          <GridCell
+            columnStart="1"
+            columnEnd="1"
+            rowStart="1"
+            rowEnd="2"
+            style={{ border: '3px solid green' }}
+          ></GridCell>
+          <GridCell
+            columnStart="1"
+            columnEnd="1"
+            rowStart="2"
+            rowEnd="3"
+            style={{ border: '3px solid blue' }}
+          ></GridCell>
+          <GridCell
+            columnStart="1"
+            columnEnd="1"
+            rowStart="3"
+            rowEnd="4"
+            style={{ border: '3px solid pink' }}
+          ></GridCell>
+        </GridContainer>
+      </FlexCell>
+    </FlexContainer>
  )
}

export default TodoView
              `}
            </Code>
            <Lightbox image={BasicLayoutScreenshot}>
              <img className={styles.basicLayoutImage} src={BasicLayoutScreenshotThumb} />
            </Lightbox>
            <p>
              We have imported 4 components from the Catalyst-Elements library: FlexContainer,
              FlexCell, GridContainer and GridCell. The Flex Components centrally align and size the
              container, and the Grid components provide the containers for the Heading / Buttons,
              the central section we will place our to-dos and the bottom section that will house
              our component for adding more to-dos.
            </p>
            <p>
              We could acheive the same with styles and divs however if we are importing the
              Catalyst-Elements library for our input components we might as well use it for our
              layout as well.
            </p>
            <p>
              For more information and details on these layout components visit the
              Catalyst-Elements documentation site{' '}
              <a
                href="https://catalyst-elements.perpetualsummer.ltd/Layout"
                target="_blank"
                rel="noreferrer"
              >
                here
              </a>
            </p>
            <p>
              You'll note that we have injected some styles in each GridCell component, that is
              simply to illustrate where each component is positioned within the layout, we'll
              remove them later, but for now, you should have the base layout looking something like
              this (pictured right):
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="Header">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#Header"
            >
              The Header Component
            </SectionHeading>
            <p>
              Next we will create the Header Component. In the <code>src/components/</code> folder,
              create a new folder called <code>appHeader</code> and inside that create a new
              component file called <code>appHeader.jsx</code> with the following code:
            </p>
            <Code filename="src/components/appHeader/appHeader.jsx" language="JSX">
              {`
+import React from 'react'
+import styles from './appHeader.module'
+
+const AppHeader = () => {
+  return (
+    <header className={styles.headerContainer}>
+      <h1 className={styles.headerText}>
+        <strong>TODO APPp</strong>
+      </h1>
+    </header>
+  )
+}
+
+export default AppHeader
              `}
            </Code>
            <p>
              As you can see, we have a div with a classname and a h1 heading that has a classname.
              We are also importing a stylesheet which we have not yet created, create a new file in
              your new <code>src/components/appHeader</code> folder and name it{' '}
              <code>appHeader.module.scss</code>. Add the following styles:
            </p>
            <Code filename="src/components/appHeader/appHeader.module.scss" language="SCSS">
              {`
+.headerContainer{
+  display:flex;
+  background: #c0c0c0; 
+  height: 100%;
+}
+
+.headerText{
+  margin: 7px 30px;
+  text-align: left;
+  color:rgb(82, 82, 82);
+  text-shadow: 1px 1px 3px rgba(0,0,0,0.3), -1px -1px 3px rgba(255,255,255,0.4);
+}
              `}
            </Code>
            <p>
              In the stylesheet we are creating a classname for the header which gives it a
              background colour and ensures it fills the container by making it a flexbox.
            </p>
            <p>
              The header text is aligned left, has an adjusted margin, we specify a colour and also
              apply a couple of dropshadows to make it stand out a little.
            </p>
            <p>
              Now lets integrate this into our application, in <code>todoView.jsx</code> make the
              following changes:
            </p>
            <Code filename="src/components/todoView/todoView.jsx" language="JSX">
              {`
import React from 'react'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
+import AppHeader from '../appHeader/appHeader'

const TodoView = () => {
  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-end"
      alignContent="center"
      wrap="wrap"
      p={0}
    >
      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
        <GridContainer
          grid="grid"
          columns="1fr"
          rows="100px 1fr 150px"
          gap="10px"
          style={{
            backgroundColor: 'white',
-            border: '3px solid red',
            height: '100vh'
          }}
        >
          <GridCell
            columnStart="1"
            columnEnd="1"
            rowStart="1"
            rowEnd="2"
-            style={{ border: '3px solid green' }}
          >
+            <AppHeader />
          </GridCell>
          <GridCell
            columnStart="1"
            columnEnd="1"
            rowStart="2"
            rowEnd="3"
            style={{ border: '3px solid blue' }}
          ></GridCell>
          <GridCell
            columnStart="1"
            columnEnd="1"
            rowStart="3"
            rowEnd="4"
            style={{ border: '3px solid pink' }}
          ></GridCell>
        </GridContainer>
      </FlexCell>
    </FlexContainer>
  )
}

export default TodoView
              `}
            </Code>
            <p>
              As you can see we have imported the component we have just created and removed the
              border styles from the GridContainer and the first instance of GridCell and added our
              new <code>AppHeader</code> component as a child of that GridCell. Later on we will add
              enhancements to the <code>AppHeader</code> component such as the ability to filter
              To-Do's based on their state (todo / done / archived etc) and a button to take us to a
              settings page, but for now all we need is the header text which is what these changes
              give us.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="AddTodo">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#AddTodo"
            >
              Add Todo Component
            </SectionHeading>
            <p>
              Next we will add the Textarea Input that the user will need in order to add To-Do's to
              their To-Do list. It will comprise of a standard Textarea and a button that submits
              the entered test when clicked.
            </p>
            <p>
              Create a folder in your <code>src/components</code> folder called <code>addTodo</code>{' '}
              then inside that a file called <code>addTodo.jsx</code> with the following code:
            </p>
            <Code filename="src/components/addtodo/addTodo.jsx" language="JSX">
              {`
+import React, { useState } from 'react'
+import InputTextarea from '@perpetualsummer/catalyst-elements/InputTextarea'
+import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
+import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
+import Button from '@perpetualsummer/catalyst-elements/Button'
+import styles from './addTodo.module.scss'
+
+const AddTodo = () => {
+  const [inputValue, setInputValue] = useState('')
+
+  return (
+    <FlexContainer
+      justifyContent="center"
+      alignItems="flex-start"
+      alignContent="center"
+      wrap="nowrap"
+      className={styles.addTodoContainer}
+    >
+      <FlexCell alignSelf="stretch" xs={10} sm={10} md={10} lg={10} xl={10}>
+        <InputTextarea
+          label="Add A To-Do"
+          id="AddToDo"
+          value={inputValue}
+          onChange={(e) => setInputValue(e.target.value)}
+          width="100%"
+          rows={5}
+          resize={false}
+        />
+      </FlexCell>
+      <FlexCell alignSelf="stretch" xs={2} sm={2} md={2} lg={2} xl={2} pl={4}>
+        <Button className={styles.submitButton}>
+          <svg
+            xmlns="http://www.w3.org/2000/svg"
+            width="50"
+            height="50"
+            x="0"
+            y="0"
+            enableBackground="new 0 0 349.03 349.031"
+            viewBox="0 0 349.03 349.031"
+            xmlSpace="preserve"
+          >
+            <path
+              fill="white"
+              d="M349.03 141.226v66.579a9.078 9.078 0 01-9.079 9.079H216.884v123.067a9.077 9.077 0 01-9.079 9.079h-66.579c-5.009 0-9.079-4.061-9.079-9.079V216.884H9.079A9.08 9.08 0 010 207.805v-66.579a9.079 9.079 0 019.079-9.079h123.068V9.079c0-5.018 4.069-9.079 9.079-9.079h66.579a9.078 9.078 0 019.079 9.079v123.068h123.067a9.077 9.077 0 019.079 9.079z"
+            ></path>
+          </svg>
+        </Button>
+      </FlexCell>
+    </FlexContainer>
+  )
+}
+
+export default AddTodo
              `}
            </Code>
            <p>
              There is quite a lot going on in this component so I'll give a brief overview of what
              does what.
            </p>
            <p>
              First we import React, as we do for each component. Then we import Four components
              from the Catalyst-Elements library:{' '}
              <a
                href="https://catalyst-elements.perpetualsummer.ltd/Input#InputTextarea"
                target="_blank"
                rel="noreferrer"
              >
                InputTextarea
              </a>
              ,{' '}
              <a
                href="https://catalyst-elements.perpetualsummer.ltd/Layout#Flex"
                target="_blank"
                rel="noreferrer"
              >
                FlexContainer
              </a>
              ,{' '}
              <a
                href="https://catalyst-elements.perpetualsummer.ltd/Layout#Flex"
                target="_blank"
                rel="noreferrer"
              >
                FlexCell
              </a>{' '}
              and{' '}
              <a
                href="https://catalyst-elements.perpetualsummer.ltd/Input#Button"
                target="_blank"
                rel="noreferrer"
              >
                Button
              </a>
              .
            </p>
            <p>
              After the component declaration you will see we are using <code>useState</code> to
              store the value that is entered into the InputTextarea component, the{' '}
              <code>setInputValue</code> setter function is called every time the types a character
              into the InputTextarea. We will use this to submit it to our application state when
              adding a To-Do. If you are not familiar with the useState hook, please refer to this
              documentation,{' '}
              <a href="https://reactjs.org/docs/hooks-state.html" target="_blank" rel="noreferrer">
                here
              </a>
            </p>
            <p>
              The Flex component pair provide the components layout. If you note the props ('xs',
              'sm', 'lg' etc), these ensure that the InputTextarea will take up 5/6ths of the width
              of the app, the remaining 6th with be used by the button. These components use a 12
              point grid system. If you are not familiar with this common pattern, click the
              component links above to read documentation on what each of the props we have used
              does, its all available in the Catalyst-Elements documentation site.
            </p>
            <p>
              The content within the Button is an SVG that provides the 'Plus' sign. Later on we
              will add a function to the Button component to enable us to add a To-Do but first we
              have to create the state in Redux which we'll cover in the next chapter.
            </p>
            <p>
              We will also need to create the SCSS module file referenced above. Create a file
              called <code>addTodo.module.scss</code> with the following styles:
            </p>
            <Code filename="src/components/addtodo/addTodo.module.scss" language="SCSS">
              {`
+.addTodoContainer{
+  padding:10px;
+}
+
+.submitButton{
+  height:100%;
+  width:100%;
+}
              `}
            </Code>
            <p>
              Now we have our AddTodo component, lets add it to our app, make the following changes
              to <code>todoView.jsx</code>:
            </p>
            <Code filename="src/components/todoView/todoView.jsx" language="JSX">
              {`
import React from 'react'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
import AppHeader from '../appHeader/appHeader'
+import AddTodo from '../addTodo/addTodo'

const TodoView = () => {
  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-end"
      alignContent="center"
      wrap="wrap"
      p={0}
    >
      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
        <GridContainer
          grid="grid"
          columns="1fr"
          rows="100px 1fr 150px"
          gap="10px"
          style={{
            backgroundColor: 'white',
            height: '100vh'
          }}
        >
          <GridCell
            columnStart="1"
            columnEnd="1"
            rowStart="1"
            rowEnd="2"
          >
            <AppHeader />
          </GridCell>
          <GridCell
            columnStart="1"
            columnEnd="1"
            rowStart="2"
            rowEnd="3"
            style={{ border: '3px solid blue' }}
          ></GridCell>
          <GridCell
            columnStart="1"
            columnEnd="1"
            rowStart="3"
            rowEnd="4"
-            style={{ border: '3px solid pink' }}
          >
+            <AddTodo />
          </GridCell>
        </GridContainer>
      </FlexCell>
    </FlexContainer>
  )
}

export default TodoView
              `}
            </Code>

            <Lightbox image={addTodoScreenshot}>
              <img className={styles.addTodoImage} src={addTodoScreenshotThumb} />
            </Lightbox>
            <p>Your app should now look like this (pictured right):</p>
            <p>
              At present it does not submit anything yet as it is not wired up to the apps global
              state, the value of the input is just stored at a component level in{' '}
              <code>addTodo.jsx</code>.
            </p>
            <p>
              In the next section of this tutorial we will consider what state our application needs
              to enable the user to add To-Dos, fliter them, remove them, archive them and delete
              them.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="ReduxState">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#ReduxState"
            >
              Building the Application State with Redux
            </SectionHeading>
            <p>
              In this section of the tutorial we are going to design and develop our application
              state. For this we are going to use the massively popular state container, Redux. If
              you want to learn more about how Redux works there are plenty of online resources and
              the Documentation is excellent with a number of examples, you can find that{' '}
              <a
                href="https://redux.js.org/introduction/getting-started"
                target="_blank"
                rel="noreferrer"
              >
                here
              </a>
              .
            </p>
            <p>
              In my personal experience using Redux, I've found it good practice, before you write a
              single line of code, to <em>think</em> about what shape your state object should be.
              Ask yourself what uses your data is going have and how is it going to change via user
              interactions or other means of data manipulation (such as endpoint calls).
            </p>
            <p>
              For example, we know each To-Do is going to have to have the following properties:{' '}
            </p>
            <p>A To-Do can:</p>
            <ul>
              <li>Be Created</li>
              <li>Be Deleted</li>
              <li>Be Archived</li>
              <li>Be set as "Done"</li>
            </ul>
            <p>
              In order to facilitate these features we need to ask ourselves what information does
              each To-Do need to store to be able to provide these features?
            </p>
            <p>Firstly we know there will be multiple To-Do's so we need to make an array.</p>
            <p>
              So in order to provide all of the things our To-Do needs we should make the shape of
              our state something like this:
            </p>
            <Code filename="Example To-Do state object">
              {`
const exampleTodoListState = [
  {
    ToDoText: "Take Out The Papers and the Trash",
    isDone: false,
    isArchived: false,
  },
  {
    ToDoText: "Get Some Spending Cash",
    isDone: false,
    isArchived: false,
  },
]
              `}
            </Code>
            <p>
              The above shows an array with two To-Do items. Each item has <code>ToDoText</code>{' '}
              which is the label description of what the To-Do is a reminder to do.{' '}
              <code>isDone</code> is a true / false value to be toggled when the user has done that
              item. <code>isArchived</code> is a true/false value to be toggled when the user no
              longer wants to display it in their To-Do list but the dont want to delete it either.
            </p>
            <p>
              Now if our user types something into the InputTextarea and clicks the Submit button in
              our <code>addTodo.jsx</code> component, that information will be added to this array.
            </p>
            <p>
              When we come to make our To-Do Component each will have an "archive" button, a "done"
              button, when clicked it will toggle the "isArchived" or "isDone" flag on that To-Do
              item respectively. It will also have a "delete" button which will remove it from the
              Array entirely.
            </p>
            <p>
              So that determines how we will manage our To-Do's. We can add items to the Redux state
              for other things such as settings later, for now we will focus on the To-Do list
              array. Lets start with the initial state.
            </p>
            <h6>
              <strong>Create the Initial State</strong>
            </h6>
            <p>
              In the <code>src/reducers</code> folder you should already have a{' '}
              <code>initialState.js</code> file from the example that comes with React-Catalyst
              welcome page. Lets start by updating that to suit our needs:
            </p>
            <Code filename="src/reducers/initialState.js" language="Javascript">
              {`
export default {
-  value: 0,
+  todoList: [],
}
              `}
            </Code>
            <p>
              As you can see, we've removed the counter value and replaced that with an empty array
              with the key <code>todoList</code>. This will start the application with an empty
              list.
            </p>
            <h6>
              <strong>Create Our First Action</strong>
            </h6>
            <p>
              Our first action will be <code>addTodo</code> a function that will be called when the
              user has entered some text and clicked the submit button on our{' '}
              <code>addTodo.jsx</code> component.
            </p>
            <p>
              Create a file called <code>todoListActions.js</code> in the <code>src/actions</code>{' '}
              directory with the following code:
            </p>
            <Code filename="src/actions/todoListActions.js" language="Javascript">
              {`
+import * as types from './actionTypes'
+
+export function addTodo(todoText) {
+  return {
+    type: types.ADD_TODO,
+    text: todoText,
+  }
+}
              `}
            </Code>
            <p>
              Here we are importing all action types (which we'll update in a moment) and exporting
              a function called <code>addTodo</code> which accepts a <code>todoText</code> parameter
              which will be the value of text we get from the InputTextarea component.
            </p>
            <p>
              Now lets update our action types constants. Open{' '}
              <code>src/action/actionTypes.js</code> and make the following changes:
            </p>
            <Code filename="src/actions/actionTypes.js" language="Javascript">
              {`
-export const INCREMENT_COUNT = 'INCREMENT_COUNT'
-export const DECREMENT_COUNT = 'DECREMENT_COUNT'
-export const RETURN_COUNT = 'RETURN_COUNT'

+export const ADD_TODO = 'ADD_TODO'
              `}
            </Code>
            <p>
              Here we have removed the old action types from the React-Catalyst example on the
              Welcome page and added our own <code>ADD_TODO</code> action type constant.
            </p>
            <h6>
              <strong>Create The Add To-Do Reducer</strong>
            </h6>
            <p>
              Next we will tie all the Redux boilerplate we made in the previous section together in
              the <code>todoListReducer.js</code> file which we will create in the{' '}
              <code>src/reducers/</code> folder:
            </p>
            <Code filename="src/reducers/todoListReducer.js" language="Javascript">
              {`
+import * as types from '../actions/actionTypes'
+import initialState from './initialState'
+
+export default function todoListReducer(state = initialState.todoList, action) {
+  switch (action.type) {
+    case types.ADD_TODO: {
+      return [
+        ...state,
+        {
+          ToDoText: action.text,
+          isDone: false,
+          isArchived: false,
+        },
+      ]
+    }
+
+    default:
+      return state
+  }
+}
`}
            </Code>
            <p>
              Again, we import all the action types from the <code>actionTypes.js</code> constants
              file (there will be more than the one we have now by the time we have finished)
            </p>
            <p>
              We import the initialState file too so we have a starting point - in this case, the
              empty todoList array.
            </p>
            <p>
              Next we declare our reducer, this is a function that will perform an operation on the
              state based on which action is called (as per the actionTypes file), in this first
              example, the action we call is <code>ADD_TODO</code>.
            </p>
            <p>
              Lets step through this line-by-line to explain what is happening in this return block:
            </p>
            <p>
              First we spread the state of the todoList as it currently stands with{' '}
              <code>...state,</code> This consists solely of the todoList array, it wont affect any
              other state elements.
            </p>
            <p>
              The next line, <code>todoList: [</code> states that we should overwrite anything that
              was in the <code>...state,</code> todo section with....
            </p>
            <code>
              <pre>{`...state,
{
  ToDoText: action.text,
  isDone: false,
  isArchived: false,
},`}</pre>
            </code>
            <p>
              Which adds a new object at the end with a <code>todoText</code> entry with the
              information passed into our action, and both <code>isDone</code> and{' '}
              <code>isArchived</code> set to false by default for new todos.
            </p>
            <p>
              To illustrate what the results of this action would be, lets say the user first loads
              our app, has no To-Dos and then adds a To-Do with the text 'Wash the Dishes', the
              state object would go from this...{' '}
            </p>
            <code>
              <pre>{`{
  todoList: []
}`}</pre>
            </code>
            <p>...to this... </p>
            <code>
              <pre>{`{
  todoList: [
    {
      ToDoText: 'Wash the dishes'
      isDone: false,
      isArchived: false,
    }
  ]
}`}</pre>
            </code>
            <p>
              Then, if they subsequently add another with the text 'Clean the kitchen', the state
              would read:
            </p>
            <code>
              <pre>{`{
  todoList: [
    {
      ToDoText: 'Wash the dishes'
      isDone: false,
      isArchived: false,
    },
    {
      ToDoText: 'Clean the kitchen'
      isDone: false,
      isArchived: false,
    }
  ]
}`}</pre>
            </code>
            <p>
              And so on. Now we have our InitialState, Action and Reducer setup, we just need to add
              it to the Root Reducer, open the file <code>src/reducers/index.js</code> and make the
              following changes:
            </p>
            <Code filename="sr/reducers/index.js" language="Javascript">
              {`
import { combineReducers } from 'redux'
-import counter from './counterReducer'
+import todoList from './todoListReducer'
import { routerReducer as routing } from 'react-router-redux'

const rootReducer = combineReducers({
-  counter,
+  todoList,
  routing,
})

export default rootReducer
`}
            </Code>
            <p>
              As you can see we are taking the <code>counter</code> reducer out and replacing it
              with the reducer we just created. You can safely delete{' '}
              <code>src/actions/counterActions.js</code> and{' '}
              <code>src/reducers/counterReducer.js</code> if you want (or keep them for reference).
            </p>
            <p>
              That is now ready, we have created our first action, now we need to wire that up to
              the <code>addTodo.jsx</code> component.
            </p>
            <h6>
              <strong>Connecting React Components to Redux Actions</strong>
            </h6>
            <p>
              Before we wire up our component I recommend you install a plugin that will help you
              understand Redux actions and see them in action. Download and install the Redux
              DevTools package for your prefered browser (
              <a
                href="https://chrome.google.com/webstore/detail/redux-devtools/lmhkpmbekcpmknklioeibfkpmmfibljd"
                target="_blank"
                rel="noreferrer"
              >
                Chrome
              </a>
              ,
              <a
                href="https://addons.mozilla.org/en-US/firefox/addon/reduxdevtools/"
                target="_blank"
                rel="noreferrer"
              >
                Firefox
              </a>{' '}
              or{' '}
              <a
                href="https://microsoftedge.microsoft.com/addons/detail/redux-devtools/nnkgneoiohoecpdiaponcejilbhhikei"
                target="_blank"
                rel="noreferrer"
              >
                Edge
              </a>
              ).
            </p>
            <p>
              Now lets connect the InputTexarea component in our <code>addTodo.jsx</code> component,
              open the file and make the following changes:
            </p>
            <Code filename="src/components/addtodo/addTodo.jsx" language="JSX">
              {`
import React, { useState } from 'react'
+import { useDispatch } from 'react-redux'
+import { addTodo } from '../../actions/todoListActions'
import InputTextarea from '@perpetualsummer/catalyst-elements/InputTextarea'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import Button from '@perpetualsummer/catalyst-elements/Button'
import styles from './addTodo.module.scss'

const AddTodo = () => {
+  const dispatch = useDispatch()
  const [inputValue, setInputValue] = useState('')

  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-start"
      alignContent="center"
      wrap="nowrap"
      className={styles.addTodoContainer}
    >
      <FlexCell alignSelf="stretch" xs={10} sm={10} md={10} lg={10} xl={10}>
        <InputTextarea
          label="Add A To-Do"
          id="AddToDo"
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
          width="100%"
          rows={5}
          resize={false}
        />
      </FlexCell>
      <FlexCell alignSelf="stretch" xs={2} sm={2} md={2} lg={2} xl={2} pl={4}>
-        <Button className={styles.submitButton}>
+        <Button className={styles.submitButton} onClick={() => dispatch(addTodo(inputValue))}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="50"
            height="50"
            x="0"
            y="0"
            enableBackground="new 0 0 349.03 349.031"
            viewBox="0 0 349.03 349.031"
            xmlSpace="preserve"
          >
            <path
              fill="white"
              d="M349.03 141.226v66.579a9.078 9.078 0 01-9.079 9.079H216.884v123.067a9.077 9.077 0 01-9.079 9.079h-66.579c-5.009 0-9.079-4.061-9.079-9.079V216.884H9.079A9.08 9.08 0 010 207.805v-66.579a9.079 9.079 0 019.079-9.079h123.068V9.079c0-5.018 4.069-9.079 9.079-9.079h66.579a9.078 9.078 0 019.079 9.079v123.068h123.067a9.077 9.077 0 019.079 9.079z"
            ></path>
          </svg>
        </Button>
      </FlexCell>
    </FlexContainer>
  )
}

export default AddTodo
              `}
            </Code>
            <Lightbox image={reduxDevToolsScreenshot}>
              <img className={styles.reduxDevtoolsImage} src={reduxDevToolsScreenshotThumb} />
            </Lightbox>
            <p>
              Now (assuming you have installed Redux DevTools) if you right-click the application in
              your browser, select 'Inspect Element' you should have a new tab available labeled
              'Redux'. Click that and you should have something that looks like this (pictured
              right):
            </p>
            <p>
              If you add a To-Do whilst you have the Redux DevTools tab open you will see a new
              action in the left column then you can use the buttons in the top-right to see what
              action was called, the state after it is called and the diff. This tool is great for
              understanding and debugging your Redux Actions and Reducers. We'll be using it more
              throughout the tutorial as we add more features.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="TodoItem">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#TodoItem"
            >
              To-Do Item Component
            </SectionHeading>
            <p>
              Now that we have the ability to add a To-Do to our application state we need a way to
              display that in the UI. We'll also need each To-Do to have the ability to be set as
              "Done" and also the ability to "Archive" it too, we will include buttons for these as
              we will be adding actions for these features later.
            </p>
            <p>
              Create a new folder in <code>src/components</code> called <code>todoList</code> and in
              there create a new file called <code>todoList.jsx</code>. This will contain all of our
              To-Do items, for now it will just be a simple list but we'll expand on this later.
            </p>
            <Code filename="src/components/todoList/todoList.jsx" language="JSX">
              {`
+import React from 'react'
+import propTypes from 'prop-types'
+import styles from './todoList.module'
+
+const TodoList = ({ children }) => {
+  return <ul className={styles.todoList}>{children}</ul>
+}
+
+TodoList.propTypes = {
+  children: propTypes.node.isRequired,
+}
+
+export default TodoList

              `}
            </Code>
            <p>
              All we are doing in this component is creating an Unordered List (<code>ul</code>) as
              is semantically correct - our To-Do list is a list of things so we should reflect that
              in our code. We'll also need the style sheet to go with that so, in the same folder,
              create a file called <code>todoList.module.scss</code> with the following styles:
            </p>
            <Code filename="src/components/todoList/todoList.module.scss" language="SCSS">
              {`
+.todoList{
+  margin:0 10px;
+  padding:0;
+}
              `}
            </Code>
            <p>
              You'll notice here that we have added the propTypes library to this component. This is
              the first component we have written that uses props so we are using the propTypes
              library to ensure that the properties that pass into this component are of the
              expected type, in this case we specify that the <code>children</code> prop should be a
              node. We will use this on all of the components that use props to ensure any missing
              or incorrect property types get flagged in the browser console.
            </p>
            <p>
              Next we will make the todoItem component that will be rendered as children for each of
              our To-Dos in the state. Create a new folder in <code>src/components/todoList</code>{' '}
              called <code>todoItem</code> and inside that folder create a file called{' '}
              <code>todoItem.jsx</code> with the following code:
            </p>
            <Code filename="src/components/todoList/todoItem/todoItem.jsx" language="JSX">
              {`
+import React from 'react'
+import propTypes from 'prop-types'
+
+const TodoItem = ({ todoData }) => {
+  const { ToDoText } = todoData
+
+  return (
+    <li>
+      <p>{ToDoText}</p>
+    </li>
+  )
+}
+
+TodoItem.propTypes = {
+  todoData: propTypes.shape({
+    ToDoText: propTypes.string.isRequired,
+    isDone: propTypes.bool.isRequired,
+    isArchived: propTypes.bool.isRequired,
+  }).isRequired,
+}
+
+export default TodoItem
              `}
            </Code>
            <p>
              Lets go through a few points worth highlighting in this component. First we import
              React and propTypes as we have seen before. In the component declaration (
              <code>{`const TodoItem = ({ todoData }) => {`}</code>) we are passing in an object
              called <code>todoData</code>. This will be the state object from redux which will be
              mapped to this component when we integrate it into <code>todoList.jsx</code>.
            </p>
            <p>
              Below that we are using
              <a
                href="https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment"
                target="_blank"
                rel="noreferrer"
              >
                Object Destructuring
              </a>{' '}
              to assign the ToDoText specified in our Redux State to a variable called{' '}
              <code>ToDoText</code>. Later on we will expand upon this to get the{' '}
              <code>isDone</code> and <code>isArchived</code> values too but for now all we want to
              do is render the text.
            </p>
            <p>
              Then we return a basic list item (<code>{`<li>`}</code>) with a paragraph tag to
              display the ToDoText value.
            </p>
            <p>
              Finally we use propTypes again to describe the shape of the prop passed into this
              component. As you can see in the code above, it is a object that has the following
              shape:
            </p>
            <code>
              <pre>{`{
  ToDoText: string,
  isDone: bool,
  isArchived: bool,
}`}</pre>
            </code>
            <p>
              The Proptypes library will ensure that for each of these items that is rendered a{' '}
              <code>todoData</code> object is passed in that it has those props.
            </p>
            <p>
              Then finally we export this component for use. Next we'll map each ToDo in our Redux
              State to a instance of this component.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="MappingToState">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#MappingToState"
            >
              Mapping the To-Do Item Component to State
            </SectionHeading>
            <p>
              No we have our basic Redux State connected to our addTodo.jsx component giving us the
              ability to create To-Dos, and we have todoList and todoItem components ready to
              display them, lets wire all this up to give us the core functionality of our To-Do
              app.
            </p>
            <p>
              Open the <code>todoView.jsx</code> component so we can use the components we just made
              and connect them to our state:
            </p>
            <Code filename="src/components/todoView/todoView.jsx" language="JSX">
              {`
import React from 'react'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'
import AppHeader from '../appHeader/appHeader'
import AddTodo from '../addTodo/addTodo'
+import TodoList from '../todoList/todoList'
+import TodoItem from '../todoList/todoItem/todoItem'
+import { useSelector } from 'react-redux'

const TodoView = () => {
+  const todoItems = useSelector((state) => state.todoList)
  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-end"
      alignContent="center"
      wrap="wrap"
      p={0}
    >
      <FlexCell alignSelf="stretch" xs={12} sm={10} md={8} lg={8} xl={8}>
        <GridContainer
          grid="grid"
          columns="1fr"
          rows="100px 1fr 150px"
          gap="10px"
          style={{
            backgroundColor: 'white',
            height: '100vh',
          }}
        >
          <GridCell columnStart="1" columnEnd="1" rowStart="1" rowEnd="2">
            <AppHeader />
          </GridCell>
          <GridCell
            columnStart="1"
            columnEnd="1"
            rowStart="2"
            rowEnd="3"
            // style={{ border: '3px solid blue' }}
          >
+            <TodoList>
+              {todoItems.map((itemData, i) => (
+                <TodoItem key={\`todoItem_$\{i}\`} todoData={itemData} />
+              ))}
+            </TodoList>
          </GridCell>
          <GridCell columnStart="1" columnEnd="1" rowStart="3" rowEnd="4">
            <AddTodo />
          </GridCell>
        </GridContainer>
      </FlexCell>
    </FlexContainer>
  )
}

export default TodoView
              `}
            </Code>
            <Lightbox image={todoItemAddedScreenshot}>
              <img className={styles.todoItemAddedImage} src={todoItemAddedScreenshotThumb} />
            </Lightbox>
            <p>
              The first two additions import the components we just created{' '}
              <code>todoList.jsx</code> and <code>todoItem.jsx</code>. The third addition is a hook
              that comes with the React-Redux library that allows us to access the state. We use
              that in the fourth added line,{' '}
              <code>{`const todoItems = useSelector((state) => state.todoList)`}</code> which binds
              the <code>todoList</code> array in our Redux state to a variable called{' '}
              <code>todoItems</code>.
            </p>
            <p>
              Then, in our return block, we import our <code>{`<TodoList>`}</code> component and
              wrap that around a code block which takes the <code>todoItems</code> variable we
              assigned to our Redux <code>todoList</code> array and maps each item to a{' '}
              <code>{`<TodoItem>`}</code> component, passing in the itemData to the{' '}
              <code>todoData</code> prop.
            </p>
            <p>
              Now if you add a Todo item in the application you should see it render in the main
              section of the app, as pictured right:
            </p>
            <p>
              As you can see, it adds the text but it is completely unstyled and could do with some
              enhancement. We'll do just that in the next section and add some buttons to enable us
              to Archive each todo and set it as "Done".
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="StylingTodos">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#StylingTodos"
            >
              Styling the To-Do Item
            </SectionHeading>
            <p>
              To style the <code>todoItem.jsx</code> component we need to add some classes, open the
              file and add the following:
            </p>
            <Code filename="src/components/todoList/todoItem/todoItem.jsx" language="JSX">
              {`
import React from 'react'
import propTypes from 'prop-types'
+import styles from './todoItem.module.scss'

const TodoItem = ({ todoData }) => {
  const { ToDoText } = todoData

  return (
-    <li>
-      <p>{ToDoText}</p>
+    <li className={styles.todoListItem}>
+      <p className={styles.todoText}>{ToDoText}</p>
    </li>
  )
}

TodoItem.propTypes = {
  todoData: propTypes.shape({
    ToDoText: propTypes.string.isRequired,
    isDone: propTypes.bool.isRequired,
    isArchived: propTypes.bool.isRequired,
  }).isRequired,
}

export default TodoItem
              `}
            </Code>
            <p>
              We need to make that stylesheet so we can add the basic styles of the list of To-Do
              items:
            </p>
            <Code filename="src/components/todoList/todoItem/todoItem.module.scss" language="SCSS">
              {`
+.todoListItem{
+  list-style-type: none;
+  min-height:50px;
+  display: flex;
+  align-items: center;
+  padding:6px 12px;
+  margin-bottom:5px;
+  background-color: gainsboro;
+  border-radius: 15px;
+}
+.todoText{
+  font-size: 30px;
+  margin:0;
+}
              `}
            </Code>
            <p>
              For the TodoItem this removes the list-item bullet-point, it centrally aligns the text
              in the container and gives us a background and rounded edges. For the text we increase
              the font-size and remove the default margin for text.{' '}
            </p>
            <p>
              This gives us the basic style and shape of the TodoItem, lets add some of the feature
              elements of the UI: The 'Done' facility and the ability to archive items.
            </p>
            <Code filename="src/components/todoList/todoItem/todoItem.jsx" language="JSX">
              {`
import React from 'react'
import propTypes from 'prop-types'
+import InputCheckbox from '@perpetualsummer/catalyst-elements/InputCheckbox'
+import Button from '@perpetualsummer/catalyst-elements/Button'
import styles from './todoItem.module.scss'

const TodoItem = ({ todoData }) => {
-  const { ToDoText } = todoData
+  const { ToDoText, isDone, isArchived } = todoData

  return (
    <li className={styles.todoListItem}>
+      <div className={styles.checkboxContainer}>
+        <InputCheckbox checked={isDone} />
+      </div>
      <p className={styles.todoText}>{ToDoText}</p>
+      <div className={styles.archiveButtonContainer}>
+        <Button className={styles.archiveButton} warning={isArchived}>
+          {isArchived ? (
+            <svg
+              xmlns="http://www.w3.org/2000/svg"
+              fill="white"
+              width="40"
+              height="40"
+              viewBox="0 0 24 24"
+            >
+              <path fill="none" d="M0 0H24V24H0z"></path>
+              <path d="M20.55 5.22l-1.39-1.68A1.51 1.51 0 0018 3H6c-.47 0-.88.21-1.15.55L3.46 5.22C3.17 5.57 3 6.01 3 6.5V19a2 2 0 002 2h14c1.1 0 2-.9 2-2V6.5c0-.49-.17-.93-.45-1.28zM12 9.5l5.5 5.5H14v2h-4v-2H6.5L12 9.5zM5.12 5l.82-1h12l.93 1H5.12z"></path>
+            </svg>
+          ) : (
+            <svg
+              xmlns="http://www.w3.org/2000/svg"
+              fill="white"
+              width="40"
+              height="40"
+              viewBox="0 0 24 24"
+            >
+              <path fill="none" d="M0 0h24v24H0z"></path>
+              <path d="M20.54 5.23l-1.39-1.68C18.88 3.21 18.47 3 18 3H6c-.47 0-.88.21-1.16.55L3.46 5.23C3.17 5.57 3 6.02 3 6.5V19c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V6.5c0-.48-.17-.93-.46-1.27zM12 17.5L6.5 12H10v-2h4v2h3.5L12 17.5zM5.12 5l.81-1h12l.94 1H5.12z"></path>
+            </svg>
+          )}
+        </Button>
+      </div>
    </li>
  )
}

TodoItem.propTypes = {
  todoData: propTypes.shape({
    ToDoText: propTypes.string.isRequired,
    isDone: propTypes.bool.isRequired,
    isArchived: propTypes.bool.isRequired,
  }).isRequired,
}

export default TodoItem
              `}
            </Code>
            <p>
              Here we import a couple of components from Catalyst-Elements:{' '}
              <code>InputCheckbox</code> and <code>Button</code>. The checkbox component will
              represent whether the component is Done (or Not) and the Button component will be used
              to Archive or Unarchive the list Item. We have not wired these up to redux actions as
              yet, these changes just place these feature elements in the UI.
            </p>
            <p>
              We must also add some styles to layout these items, make the following changes to the
              stylesheet to acheive this:
            </p>
            <Code filename="src/components/todoList/todoItem/todoItem.module.scss" language="SCSS">
              {`
.todoListItem{
  list-style-type: none;
  min-height:50px;
  display: flex;
  align-items: center;
  padding:6px 12px;
  margin-bottom:5px;
  background-color: gainsboro;
  border-radius: 15px;
}
+.checkboxContainer{
+  flex: 0 0 auto;
+  display:flex;
+  justify-content: center;
+  width: 50px;
+}
.todoText{
+  flex: 1 1 auto;
  font-size: 30px;
  margin:0;
+  padding-left:10px;
}
+.archiveButtonContainer{
+  flex: 0 0 auto;
+  display:flex;
+  justify-content: center;
+  width: 50px;
+}
+.archiveButton{
+  min-width:60px;
+  padding:0;
+}
              `}
            </Code>
            <p>With those changes our Todo Item should now look like this:</p>
            <img className={styles.todoItemStyledImage} src={todoItemStyledScreenshot} />
            <p>
              With those UI elements added, in the next section we will create some redux actions to
              enable the ability to set a To-Do to "Done" and to Archive/Unarchive them.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="TodoReduxActions">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#TodoReduxActions"
            >
              Additional To-Do Redux Actions
            </SectionHeading>
            <p>
              In this section we will create some actions for the UI elements we added to the
              TodoItem component. The first step is to add our new actions to both the{' '}
              <code>actionTypes.js</code> and
              <code>todoListActions.js</code> files.{' '}
            </p>
            <Code filename="src/actions/actionTypes.js" language="Javascript">
              {`
export const ADD_TODO = 'ADD_TODO'
+export const TOGGLE_TODO_DONE = 'TOGGLE_TODO_DONE'
+export const TOGGLE_TODO_ARCHIVED = 'TOGGLE_TODO_ARCHIVED'
              `}
            </Code>
            <p>&nbsp;</p>
            <Code filename="src/actions/todoListActions.js" language="Javascript">
              {`
import * as types from './actionTypes'

export function addTodo(todoText) {
  return {
    type: types.ADD_TODO,
    text: todoText,
  }
}

+export function toggleTodoDone(todoText) {
+  return {
+    type: types.TOGGLE_TODO_DONE,
+    text: todoText,
+  }
+}
+
+export function toggleTodoArchived(todoText) {
+  return {
+    type: types.TOGGLE_TODO_ARCHIVED,
+    text: todoText,
+  }
+}
              `}
            </Code>
            <p>
              Now that we can trigger these actions, lets write our reducers to manipulate the data
              in our state.
            </p>
            <Code filename="src/reducers/todoListReducer.js" language="Javascript">
              {`
import * as types from '../actions/actionTypes'
import initialState from './initialState'

export default function todoListReducer(state = initialState.todoList, action) {
  switch (action.type) {
    case types.ADD_TODO: {
      return [
        ...state,
        {
          ToDoText: action.text,
          isDone: false,
          isArchived: false,
        },
      ]
    }
    
+    case types.TOGGLE_TODO_DONE: {
+      const index = this.state.findIndex(item => item.ToDoText === action.text)
+      return [
+        ...state.slice(0, index),
+        {
+           ...state[index],
+           isDone: !state[index].isDone,
+        },
+        ...state.slice(index + 1),
+      ]
+    }
+    
+    case types.TOGGLE_TODO_ARCHIVED: {
+      const index = this.state.findIndex(item => item.ToDoText === action.text)
+      return [
+        ...state.slice(0, index),
+        {
+           ...state[index],
+           isArchived: !state[index].isArchived,
+        },
+        ...state.slice(index + 1),
+      ]
+    }
+
    default:
      return state
  }
}
`}
            </Code>
            <p>
              These two new reducers both work in very similar ways. We want to select the To-do
              item to be changed, we do that by finding that item (by the text value) amongst the
              array in the Redux State here:
            </p>
            <code>{`const index = this.state.findIndex(item => item.ToDoText === action.text)`}</code>
            <p>
              Then in the return block, all the To-Do items before that specific item are returned
              on this line:
            </p>
            <code>...state.slice(0, index),</code>
            <p>
              Then the object representing the selected item is edited by returning that item...
            </p>
            <code>
              <pre>{`{
  ...state[index],`}</pre>
            </code>
            <p>
              ...then we change the appropriate data within it (in this example, the 'isArchived'
              boolean)...
            </p>
            <code>
              <pre>{`   isArchived: !state[index].isArchived,
},`}</pre>
            </code>
            <p>
              ...then finally we return every To-Do item after the item we want to change with...
            </p>
            <code>
              <pre>{`  ...state.slice(index + 1),`}</pre>
            </code>
            <p>
              And with those changes in place we should now have some working actions, all we need
              to do is connect them to the UI elements we added in the previous section.
            </p>
            <Code filename="src/components/todoList/todoItem/todoItem.jsx" language="JSX">
              {`
import React from 'react'
import propTypes from 'prop-types'
import InputCheckbox from '@perpetualsummer/catalyst-elements/InputCheckbox'
import Button from '@perpetualsummer/catalyst-elements/Button'
+import { useDispatch } from 'react-redux'
+import { toggleTodoDone, toggleTodoArchived } from '../../../actions/todoListActions'
import styles from './todoItem.module.scss'

const TodoItem = ({ todoData }) => {
+  const dispatch = useDispatch()
  const { ToDoText, isDone, isArchived } = todoData

  return (
    <li className={styles.todoListItem}>
      <div className={styles.checkboxContainer}>
-        <InputCheckbox checked={isDone} />
+        <InputCheckbox checked={isDone} onChange={() => dispatch(toggleTodoDone(ToDoText))} />
      </div>
      <p className={styles.todoText}>{ToDoText}</p>
      <div className={styles.archiveButtonContainer}>
-        <Button className={styles.archiveButton} warning={isArchived}>
+        <Button
+          className={styles.archiveButton}
+          warning={isArchived}
+          onClick={() => dispatch(toggleTodoArchived(ToDoText))}
+        >
          {isArchived ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="white"
              width="40"
              height="40"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0H24V24H0z"></path>
              <path d="M20.55 5.22l-1.39-1.68A1.51 1.51 0 0018 3H6c-.47 0-.88.21-1.15.55L3.46 5.22C3.17 5.57 3 6.01 3 6.5V19a2 2 0 002 2h14c1.1 0 2-.9 2-2V6.5c0-.49-.17-.93-.45-1.28zM12 9.5l5.5 5.5H14v2h-4v-2H6.5L12 9.5zM5.12 5l.82-1h12l.93 1H5.12z"></path>
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="white"
              width="40"
              height="40"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0h24v24H0z"></path>
              <path d="M20.54 5.23l-1.39-1.68C18.88 3.21 18.47 3 18 3H6c-.47 0-.88.21-1.16.55L3.46 5.23C3.17 5.57 3 6.02 3 6.5V19c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V6.5c0-.48-.17-.93-.46-1.27zM12 17.5L6.5 12H10v-2h4v2h3.5L12 17.5zM5.12 5l.81-1h12l.94 1H5.12z"></path>
            </svg>
          )}
        </Button>
      </div>
    </li>
  )
}

TodoItem.propTypes = {
  todoData: propTypes.shape({
    ToDoText: propTypes.string.isRequired,
    isDone: propTypes.bool.isRequired,
    isArchived: propTypes.bool.isRequired,
  }).isRequired,
}

export default TodoItem
              `}
            </Code>

            <Lightbox image={appWithTodosScreenshot}>
              <img className={styles.appWithTodosScreenshot} src={appWithTodosScreenshotThumb} />
            </Lightbox>
            <p>
              Here we are importing the <code>useDispatch</code> hook and our two new actions{' '}
              <code>toggleTodoDone</code> and <code>toggleTodoArchived</code>.
            </p>
            <p>
              We bind useDispatch to the <code>dispatch</code> constant for use in our components.
            </p>
            <p>
              We dispatch the toggleTodoDone action when the Checkbox is clicked by adding the
              onChange prop:
            </p>
            <code>{`onChange={() => dispatch(toggleTodoDone(ToDoText))}`}</code>
            <p>We also dispatch the toggleTodoArchived function when the Button is clicked:</p>
            <code>{`onClick={() => dispatch(toggleTodoArchived(ToDoText))}`}</code>
            <p>
              With those changes in place we should not be able to create To-Do's, set them to
              "Done" and Archive them. Your application should now look something like this
              (pictured).
            </p>
            <p>
              In the next section we will make some quality improvements before moving on to adding
              some filters and a settings page.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="QualityUsability">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#QualityUsability"
            >
              Usability and Quality Improvements
            </SectionHeading>
            <p>
              You might have noticed that when you type and submit a To-Do, after the To-Do is added
              to the list the text remains in the AddTodo component, lets fix that issue.
            </p>

            <Code filename="src/components/addtodo/addTodo.jsx" language="JSX">
              {`
import React, { useState } from 'react'
import { useDispatch } from 'react-redux'
import { addTodo } from '../../actions/todoListActions'
import InputTextarea from '@perpetualsummer/catalyst-elements/InputTextarea'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import Button from '@perpetualsummer/catalyst-elements/Button'
import styles from './addTodo.module.scss'

const AddTodo = () => {
  const dispatch = useDispatch()
  const [inputValue, setInputValue] = useState('')

+  const submitTodo = () => {
+    dispatch(addTodo(inputValue))
+    setInputValue('')
+  }

  return (
    <FlexContainer
      justifyContent="center"
      alignItems="flex-start"
      alignContent="center"
      wrap="nowrap"
      className={styles.addTodoContainer}
    >
      <FlexCell alignSelf="stretch" xs={10} sm={10} md={10} lg={10} xl={10}>
        <InputTextarea
          label="Add A To-Do"
          id="AddToDo"
          value={inputValue}
          onChange={(e) => setInputValue(e.target.value)}
          width="100%"
          rows={5}
          resize={false}
        />
      </FlexCell>
      <FlexCell alignSelf="stretch" xs={2} sm={2} md={2} lg={2} xl={2} pl={4}>
-        <Button className={styles.submitButton} onClick={() => dispatch(addTodo(inputValue))}>
+        <Button className={styles.submitButton} onClick={() => submitTodo()}>
          <svg
            xmlns="http://www.w3.org/2000/svg"
            width="50"
            height="50"
            x="0"
            y="0"
            enableBackground="new 0 0 349.03 349.031"
            viewBox="0 0 349.03 349.031"
            xmlSpace="preserve"
          >
            <path
              fill="white"
              d="M349.03 141.226v66.579a9.078 9.078 0 01-9.079 9.079H216.884v123.067a9.077 9.077 0 01-9.079 9.079h-66.579c-5.009 0-9.079-4.061-9.079-9.079V216.884H9.079A9.08 9.08 0 010 207.805v-66.579a9.079 9.079 0 019.079-9.079h123.068V9.079c0-5.018 4.069-9.079 9.079-9.079h66.579a9.078 9.078 0 019.079 9.079v123.068h123.067a9.077 9.077 0 019.079 9.079z"
            ></path>
          </svg>
        </Button>
      </FlexCell>
    </FlexContainer>
  )
}

export default AddTodo
              `}
            </Code>
            <p>
              In the changes above we create a new function that dispatches the <code>addTodo</code>{' '}
              action and then clears the input state by calling the useState function{' '}
              <code>setInputValue('')</code> to reset the value to an empty string. Now, whenever
              you submit a To-Do the input will clear.
            </p>
            <p>
              Another improvement we can make, is that when a To-Do is set to "Done", the only
              visual indicator of that is that the checkbox is checked. To improve on this we can
              set the text to be have strikethrough and lighten the text colour a little to visually
              differentiate them more from To-Dos that are not yet set to "Done". First we must make
              a small change to the <code>todoItem.jsx</code> component.
            </p>
            <Code filename="src/components/todoList/todoItem/todoItem.jsx" language="JSX">
              {`
import React from 'react'
import propTypes from 'prop-types'
import InputCheckbox from '@perpetualsummer/catalyst-elements/InputCheckbox'
import Button from '@perpetualsummer/catalyst-elements/Button'
import { useDispatch } from 'react-redux'
import { toggleTodoDone, toggleTodoArchived } from '../../../actions/todoListActions'
import styles from './todoItem.module.scss'

const TodoItem = ({ todoData }) => {
  const dispatch = useDispatch()
  const { ToDoText, isDone, isArchived } = todoData

  return (
    <li className={styles.todoListItem}>
      <div className={styles.checkboxContainer}>
        <InputCheckbox checked={isDone} onChange={() => dispatch(toggleTodoDone(ToDoText))} />
      </div>
-      <p className={styles.todoText}>{ToDoText}</p>
+      <p className={isDone ? styles.todoText_done : styles.todoText}>{ToDoText}</p>
      <div className={styles.archiveButtonContainer}>
        <Button
          className={styles.archiveButton}
          warning={isArchived}
          onClick={() => dispatch(toggleTodoArchived(ToDoText))}
        >
          {isArchived ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="white"
              width="40"
              height="40"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0H24V24H0z"></path>
              <path d="M20.55 5.22l-1.39-1.68A1.51 1.51 0 0018 3H6c-.47 0-.88.21-1.15.55L3.46 5.22C3.17 5.57 3 6.01 3 6.5V19a2 2 0 002 2h14c1.1 0 2-.9 2-2V6.5c0-.49-.17-.93-.45-1.28zM12 9.5l5.5 5.5H14v2h-4v-2H6.5L12 9.5zM5.12 5l.82-1h12l.93 1H5.12z"></path>
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="white"
              width="40"
              height="40"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0h24v24H0z"></path>
              <path d="M20.54 5.23l-1.39-1.68C18.88 3.21 18.47 3 18 3H6c-.47 0-.88.21-1.16.55L3.46 5.23C3.17 5.57 3 6.02 3 6.5V19c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V6.5c0-.48-.17-.93-.46-1.27zM12 17.5L6.5 12H10v-2h4v2h3.5L12 17.5zM5.12 5l.81-1h12l.94 1H5.12z"></path>
            </svg>
          )}
        </Button>
      </div>
    </li>
  )
}

TodoItem.propTypes = {
  todoData: propTypes.shape({
    ToDoText: propTypes.string.isRequired,
    isDone: propTypes.bool.isRequired,
    isArchived: propTypes.bool.isRequired,
  }).isRequired,
}

export default TodoItem
              `}
            </Code>
            <p>
              As you can see we are toggling the class used on the text of the todo to include a
              modifier when the To-Do is "Done". We will need to add that modifier to the{' '}
              <code>todoItem.module.scss</code> stylesheet.
            </p>
            <Code filename="src/components/todoList/todoItem/todoItem.module.scss" language="SCSS">
              {`
.todoListItem{
  list-style-type: none;
  min-height:50px;
  display: flex;
  align-items: center;
  padding:6px 12px;
  margin-bottom:5px;
  background-color: gainsboro;
  border-radius: 15px;
}
.checkboxContainer{
  flex: 0 0 auto;
  display:flex;
  justify-content: center;
  width: 50px;
}
.todoText{
  flex: 1 1 auto;
  font-size: 30px;
  margin:0;
  padding-left:10px;
}
+.todoText_done{
+  composes: todoText;
+  color:gray;
+  text-decoration: line-through;
+}
.archiveButtonContainer{
  flex: 0 0 auto;
  display:flex;
  justify-content: center;
  width: 50px;
}
.archiveButton{
  min-width:60px;
  padding:0;
}
              `}
            </Code>
            <p>
              We have added the modifier class <code>.todoText_done</code> which uses the CSS
              Modules feature <code>composes</code>, that takes the styles of an exisiting class and
              applies it to the class it is called in so, in effect, this change takes all the{' '}
              <code>.todoText</code> styles and adds a change of colour (to 'grey') and adds the{' '}
              <code>text-decoration: line-through;</code> which results in the following when a
              To-Do is set to "Done".
            </p>
            <img
              className={styles.todoWithStrikethroughScreenshot}
              src={todoWithStrikethroughScreenshot}
            />
            <p>
              That looks much better and behaves in a more user-friendly manner now. You might have
              noticed that if you refresh the application then all of your To-Dos are lost and you
              have to start again. Thats obviously not going to be satisfactory in a real-world
              To-Do app so that will be the next issue we will tackle.{' '}
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="PersistReduxState">
            <SectionHeading
              tooltipAlignment="bottom"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#PersistReduxState"
            >
              Persisting Redux State
              <br /> Across Refreshes and Reloads.
            </SectionHeading>
            <p>
              In this section we will make changes to ensure that if the user refreshes orcloses
              their browser and revisits the site at a later date then their To-Do items will remain
              in the application. We will acheive this by using a browser feature called
              localStorage. This is a way to store string values even after a page has been
              refreshed or the browser closed and reopened. LocalStorage has{' '}
              <a
                href="https://caniuse.com/mdn-api_window_localstorage"
                target="_blank"
                rel="noreferrer"
              >
                strong support in all browsers
              </a>
              .
            </p>
            <p>
              First we will make a new file for a couple of new functions we need to write{' '}
              <code>loadState</code> and <code>saveState</code>.
            </p>
            <p>
              Create a file called <code>localStorage.js</code> in the <code>src/store</code>{' '}
              directory with the following code:
            </p>
            <Code filename="src/store/localStorage.js" language="Javascript">
              {`
export const loadState = () => {
  try {
    const serializedState = localStorage.getItem('state')
    if (serializedState === null) {
      return undefined
    }
    return JSON.parse(serializedState)
  } catch (err) {
    return undefined
  }
}

export const saveState = (state) => {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem('state', serializedState)
  } catch (err) {
    // Ignore write errors.
  }
}
              `}
            </Code>
            <p>
              The <code>loadState</code> function gets the state string from our localStorage entry,
              converts it into JSON and returns it.
            </p>
            <p>
              The <code>saveState</code> function does the opposite, it gets the state from redux,
              converts it into a string then saves that in the localStorage of the browser.
            </p>
            <p>
              Lets integrate these into our app. First we need to set our application to save the
              state upon every change to localStorage, we can do that with the{' '}
              <code>store.subscribe</code> method. Open <code>src/index.js</code> and make the
              following changes:
            </p>
            <Code filename="src/index.js" language="JSX">
              {`
// WEB BUILD Entry point
// Used for both 'dev' mode and 'production' mode builds
import React from 'react'
import { render } from 'react-dom'

// APPLICATION ROOT COMPONENT
import App from './app'

// REACT ROUTER DOM
import { BrowserRouter } from 'react-router-dom'

// REDUX
import { Provider } from 'react-redux'
import configureStore from './store/configureStore'
+import { saveState } from './store/localStorage'
const store = configureStore()

+store.subscribe(() => {
+  saveState(store.getState())
+})

// CONTEXT
import ViewportContextProvider from './context/viewportContextProvider'

{
  /* 
  const basenameVar = process.env.NODE_ENV == 'production' ? '/projects/catalyst2/' : '/'
  <BrowserRouter basename={basenameVar} history={customHistory}> 
  */
}
render(
  <Provider store={store}>
    <BrowserRouter>
      <ViewportContextProvider>
        <App />
      </ViewportContextProvider>
    </BrowserRouter>
  </Provider>,
  document.getElementById('application')
)

              `}
            </Code>
            <p>
              Now, whenever the Redux State is updated it will be stored in localStorage. We now
              need to load any state that is saved in localStorage into the app whenever it is
              started. We do that in the <code>configureStore</code> files.
            </p>
            <p>
              Before we do that its worth pointing out that in order to facilitate{' '}
              <a href="https://gaearon.github.io/react-hot-loader/getstarted/">hot-reloading</a> in
              React-Catalyst we actually have 2 configureStore files,{' '}
              <code>configureStore.dev.js</code> and <code>configureStore.prod.js</code>. When
              running the app in development mode it uses the <code>dev</code> file, when running a
              production build it uses the <code>prod</code> file so we will need to make our
              changes here in both files:
            </p>

            <Code filename="src/store/configureStore.dev.js" language="JSX">
              {`
import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from '../reducers'
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant'
import thunk from 'redux-thunk'
+import { loadState } from './localStorage'

export default function configureStore(initialState) {
+  const persistedState = loadState()
  const store = createStore(
    rootReducer,
-    initialState,
+    persistedState ? persistedState : initialState,
    compose(
      applyMiddleware(thunk, reduxImmutableStateInvariant()),
      window.__REDUX_DEVTOOLS_EXTENSION__ ? window.devToolsExtension() : (f) => f
    )
  )

  if (module.hot) {
    // Enable Webpack hot module replacement for reducers
    module.hot.accept('../reducers', () => {
      const nextRootReducer = require('../reducers/index')
      store.replaceReducer(nextRootReducer)
    })
  }

  return store
}
              `}
            </Code>
            <p>&nbsp;</p>
            <Code filename="src/store/configureStore.prod.js" language="JSX">
              {`
import { createStore, applyMiddleware, compose } from 'redux'
import rootReducer from '../reducers'
import thunk from 'redux-thunk'
+import { loadState } from './localStorage'

export default function configureStore(initialState) {
+  const persistedState = loadState()
  return createStore(
    rootReducer,
-    initialState,
+    persistedState ? persistedState : initialState,
    compose(
      applyMiddleware(thunk),
      window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : (f) => f
    )
  )
}
              `}
            </Code>
            <p>
              In both cases we are importing the <code>loadState</code> function, assigning its
              returned value to a variable called <code>persistedState</code> then adding that as a
              parameter to the createStore function which (as you might have guessed) creates the
              Redux Store. If the <code>persistedState</code> variable does not exist (which it will
              not on the users first run of the application) it returns the{' '}
              <code>initialState</code> instead.
            </p>
            <p>
              With that change made, whenever you refresh your application it should keep any
              To-Do's you have made. If you do not see this behaviour you may need to restart the
              React-Catalyst dev environment by going into the dev-server terminal, stopping it by
              pressing <code>CTRL+C</code> and then restaring it with the <code>npm start</code>{' '}
              command.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="AddingDeleteFeature">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#AddingDeleteFeature"
            >
              Adding a Delete To-Do Feature.
            </SectionHeading>
            <p>
              When fixing one issue by adding the ability to persist the state we've exposed another
              issue: The inability to delete To-Do items. This is a glaring ommission on my part but
              its easily remedied.
            </p>
            <p>
              In this section we'll add a button next to the Archive button to allow us to delete a
              To-Do from the Redux State. We'll start by adding the relevant action to our{' '}
              <code>actionTypes.js</code>:
            </p>
            <Code filename="src/actions/actionTypes.js" language="Javascript">
              {`
export const ADD_TODO = 'ADD_TODO'
+export const DELETE_TODO = 'DELETE_TODO'
export const TOGGLE_TODO_DONE = 'TOGGLE_TODO_DONE'
export const TOGGLE_TODO_ARCHIVED = 'TOGGLE_TODO_ARCHIVED'
              `}
            </Code>
            <p>
              Next we will add the action to our <code>todoListActions.js</code>
            </p>

            <Code filename="src/actions/todoListActions.js" language="Javascript">
              {`
import * as types from './actionTypes'

export function addTodo(todoText) {
  return {
    type: types.ADD_TODO,
    text: todoText,
  }
}

+export function deleteTodo(todoText) {
+  return {
+    type: types.DELETE_TODO,
+    text: todoText,
+  }
+}

export function toggleTodoDone(todoText) {
  return {
    type: types.TOGGLE_TODO_DONE,
    text: todoText,
  }
}

export function toggleTodoArchived(todoText) {
  return {
    type: types.TOGGLE_TODO_ARCHIVED,
    text: todoText,
  }
}
              `}
            </Code>
            <p>
              As you can see, the action is the same as others with only the action type changining.
              As we have done previously, we'll use the text as the key. Next we need to add the
              reducer.
            </p>
            <Code filename="src/reducers/todoListReducer.js" language="Javascript">
              {`
import * as types from '../actions/actionTypes'
import initialState from './initialState'

export default function todoListReducer(state = initialState.todoList, action) {
  switch (action.type) {
    case types.ADD_TODO: {
      return [
        ...state,
        {
          ToDoText: action.text,
          isDone: false,
          isArchived: false,
        },
      ]
    }
        
+    case types.DELETE_TODO: {
+      const index = state.findIndex((item) => item.ToDoText === action.text)
+      return [
+        ...state.slice(0, index),
+        ...state.slice(index + 1),
+      ]
+    }
    
    case types.TOGGLE_TODO_DONE: {
      const index = this.state.findIndex(item => item.ToDoText === action.text)
      return [
        ...state.slice(0, index),
        {
           ...state[index],
           isDone: !state[index].isDone,
        },
        ...state.slice(index + 1),
      ]
    }
    
    case types.TOGGLE_TODO_ARCHIVED: {
      const index = this.state.findIndex(item => item.ToDoText === action.text)
      return [
        ...state.slice(0, index),
        {
           ...state[index],
           isArchived: !state[index].isArchived,
        },
        ...state.slice(index + 1),
      ]
    }

    default:
      return state
  }
}
`}
            </Code>
            <p>
              As you can see, this is very similart to our Archive and Done reducers, we simply omit
              the object of the item we are changing entirely to remove it from the array.
            </p>
            <p>
              Now that we have our Delete action added, lets add that to the{' '}
              <code>todoItem.jsx</code> component.
            </p>
            <Code filename="src/components/todoList/todoItem/todoItem.jsx" language="JSX">
              {`
import React from 'react'
import propTypes from 'prop-types'
import InputCheckbox from '@perpetualsummer/catalyst-elements/InputCheckbox'
import Button from '@perpetualsummer/catalyst-elements/Button'
import { useDispatch } from 'react-redux'
-import { toggleTodoDone, toggleTodoArchived } from '../../../actions/todoListActions'
+import { toggleTodoDone, toggleTodoArchived, deleteTodo } from '../../../actions/todoListActions'
import styles from './todoItem.module.scss'

const TodoItem = ({ todoData }) => {
  const dispatch = useDispatch()
  const { ToDoText, isDone, isArchived } = todoData

  return (
    <li className={styles.todoListItem}>
      <div className={styles.checkboxContainer}>
        <InputCheckbox checked={isDone} onChange={() => dispatch(toggleTodoDone(ToDoText))} />
      </div>
      <p className={isDone ? styles.todoText_done : styles.todoText}>{ToDoText}</p>
+      <div className={styles.deleteButtonContainer}>
+        <Button
+          className={styles.deleteButton}
+          warning={true}
+          onClick={() => dispatch(deleteTodo(ToDoText))}
+        >
+          <svg
+            xmlns="http://www.w3.org/2000/svg"
+            fill="white"
+            width="40"
+            height="40"
+            viewBox="0 0 24 24"
+          >
+            <path fill="none" d="M0 0h24v24H0z"></path>
+            <path fill="none" d="M0 0h24v24H0V0z"></path>
+            <path d="M6 19c0 1.1.9 2 2 2h8c1.1 0 2-.9 2-2V7H6v12zm2.46-7.12l1.41-1.41L12 12.59l2.12-2.12 1.41 1.41L13.41 14l2.12 2.12-1.41 1.41L12 15.41l-2.12 2.12-1.41-1.41L10.59 14l-2.13-2.12zM15.5 4l-1-1h-5l-1 1H5v2h14V4z"></path>
+          </svg>
+        </Button>
+      </div>
      <div className={styles.archiveButtonContainer}>
        <Button
          className={styles.archiveButton}
          warning={isArchived}
          onClick={() => dispatch(toggleTodoArchived(ToDoText))}
        >
          {isArchived ? (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="white"
              width="40"
              height="40"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0H24V24H0z"></path>
              <path d="M20.55 5.22l-1.39-1.68A1.51 1.51 0 0018 3H6c-.47 0-.88.21-1.15.55L3.46 5.22C3.17 5.57 3 6.01 3 6.5V19a2 2 0 002 2h14c1.1 0 2-.9 2-2V6.5c0-.49-.17-.93-.45-1.28zM12 9.5l5.5 5.5H14v2h-4v-2H6.5L12 9.5zM5.12 5l.82-1h12l.93 1H5.12z"></path>
            </svg>
          ) : (
            <svg
              xmlns="http://www.w3.org/2000/svg"
              fill="white"
              width="40"
              height="40"
              viewBox="0 0 24 24"
            >
              <path fill="none" d="M0 0h24v24H0z"></path>
              <path d="M20.54 5.23l-1.39-1.68C18.88 3.21 18.47 3 18 3H6c-.47 0-.88.21-1.16.55L3.46 5.23C3.17 5.57 3 6.02 3 6.5V19c0 1.1.9 2 2 2h14c1.1 0 2-.9 2-2V6.5c0-.48-.17-.93-.46-1.27zM12 17.5L6.5 12H10v-2h4v2h3.5L12 17.5zM5.12 5l.81-1h12l.94 1H5.12z"></path>
            </svg>
          )}
        </Button>
      </div>
    </li>
  )
}

TodoItem.propTypes = {
  todoData: propTypes.shape({
    ToDoText: propTypes.string.isRequired,
    isDone: propTypes.bool.isRequired,
    isArchived: propTypes.bool.isRequired,
  }).isRequired,
}

export default TodoItem
              `}
            </Code>
            <p>
              Here you'll note that we've added a container div, with a instance of the
              Catalyst-Elements Button component. The container and button have classes{' '}
              <code>deleteButtonContainer</code> and <code>deleteButton</code> respectively. The
              Button has the warning prop set to true to colour it red and denote that this is a
              destructive action and it dispatches the <code>deleteTodo</code> action when clicked
              (which we have added to the imports). The SVG it has as a child is a Trash Can Icon
              from the Material UI Icon set.
            </p>
            <p>We just need to add a little more SCSS to finish up this section:</p>

            <Code filename="src/components/todoList/todoItem/todoItem.module.scss" language="SCSS">
              {`
.todoListItem{
  list-style-type: none;
  min-height:50px;
  display: flex;
  align-items: center;
  padding:6px 12px;
  margin-bottom:5px;
  background-color: gainsboro;
  border-radius: 15px;
}
.checkboxContainer{
  flex: 0 0 auto;
  display:flex;
  justify-content: center;
  width: 50px;
}
.todoText{
  flex: 1 1 auto;
  font-size: 30px;
  margin:0;
  padding-left:10px;
}
.todoText_done{
  composes: todoText;
  color:gray;
  text-decoration: line-through;
}
+.deleteButtonContainer,
.archiveButtonContainer{
  flex: 0 0 auto;
  display:flex;
  justify-content: center;
  width: 50px;
}
+.deleteButton,
.archiveButton{
  min-width:60px;
  padding:0;
}
+.deleteButton{
+  margin-right:8px;
+}
              `}
            </Code>
            <p>
              As you can see we are just adding the class selectors{' '}
              <code>.deleteButtonContainer</code> and <code>.deleteButton</code> to the styles we've
              already made for the archive button and its container. To add a bit of space we have
              also added a margin to the right of the delete Button to put a bit of space between
              that and the Archive button.
            </p>
            <p>
              Now with that change in place, if you refresh your app any To-Dos you have will be
              persisted but if you click the delete button then they will be removed. Easy!
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="Part1Summary">
            <Lightbox image={partOneComplete}>
              <img className={styles.partOneComplete} src={partOneCompleteThumb} />
            </Lightbox>
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Tutorial#Part1Summary"
            >
              Summary
            </SectionHeading>
            <p>
              With the Delete action added we now have the core functionality of our application in
              place. We can Add, Delete and Archive To-Do's in our list, we can set them to Done and
              any that are in our list when we close the app will be present when we reopen it
              again.
            </p>
            <p>
              In the Part 2 of the Tutorial we will add the ability to Filter the To-Do's based on
              their state (Archived or Done), we will add a settings page to demonstrate the usage
              of ReactRouter and the ability to toggle the theme either "Light" or "Dark" to
              demonstrate the use of React's Context API.
            </p>
            <p>
              I hope you have enjoyed this walkthrough of the practical usage of the React-Catalyst
              Development Environment and I hope you've learned something along the way. Thanks for
              following along and for using React-Catalyst.
            </p>
          </IntersectionWrapper>
        </ScrollIntoView>
      </div>
    </>
  )
}

export default Tutorial1

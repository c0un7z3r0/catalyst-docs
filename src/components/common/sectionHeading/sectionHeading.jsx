import React from 'react'
import propTypes from 'prop-types'
import Tooltip from '@perpetualsummer/catalyst-elements/Tooltip'
import CopyToClipboard from '@perpetualsummer/catalyst-elements/CopyToClipboard'
import SvgImg from '../svgIcon/svgIcon'
import styles from './sectionHeading.module'

/**
 * Component renders a section heading that can be clicked to copy a link to clipboard. It is composed of Tooltip and CopyToClipboard
 *
 * @component
 * @example
 * return (
 *  <SectionHeading
 *    tooltipAlignment="right"
 *   tooltipContent={'Copy Link To Clipboard'}
 *    clipboardContent="Some text to be added to the users clipboard on click"
 *  >
 *    A Heading for a section of text
 *  </SectionHeading>
 * )
 * @param {string} tooltipAlignment A string ('top', 'right', 'bottom' or 'left') that determines where the tooltip should be positioned relative to the content.
 * @param {node} tooltipContent A node containing the content to be shown in the tooltop wrapper when hovering over the content
 * @param {string} clipboardContent A string to be copied to clipboard upon clicking the content
 * @param {node} children Section heading content Content that requires a tooltip
 */

const SectionHeading = ({ tooltipAlignment, tooltipContent, clipboardContent, children }) => {
  return (
    <h5 className={styles.heading}>
      <CopyToClipboard clipboardContent={clipboardContent}>
        <strong>{children}</strong>
        <Tooltip
          alignment={tooltipAlignment}
          tooltipContent={tooltipContent}
          customStyle={{ fontSize: '14px !important', padding: '10px' }}
        >
          <div className={styles.linkIconContainer}>
            <SvgImg image="link" />
          </div>
        </Tooltip>
      </CopyToClipboard>
    </h5>
  )
}

SectionHeading.propTypes = {
  tooltipAlignment: propTypes.string.isRequired,
  tooltipContent: propTypes.node.isRequired,
  clipboardContent: propTypes.string.isRequired,
  children: propTypes.node.isRequired,
}

export default SectionHeading

import { useEffect } from 'react'
import PropTypes from 'prop-types'
import { useLocation } from 'react-router-dom'
import ReactGA from 'react-ga4'

const GoogleAnalytics = ({ measurementId }) => {
  ReactGA.initialize(measurementId)
  const location = useLocation()
  useEffect(() => {
    ReactGA.send({ hitType: 'pageview', page: location.pathname })
  }, [location])

  return null
}

GoogleAnalytics.propTypes = {
  measurementId: PropTypes.string,
}

export default GoogleAnalytics

import React from 'react'
import propTypes from 'prop-types'
import styles from './pageHeadingSplash.module'
import useIntersectionObserver from '../../../hooks/useIntersectionObserver'
import { getScreenSize } from '../../../utilities/utilities'

/**
 * Component renders a parralax scrolling heading image with stylised heading text.
 *
 * @component
 * @example
 * import MyImageXS from '../../path/to/myImageXS_400.jpg'
 * import MyImageSM from '../../path/to/myImageSM_600.jpg'
 * import MyImageMD from '../../path/to/myImageMD_900.jpg'
 * import MyImageLG from '../../path/to/myImageLG_1200.jpg'
 * import MyImageXL from '../../path/to/myImageXL_1920.jpg'
 * const myImageSet = {
 *  xs: { url: MyImageXS },
 *  sm: { url: MyImageSM },
 *  md: { url: MyImageMD },
 *  lg: { url: MyImageLG },
 *  xl: { url: MyImageXL },
 *  default: { url: MyImageMD },
 * }
 * return (
 *   <PageHeadingSplash imageSet={myImageSet}>
 *     <h1>Some Heading Content</h1>
 *   </PageHeadingSplash>
 * )
 * @param {string} imageSet A object containing a series of objects with screensizes ('xl', 'lg' etc...) and their respective image paths.
 * @param {node} children A Heading content string to be used in the stylised heading
 */

const PageHeadingSplash = ({ children, imageSet }) => {
  // Parallax effect with intersectionObserver
  const buildThresholdArray = () => Array.from(Array(100).keys(), (i) => i / 100)
  const [ref, entry] = useIntersectionObserver({
    threshold: buildThresholdArray(),
  })

  // When component mounts use base styles, before intersectionRatio has computed
  let image = imageSet['default'].url
  let splashPositionStyles = {
    // backgroundImage: `url(${image})`, // dont load the default until its calced size
    backgroundPositionY: '0%',
  }
  // Once Intersection Observer has computed its values, use them to calculate position
  if (entry && entry.intersectionRatio > -1) {
    const percentile = entry.boundingClientRect.height / 100
    const totalPercentage = `${
      (entry.boundingClientRect.height - entry.boundingClientRect.top) / percentile - 100
    }%`
    // Update styles and image according to intersectionObservers computations...
    const screenSize = getScreenSize(entry.rootBounds.width)
    image = imageSet[screenSize].url
    splashPositionStyles = {
      backgroundImage: `url(${image})`,
      backgroundPositionY: totalPercentage,
    }
  }

  return (
    <div className={styles.splashContainer} ref={ref} style={splashPositionStyles}>
      <div className={styles.splashHeadingContainer}>
        <div className={styles.splashHeadingWrapper}>{children}</div>
      </div>
    </div>
  )
}

PageHeadingSplash.propTypes = {
  children: propTypes.node.isRequired,
  imageSet: propTypes.object.isRequired,
}

export default PageHeadingSplash

import React, { useState, useRef, useEffect } from 'react'
// import { useDispatch } from 'react-redux'
// import { notificationAddToastAndItem } from '../../../../actions/notificationsActions'
import propTypes from 'prop-types'
import styles from './swatches.module'

/**
 * Component renders a colour swatch with tooltip.
 * It composes the colour name with colour+modifier then
 * gets the colour value from css variables.
 *
 * @component
 * @example
 * return (
 *  <Swatch colour='primary' modifier='_darker'>
 * )
 * @param {string} colour
 * @param {string} modifier
 */

const Swatch = ({ colour, modifier }) => {
  // const dispatch = useDispatch()
  const [hovered, setHovered] = useState(false)
  const [colourValue, setColourValue] = useState('')
  const thisItem = useRef()

  useEffect(() => {
    const currentColour = getComputedStyle(thisItem.current).getPropertyValue(
      `--${colour}${modifier.modifier}`
    )
    setColourValue(currentColour)
  }, [colour, modifier])

  const copyToClipboard = () => {
    var clipboardTextElement = document.getElementById(
      `${colour}${modifier.modifier}_clipboardInput`
    )
    clipboardTextElement.select()
    clipboardTextElement.setSelectionRange(0, 99999)
    document.execCommand('copy')
    // dispatch(
    //   notificationAddToastAndItem(
    //     `"$${colour}${modifier.modifier}" `,
    //     `Copied To Clipboard`,
    //     'information'
    //   )
    // )
  }

  const copyToClipboardRightClick = (e) => {
    e.preventDefault()
    var clipboardTextElement = document.getElementById(
      `COLOURHEX_${colour}${modifier.modifier}_clipboardInput`
    )
    clipboardTextElement.select()
    clipboardTextElement.setSelectionRange(0, 99999)
    document.execCommand('copy')
    // dispatch(
    //   notificationAddToastAndItem(
    //     `"${colourValue}" `,
    //     `Copied To Clipboard`,
    //     'information'
    //   )
    // )
  }

  return (
    <div
      className={hovered ? styles.swatchHovered : styles.swatch}
      style={{
        backgroundColor: `var(--${colour}${modifier.modifier})`,
      }}
      ref={thisItem}
      onMouseEnter={() => setHovered(true)}
      onMouseLeave={() => setHovered(false)}
      onClick={() => copyToClipboard()}
      onContextMenu={(e) => copyToClipboardRightClick(e)}
    >
      <input
        className={styles.hiddenInput}
        type="text"
        readOnly={true}
        defaultValue={`$${colour}${modifier.modifier}`}
        id={`${colour}${modifier.modifier}_clipboardInput`}
      />
      <input
        className={styles.hiddenInput}
        type="text"
        readOnly={true}
        defaultValue={`${colourValue}`}
        id={`COLOURHEX_${colour}${modifier.modifier}_clipboardInput`}
      />

      {hovered ? (
        <div className={styles.tooltipContainer}>
          <div className={styles.tooltip}>
            <label>{`$${colour}${modifier.modifier}`}</label>
            <label>{colourValue}</label>
          </div>
        </div>
      ) : null}
    </div>
  )
}

Swatch.propTypes = {
  colour: propTypes.string.isRequired,
  modifier: propTypes.object.isRequired,
}

export default Swatch

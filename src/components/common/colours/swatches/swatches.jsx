import React from 'react'
import propTypes from 'prop-types'
import Swatch from './swatch'
import styles from './swatches.module'

/**
 * Component renders a column of colour swatches.
 *
 * @component
 * @example
 * return (
 *  <Swatches colour='primary'>
 * )
 * @param {string} colour
 */

const Swatches = ({ colour }) => {
  const themeColourModifiers = [
    { modifier: '_darkest' },
    { modifier: '_darker' },
    { modifier: '_dark' },
    { modifier: '' },
    { modifier: '_light' },
    { modifier: '_lighter' },
    { modifier: '_lightest' },
  ]

  return (
    <div className={styles.swatchColumn}>
      {themeColourModifiers.map((modifier, i) => (
        <Swatch colour={colour} modifier={modifier} key={`${colour}_${modifier}_${i}`} />
      ))}
    </div>
  )
}

Swatches.propTypes = {
  colour: propTypes.string.isRequired,
}

export default Swatches

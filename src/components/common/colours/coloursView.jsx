import React from 'react'
import styles from './coloursView.module'
import Swatches from './swatches/swatches'
import GridContainer from '@perpetualsummer/catalyst-elements/GridContainer'
import GridCell from '@perpetualsummer/catalyst-elements/GridCell'

/**
 * Component renders Colours View element.
 *
 * @component
 * @example
 * return (
 *  <Colours>
 * )
 */

const ColoursView = () => {
  return (
    <GridContainer
      grid="grid"
      columns="1fr 1fr"
      rows="3rem 1fr"
      gap="2rem"
      padding="2rem 2rem 6rem 2rem"
      className={styles.container}
    >
      <GridCell
        columnStart="1"
        columnEnd="2"
        rowStart="1"
        rowEnd="2"
        className={styles.themeHeader}
      >
        <h4>Theme Colours</h4>
      </GridCell>

      <GridCell
        columnStart="1"
        columnEnd="2"
        rowStart="2"
        rowEnd="3"
        className={styles.themeColoursContainer}
      >
        <Swatches colour={'primary'} />
        <Swatches colour={'secondary'} />
        <Swatches colour={'tertiary'} />
        <Swatches colour={'quaternary'} />
        <Swatches colour={'quinary'} />
        <Swatches colour={'senary'} />
      </GridCell>

      <GridCell
        columnStart="2"
        columnEnd="3"
        rowStart="1"
        rowEnd="2"
        className={styles.interactionHeader}
      >
        <h4>Interaction Colours</h4>
      </GridCell>
      <GridCell
        columnStart="2"
        columnEnd="3"
        rowStart="2"
        rowEnd="3"
        className={styles.interactionColoursContainer}
      >
        <Swatches colour={'destructive'} />
        <Swatches colour={'callout'} />
        <Swatches colour={'complement'} />
        <Swatches colour={'constructive'} />
        <Swatches colour={'greyscale'} />
      </GridCell>
    </GridContainer>
  )
}

export default ColoursView

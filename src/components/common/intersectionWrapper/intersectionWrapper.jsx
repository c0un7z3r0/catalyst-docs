import React, { useEffect, useRef, useState } from 'react'
import propTypes from 'prop-types'
import useIntersectionObserver from '../../../hooks/useIntersectionObserver'
import { updateIntersectionData } from '../../../actions/navMenuActions'
import { useDispatch } from 'react-redux'

/**
 * Component renders a Intersection Observer Hook Enhanced Container Element.
 *
 * @component
 * @example
 * return (
 *   <IntersectionWrapper id="Context">
 *     Some Content
 *   </IntersectionWrapper>
 * )
 * @param {string} id A unique Id string that applies to the container element.
 * @param {node} children Child html/jsx that comprises the content to be measured by intersectionObserver Hook
 */

const IntersectionWrapper = ({ id, children }) => {
  const wrapperRef = useRef(null)
  const dispatch = useDispatch()
  const [scrollY, setScrollY] = useState(0)
  const logOffsetTop = () => {
    setScrollY(window.pageYOffset)
  }
  useEffect(() => {
    const watchScroll = () => {
      window.addEventListener('scroll', logOffsetTop)
    }
    watchScroll()
    return () => {
      window.removeEventListener('scroll', logOffsetTop)
    }
  })

  const buildThresholdArray = () => Array.from(Array(100).keys(), (i) => i / 100)
  const [ref, entry] = useIntersectionObserver({
    // rootMargin: '30px',
    threshold: buildThresholdArray(),
  })

  useEffect(() => {
    if (entry && entry.intersectionRatio > -1) {
      const { top, height } = wrapperRef.current.getBoundingClientRect()
      const distanceFromTopAsPercentage = top > 0 ? 0 : (Math.abs(top) / height) * 100

      const isElementVisible = () => {
        var rect = wrapperRef.current.getBoundingClientRect()
        return (
          rect.bottom > 0 &&
          rect.right > 0 &&
          rect.left <
            (window.innerWidth ||
              document.documentElement.clientWidth) /* or $(window).width() */ &&
          rect.top < (window.innerHeight || document.documentElement.clientHeight)
        )
      }

      dispatch(
        updateIntersectionData({
          id: id,
          intersectionRatio: !isElementVisible() ? 0 : entry.intersectionRatio * 100,
          alignment: 'top',
          percentFromTop: distanceFromTopAsPercentage > 100 ? 0 : distanceFromTopAsPercentage,
        })
      )
    }
  }, [wrapperRef, dispatch, id, entry, scrollY])

  return (
    <div ref={wrapperRef}>
      <div ref={ref} id={id}>
        {children}
      </div>
    </div>
  )
}

IntersectionWrapper.propTypes = {
  id: propTypes.string.isRequired,
  children: propTypes.node.isRequired,
}

export default IntersectionWrapper

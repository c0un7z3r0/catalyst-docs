import React from 'react'
import propTypes from 'prop-types'
import { useLocation } from 'react-router-dom'
import smoothScrollIntoViewIfNeeded from 'smooth-scroll-into-view-if-needed'

/**
 * Component renders a scrollIntoView container, the following will scroll into view if a hashlink is clicked (<a href="/ThisPagePath#Test">TEST</a>).
 *
 * @component
 * @example
 * return (
 *   <ScrollIntoView>
 *     <p id="Test">
 *       This is some content
 *     </p>
 *   </ScrollIntoView>
 * )
 * @param {node} children Content that the user wishes to scroll into view
 */

const ScrollIntoView = ({ children }) => {
  const location = useLocation()
  if (location.hash !== '') {
    const element = document.querySelector(location.hash)
    if (element) {
      smoothScrollIntoViewIfNeeded(element, {
        behavior: 'smooth',
        block: 'start',
        inline: 'end',
      })
      // //If Firefox...
      // if (navigator.userAgent.indexOf('Firefox') > 0) {
      //   //...use native smooth scrolling.
      //   element.scrollIntoView({ block: 'end', behavior: 'smooth' })
      //   // If its any other browser, use custom polyfill...
      // } else {
      //   //... luckily I have this handy polyfill...
      //   smoothScrollIntoViewIfNeeded(element, {
      //     behavior: 'smooth',
      //     block: 'start',
      //     inline: 'end',
      //   })
      //   //  (⌐■_■
      // }
    }
  }
  return children
}

ScrollIntoView.propTypes = {
  children: propTypes.node.isRequired,
}

export default ScrollIntoView

import React from 'react'
import FlexContainer from '@perpetualsummer/catalyst-elements/FlexContainer'
import FlexCell from '@perpetualsummer/catalyst-elements/FlexCell'
import LoadingSpinner from '@perpetualsummer/catalyst-elements/LoadingSpinner'

const PageLoadingSpinner = () => {
  return (
    <FlexContainer
      justifyContent="center"
      alignItems="center"
      alignContent="center"
      direction="col"
    >
      <FlexCell justifyContent="center">
        <LoadingSpinner />
      </FlexCell>
    </FlexContainer>
  )
}

export default PageLoadingSpinner

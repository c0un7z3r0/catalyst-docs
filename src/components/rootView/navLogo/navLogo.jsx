import React from 'react'
import SvgImg from '../../common/svgIcon/svgIcon'
import styles from './navLogo.module'

/**
 * The SideMenu Heading Logo Component
 *
 * @component
 * @example
 * return (
 *   <NavLogo />
 * )
 */
const NavLogo = () => {
  return (
    <div className={styles.navLogoContainer}>
      <div className={styles.iconContainer}>
        <SvgImg image="catalyst-logo-text" />
      </div>
      <div className={styles.textContainer}></div>
    </div>
  )
}

export default NavLogo

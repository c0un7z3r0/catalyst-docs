import React from 'react'
import propTypes from 'prop-types'
import styles from './navMenu.module'

/**
 * A Navigation Menu Component
 *
 * @component
 * @example
 * return (
 *   <NavMenu>
 *     <NavItem label={'Introduction'} route="/">
 *       <NavSubItem hash="Purpose">Purpose</NavSubItem>
 *       <NavSubItem hash="Requirements">Requirements</NavSubItem>
 *       <NavSubItem hash="Installation">Installation</NavSubItem>
 *       <NavSubItem hash="Usage">Usage</NavSubItem>
 *     </NavItem>
 *   </NavMenu>
 * )
 * @param {node} children A series of NavItem and NavSubItem components
 */

const NavMenu = ({ children }) => {
  return <dl className={styles.navMenu}>{children}</dl>
}

NavMenu.propTypes = {
  children: propTypes.node.isRequired,
}

export default NavMenu

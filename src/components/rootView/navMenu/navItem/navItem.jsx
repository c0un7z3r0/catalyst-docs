import React, { Children, cloneElement } from 'react'
import propTypes from 'prop-types'
import { useRouteMatch, useLocation, useHistory } from 'react-router-dom'
import { useDispatch } from 'react-redux'
import SvgIcon from '../../../common/svgIcon/svgIcon'
import { flushIntersectionData } from '../../../../actions/navMenuActions'
import classNames from 'classnames/bind'
import styles from './navItem.module'
let cx = classNames.bind(styles)

/**
 * A Navigation Menu Item Component
 *
 * @component
 * @example
 * return (
 *   <NavItem label={'Introduction'} route="/">
 *     <NavSubItem hash="Purpose">Purpose</NavSubItem>
 *     <NavSubItem hash="Requirements">Requirements</NavSubItem>
 *     <NavSubItem hash="Installation">Installation</NavSubItem>
 *     <NavSubItem hash="Usage">Usage</NavSubItem>
 *   </NavItem>
 * )
 * @param {string} label A Text Label
 * @param {string} route A string with the current route (URL path)
 * @param {node} children A series of NavSubItem components
 */

const NavItem = ({ label, route, children }) => {
  let location = useLocation()
  let history = useHistory()
  let match = useRouteMatch({
    path: route,
    strict: true,
    sensitive: true,
    exact: true,
  })
  let dispatch = useDispatch()

  // Map the children to inject the 'route' property onto them
  const childrenWithProps = Children.map(children, (child, index) => {
    return cloneElement(child, {
      route,
    })
  })

  const onClick = (e) => {
    if (route === location.pathname) {
      e.preventDefault()
      history.push(route)
      window.scrollTo({ top: 0, behavior: 'smooth' })
    } else {
      dispatch(flushIntersectionData())
      history.push(route)
      window.scrollTo({ top: 0, behavior: 'smooth' })
    }
  }

  let navItemClasses = cx({
    navItem: true,
    active: location.pathname === route,
  })

  let activeIndicatorClasses = cx({
    activeIndicator: true,
    activeIndicatorActive: location.pathname === route,
  })

  return (
    <>
      <dt onClick={onClick} className={navItemClasses}>
        <div className={activeIndicatorClasses}>
          <SvgIcon image="menu-toggle-arrow" fill={'var(--tertiary_dark)'} />
        </div>
        {label}
      </dt>
      {match ? childrenWithProps : null}
    </>
  )
}

NavItem.propTypes = {
  label: propTypes.string.isRequired,
  route: propTypes.string.isRequired,
  children: propTypes.node,
}

export default NavItem

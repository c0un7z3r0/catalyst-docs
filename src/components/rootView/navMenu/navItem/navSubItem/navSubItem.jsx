import React from 'react'
import propTypes from 'prop-types'
import { useSelector } from 'react-redux'
import { useLocation, useHistory, Link } from 'react-router-dom'
import styles from './navSubItem.module'
import classNames from 'classnames/bind'
let cx = classNames.bind(styles)

/**
 * A Navigation Sub Menu Item Component
 *
 * @component
 * @example
 * return (
 *   <NavSubItem hash="Purpose">Purpose</NavSubItem>
 * )
 * @param {string} route A string with the parent route (URL path)
 * @param {string} hash A hash for the Id this navSubItem refers to
 * @param {node} children A Label for this navSubItem
 */

const NavSubItem = ({ route, hash, children }) => {
  const location = useLocation()
  let history = useHistory()

  const onClick = () => {
    history.push(`${route}#${hash}`)
  }

  let navSubItemClasses = cx({
    navSubItem: true,
    active: location.pathname === route && location.hash === `#${hash}`,
  })

  // Compute styles for viewport visibility indicator
  const viewportVisibility = useSelector((state) => state.navMenu.viewportVisibility)
  const viewportItem = viewportVisibility[hash]
  let progressStyles
  if (viewportItem != undefined) {
    progressStyles = {
      height: `${viewportItem.intersectionRatio}%`,
      top: `${viewportItem.percentFromTop}%`,
      bottom: `${viewportItem.percentFromBottom}%`,
      // bottom: viewportItem.alignment == 'bottom' ? '0' : 'unset',
    }
  }

  return (
    <dd
      onClick={onClick}
      className={navSubItemClasses}
      // data-testid={`${createTestId(children)}-navlink-subitem`}>
      role={`menuitem`}
      // name={`${createTestId(children)}`}
    >
      <Link to={`${route}#${hash}`}>
        <div className={styles.intersectionBar} style={progressStyles} />
        {children}
      </Link>
    </dd>
  )
}

NavSubItem.propTypes = {
  route: propTypes.string,
  hash: propTypes.string.isRequired,
  children: propTypes.node.isRequired,
}

export default NavSubItem

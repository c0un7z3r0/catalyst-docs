import React from 'react'
import styles from './404.module'

const FourOhFour = () => {
  return (
    <div className={styles.fourOhFour}>
      <div>
        <h1>404</h1>
        <h5>Page Not Found</h5>
      </div>
    </div>
  )
}

export default FourOhFour

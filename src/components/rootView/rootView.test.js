import React from 'react'
import { render, cleanup, getByTestId, fireEvent } from 'testUtils'
import RootView from './rootView'

afterEach(cleanup)

it('should take a snapshot', () => {
  const { asFragment } = render(<RootView />)
  expect(asFragment(<RootView />)).toMatchSnapshot()
})

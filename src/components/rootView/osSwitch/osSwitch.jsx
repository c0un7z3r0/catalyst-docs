import React from 'react'
import propTypes from 'prop-types'
import { useDispatch, useSelector } from 'react-redux'
import SvgImg from '../../common/svgIcon/svgIcon'
import styles from './osSwitch.module'
import { OS_OPTIONS } from '../../../constants/options'
import { selectOs } from '../../../actions/optionsActions'
import classNames from 'classnames/bind'
let cx = classNames.bind(styles)

/**
 * A Operating System Button
 *
 * @component
 * @example
 * return (
 *   <OsButton Os={'windows'} />
 * )
 * @param {string} Os A string representing the operating system selected
 */
const OsButton = ({ Os }) => {
  const dispatch = useDispatch()
  const selectOS = (os) => {
    dispatch(selectOs(os))
  }
  const selectedOS = useSelector((state) => state.options.selectedOS)

  let OsButtonClasses = cx({
    switchOption: true,
    active: selectedOS == Os,
  })

  return (
    <button className={OsButtonClasses} onClick={() => selectOS(Os)}>
      <SvgImg image={Os} />
    </button>
  )
}

/**
 * The Operating System Switch Feature
 *
 * @component
 * @example
 * return (
 *   <OsSwitch />
 * )
 */
const OsSwitch = () => {
  return (
    <div className={styles.switchContainer}>
      <OsButton Os={OS_OPTIONS.WINDOWS} />
      <OsButton Os={OS_OPTIONS.MACOS} />
      <OsButton Os={OS_OPTIONS.LINUX} />
    </div>
  )
}

OsButton.propTypes = {
  Os: propTypes.string.isRequired,
}

export default OsSwitch

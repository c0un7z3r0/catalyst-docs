import React from 'react'
import propTypes from 'prop-types'

const HamburgerIcon = ({ stroke = 'black' }) => {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 10 10">
      <path stroke={stroke} strokeLinecap="round" strokeWidth="0.7" d="M2 3L8 3"></path>
      <path stroke={stroke} strokeLinecap="round" strokeWidth="0.7" d="M2 5L8 5"></path>
      <path stroke={stroke} strokeLinecap="round" strokeWidth="0.7" d="M2 7L8 7"></path>
    </svg>
  )
}

HamburgerIcon.propTypes = {
  width: propTypes.string,
  height: propTypes.string,
  fill: propTypes.string,
  stroke: propTypes.string,
}

export default HamburgerIcon

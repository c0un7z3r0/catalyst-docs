import React, { useState, useRef } from 'react'
import styles from './sideNav.module'
import NavLogo from '../navLogo/navLogo'
import NavMenu from '../navMenu/navMenu'
import NavItem from '../navMenu/navItem/navItem'
import NavSubItem from '../navMenu/navItem/navSubItem/navSubItem'
import { isScreenSmallerThan } from '../../../utilities/utilities'
import useOnClickOutside from '../../../hooks/useOnClickOutside'
import classNames from 'classnames/bind'
import HamburgerIcon from './hamburgerIcon'
let cx = classNames.bind(styles)
import OsSwitch from '../osSwitch/osSwitch'

const SideNav = () => {
  const [menuIsOpen, setMenuIsOpen] = useState(false)
  let containerClasses = cx({
    navContainer: !isScreenSmallerThan('md'),
    navContainerMobile_open: isScreenSmallerThan('md') && menuIsOpen,
    navContainerMobile_closed: isScreenSmallerThan('md') && !menuIsOpen,
  })

  const ref = useRef()

  useOnClickOutside(ref, () => setMenuIsOpen(false))

  return (
    <>
      {isScreenSmallerThan('md') && (
        <div className={styles.burgerButton} onClick={() => setMenuIsOpen(!menuIsOpen)}>
          <HamburgerIcon />
        </div>
      )}
      <div className={containerClasses} ref={ref}>
        <div className={styles.navCollapseWrapper}>
          <div className={styles.logoCell}>
            <NavLogo />
          </div>

          <nav className={styles.navMenuCell}>
            <NavMenu>
              <NavItem label={'Introduction'} route="/">
                <NavSubItem hash="Purpose">Purpose</NavSubItem>
                <NavSubItem hash="SupportedTools">Supported Tools</NavSubItem>
                <NavSubItem hash="UsefulLinks">Useful Links</NavSubItem>
              </NavItem>
              <NavItem label={'Quick Start'} route="/QuickStart">
                <NavSubItem hash="Requirements">Requirements</NavSubItem>
                <NavSubItem hash="Installation">Installation</NavSubItem>
                <NavSubItem hash="Usage">Usage</NavSubItem>
              </NavItem>
              <NavItem label={'Features'} route="/Features">
                <NavSubItem hash="Context">Context</NavSubItem>
                <NavSubItem hash="Styles">Styles</NavSubItem>
                <NavSubItem hash="Redux">Redux</NavSubItem>
                <NavSubItem hash="Hooks">Hooks</NavSubItem>
                <NavSubItem hash="Media">Media Types</NavSubItem>
                <NavSubItem hash="Testing">Testing</NavSubItem>
                <NavSubItem hash="BundleSplitting">Bundle Splitting</NavSubItem>
                <NavSubItem hash="Development">Development Mode</NavSubItem>
                <NavSubItem hash="Production">Production Build</NavSubItem>
                <NavSubItem hash="Deployment">Deployment</NavSubItem>
              </NavItem>
              <NavItem label={'Tutorial Part 1'} route="/TutorialPart1">
                <NavSubItem hash={'Purpose'}>Purpose</NavSubItem>
                <NavSubItem hash={'Installation'}>Installation</NavSubItem>
                <NavSubItem hash={'Preparation'}>Preparation</NavSubItem>
                <NavSubItem hash={'Layout'}>Creating The Layout</NavSubItem>
                <NavSubItem hash={'Header'}>Header Component</NavSubItem>
                <NavSubItem hash={'AddTodo'}>Add To-Do Component</NavSubItem>
                <NavSubItem hash={'ReduxState'}>
                  Building the Application State with Redux
                </NavSubItem>
                <NavSubItem hash={'TodoItem'}>To-Do Item Component</NavSubItem>
                <NavSubItem hash={'MappingToState'}>
                  Mapping the To-Do Component to State
                </NavSubItem>
                <NavSubItem hash={'StylingTodos'}>Styling the To-Do Item Component</NavSubItem>
                <NavSubItem hash={'TodoReduxActions'}>Additional To-Do Redux Actions</NavSubItem>
                <NavSubItem hash={'QualityUsability'}>
                  Usability and Quality Improvements
                </NavSubItem>
                <NavSubItem hash={'PersistReduxState'}>Persisting Redux State</NavSubItem>
                <NavSubItem hash={'AddingDeleteFeature'}>Adding a Delete To-Do Feature</NavSubItem>
                <NavSubItem hash={'Part1Summary'}>Summary</NavSubItem>
              </NavItem>
              <NavItem label={'Tutorial Part 2'} route="/TutorialPart2">
                <NavSubItem hash={'SettingsRoute'}>Settings Page Route</NavSubItem>
                <NavSubItem hash={'ThemeContext'}>Theme Switch With Context API</NavSubItem>
                <NavSubItem hash={'ThemeStyles'}>Refactor Styles To Use Theme Context</NavSubItem>
                <NavSubItem hash={'AddingFilters'}>Adding Filters To Hide & Show To-Dos</NavSubItem>
              </NavItem>
              <NavItem label={'Dependencies'} route="/Dependencies">
                <NavSubItem hash={'WyleECayote'}>Wyle E Cayote</NavSubItem>
                <NavSubItem hash={'DaffyDuck'}>Daffy Duck</NavSubItem>
                <NavSubItem hash={'Tom'}>Tom</NavSubItem>
              </NavItem>
              <NavItem label={'Colours'} route="/Colours"></NavItem>
            </NavMenu>
          </nav>

          <div className={styles.osSwitchCell}>
            <OsSwitch />
          </div>
        </div>
      </div>
    </>
  )
}

export default SideNav

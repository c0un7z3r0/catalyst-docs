import React, { Fragment, lazy, Suspense, useState, useContext, useRef, useEffect } from 'react'
import { useLocation } from 'react-router-dom'
import propTypes from 'prop-types'
import styles from './rootView.module.scss'
import SvgImg from '../common/svgIcon/svgIcon'
import SideNav from './sideNav/sideNav'
import { isScreenSmallerThan } from '../../utilities/utilities'

/**
 * Component that creates the standard view layout for the documentation app.
 *
 * @component
 * @example
 * const childrenContent = {
 *  <>
 *     <h1>I am A Heading</h1>
 *     <p>I am some copytext</p>
 *  </>
 * }
 * return (
 *  <RootView>
 *    {childrenContent}
 *  </RootView>
 * )
 * @param {node} children Child html/jsx that comprises the view content
 */
const RootView = ({ children }) => {
  const myRef = useRef()
  const location = useLocation()

  // LazyLoad Broke the ability to type a hash link into address bar and have it load
  // with that target element in the viewport. This useEffect uses mutation observer
  // to see when the lazy loaded page has loaded then smoothScroll to that element
  useEffect(() => {
    // Select the node that will be observed for mutations
    if (myRef.current) {
      const targetNode = myRef.current
      // Options for the observer (which mutations to observe)
      const config = { attributes: false, childList: true, subtree: false }

      // Callback function to execute when mutations are observed
      const callback = function (mutationsList) {
        // Use traditional 'for loops' for IE 11
        // console.log(observer)
        for (const mutation of mutationsList) {
          if (mutation.type === 'childList') {
            // console.log('A child node has been added or removed.');
            // if the address bar requested URL has a hash link
            if (location.hash) {
              // find that hash links element id
              let scrollElement = document.querySelector(location.hash)
              // if it exists scroll it into view:
              if (scrollElement) {
                scrollElement.scrollIntoView()
                // scrollElement.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"})
              }
            }
          }
          // if (mutation.type === 'attributes') {
          //     console.log('The ' + mutation.attributeName + ' attribute was modified.');
          // }
          // else if (mutation.type === 'subtree') {
          //     console.log('The subtree was modified.');
          //     console.log(mutation);
          // }
        }
      }

      // Create an observer instance linked to the callback function
      const observer = new MutationObserver(callback)

      // Start observing the target node for configured mutations
      observer.observe(targetNode, config)
    }

    // TODO: CLEANUP
    // return cleanup = () => observer.disconnect();
  }, [myRef, location])
  return (
    <article className={styles.container}>
      <nav>
        <SideNav />
      </nav>

      <main
        ref={myRef}
        id="content"
        className={isScreenSmallerThan('md') ? styles.mainGrid : styles.mainGridWithMenuMargin}
      >
        {children}
      </main>

      <footer className={styles.footerGrid}>
        <a href={'https://perpetualsummer.ltd'}>
          <SvgImg image="perpetual-summer" />
        </a>
      </footer>
    </article>
  )
}

RootView.propTypes = {
  children: propTypes.node.isRequired,
}

export default RootView

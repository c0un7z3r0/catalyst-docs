import React, { useState } from 'react'
import IntersectionWrapper from '../common/intersectionWrapper/intersectionWrapper'
import ScrollIntoView from '../common/scrollIntoView/scrollIntoView'
import PageHeadingSplash from '../common/pageHeadingSplash/pageHeadingSplash'
import styles from './features.module'
import StTrinityCollegeLibraryImageXS from '../../public/images/splash/stTrinityCollegeLibraryXS_400.jpg'
import StTrinityCollegeLibraryImageSM from '../../public/images/splash/stTrinityCollegeLibrarySM_600.jpg'
import StTrinityCollegeLibraryImageMD from '../../public/images/splash/stTrinityCollegeLibraryMD_900.jpg'
import StTrinityCollegeLibraryImageLG from '../../public/images/splash/stTrinityCollegeLibraryLG_1200.jpg'
import StTrinityCollegeLibraryImageXL from '../../public/images/splash/stTrinityCollegeLibraryXL_1920.jpg'

import Code from '@perpetualsummer/catalyst-elements/Code'
import SectionHeading from '../common/sectionHeading/sectionHeading'
import InputText from '@perpetualsummer/catalyst-elements/InputText'
import ManchesterBee from '../common/manchesterBee/ManchesterBee'
import Button from '@perpetualsummer/catalyst-elements/Button'
/**
 * A Content component for the 'features' Page
 *
 * @component
 * @example
 * return (
 *  <Features />
 * )
 */

const Features = () => {
  const imageSet = {
    xs: { url: StTrinityCollegeLibraryImageXS },
    sm: { url: StTrinityCollegeLibraryImageSM },
    md: { url: StTrinityCollegeLibraryImageMD },
    lg: { url: StTrinityCollegeLibraryImageLG },
    xl: { url: StTrinityCollegeLibraryImageXL },
    default: { url: StTrinityCollegeLibraryImageMD },
  }

  const [inputValue, setInputValue] = useState('')

  return (
    <>
      <PageHeadingSplash imageSet={imageSet}>
        <h1>
          <em>
            <strong>Features</strong>
          </em>
        </h1>
      </PageHeadingSplash>
      <div className={styles.contentContainer}>
        <ScrollIntoView>
          <IntersectionWrapper id="Context">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Features#Context"
            >
              Context
            </SectionHeading>
            <p>
              The path of the righteous man is beset on all sides by the iniquities of the selfish
              and the tyranny of evil men. Blessed is he who, in the name of charity and good will,
              shepherds the weak through the valley of darkness, for he is truly his brother's
              keeper and the finder of lost children. And I will strike down upon thee with great
              vengeance and furious anger those who would attempt to poison and destroy My brothers.
              And you will know My name is the Lord when I lay My vengeance upon thee.
            </p>
            <InputText
              label="Text Input Label"
              value={inputValue}
              onChange={(e) => setInputValue(e.target.value)}
              icon={<ManchesterBee />}
            />
            <ManchesterBee />
            <Button>I AM A BUTTON. PRESS ME. I LIKE IT</Button>
            <h6>Viewport Context Helper</h6>
            <p>
              Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is
              advertised as the most popular gun in American crime. Do you believe that shit? It
              actually says that in the little book that comes with it: the most popular gun in
              American crime. Like they're actually proud of that shit.{' '}
            </p>

            <Code filename="./src/some/path/to/file.js" language="Javascript">
              {`
This is a bunch of text 
It has line breaks, 
+It has multiple lines of text, 
-We have this so we can try and split each line into a seperate line. 
If we can do that we can style each individual line which will make life easier.
`}
            </Code>
            <Code filename="./src/some/path/to/file.jsx" language="JSX">
              {`
This is a bunch of text 
It has line breaks, 
It has multiple lines of text, 
We have this so we can try and split each line into a seperate line. 
If we can do that we can style each individual line which will make life easier.
`}
            </Code>

            <p>
              Look, just because I don't be givin' no man a foot massage don't make it right for
              Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the
              nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll
              kill the motherfucker, know what I'm sayin'?{' '}
            </p>

            <p>
              Well, the way they make shows is, they make one show. That show's called a pilot. Then
              they show that show to the people who make shows, and on the strength of that one show
              they decide if they're going to make more shows. Some pilots get picked and become
              television programs. Some don't, become nothing. She starred in one of the ones that
              became nothing.{' '}
            </p>

            <p>
              Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is
              advertised as the most popular gun in American crime. Do you believe that shit? It
              actually says that in the little book that comes with it: the most popular gun in
              American crime. Like they're actually proud of that shit.{' '}
            </p>

            <p>
              My money's in that office, right? If she start giving me some bullshit about it ain't
              there, and we got to go someplace else and get it, I'm gonna shoot you in the head
              then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my
              goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you,
              motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in
              there, you the first motherfucker to get shot. You understand?
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
        <ScrollIntoView>
          <IntersectionWrapper id="Styles">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Features#Styles"
            >
              Styles
            </SectionHeading>
            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              Now that there is the Tec-9, a crappy spray gun from South Miami. This gun is
              advertised as the most popular gun in American crime. Do you believe that shit? It
              actually says that in the little book that comes with it: the most popular gun in
              American crime. Like they're actually proud of that shit.{' '}
            </p>

            <p>
              Do you see any Teletubbies in here? Do you see a slender plastic tag clipped to my
              shirt with my name printed on it? Do you see a little Asian child with a blank
              expression on his face sitting outside on a mechanical helicopter that shakes when you
              put quarters in it? No? Well, that's what you see at a toy store. And you must think
              you're in a toy store, because you're here shopping for an infant named Jeb.{' '}
            </p>

            <p>
              Now that we know who you are, I know who I am. I'm not a mistake! It all makes sense!
              In a comic, you know how you can tell who the arch-villain's going to be? He's the
              exact opposite of the hero. And most times they're friends, like you and me! I
              should've known way back when... You know why, David? Because of the kids. They called
              me Mr Glass.{' '}
            </p>

            <p>
              realism systemic 8-bit shoes market futurity paranoid convenience store. bicycle
              marketing smart- wristwatch market skyscraper RAF franchise. geodesic table courier
              corporation rifle courier convenience store futurity.
            </p>

            <p>
              physical spook car camera dome tattoo cardboard futurity. Tokyo narrative lights city
              geodesic shoes 8-bit pre-. girl shoes rain assault fetishism engine human franchise.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
        <ScrollIntoView>
          <IntersectionWrapper id="Redux">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Features#Redux"
            >
              Redux
            </SectionHeading>
            <p>
              human Kowloon systemic franchise order-flow tube -ware skyscraper. woman shrine
              singularity corrupted denim gang lights garage. boat Legba long-chain hydrocarbons
              tiger-team rebar corrupted garage neon.
            </p>

            <p>
              My money's in that office, right? If she start giving me some bullshit about it ain't
              there, and we got to go someplace else and get it, I'm gonna shoot you in the head
              then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my
              goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you,
              motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in
              there, you the first motherfucker to get shot. You understand?{' '}
            </p>

            <p>
              Do you see any Teletubbies in here? Do you see a slender plastic tag clipped to my
              shirt with my name printed on it? Do you see a little Asian child with a blank
              expression on his face sitting outside on a mechanical helicopter that shakes when you
              put quarters in it? No? Well, that's what you see at a toy store. And you must think
              you're in a toy store, because you're here shopping for an infant named Jeb.{' '}
            </p>

            <p>
              My money's in that office, right? If she start giving me some bullshit about it ain't
              there, and we got to go someplace else and get it, I'm gonna shoot you in the head
              then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my
              goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you,
              motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in
              there, you the first motherfucker to get shot. You understand?{' '}
            </p>

            <p>
              The path of the righteous man is beset on all sides by the iniquities of the selfish
              and the tyranny of evil men. Blessed is he who, in the name of charity and good will,
              shepherds the weak through the valley of darkness, for he is truly his brother's
              keeper and the finder of lost children. And I will strike down upon thee with great
              vengeance and furious anger those who would attempt to poison and destroy My brothers.
              And you will know My name is the Lord when I lay My vengeance upon thee.{' '}
            </p>

            <p>
              Your bones don't break, mine do. That's clear. Your cells react to bacteria and
              viruses differently than mine. You don't get sick, I do. That's also clear. But for
              some reason, you and I react the exact same way to water. We swallow it too fast, we
              choke. We get some in our lungs, we drown. However unreal it may seem, we are
              connected, you and I. We're on the same curve, just on opposite ends.{' '}
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
        <ScrollIntoView>
          <IntersectionWrapper id="Hooks">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Features#Hooks"
            >
              Hooks
            </SectionHeading>

            <p>
              Well, the way they make shows is, they make one show. That show's called a pilot. Then
              they show that show to the people who make shows, and on the strength of that one show
              they decide if they're going to make more shows. Some pilots get picked and become
              television programs. Some don't, become nothing. She starred in one of the ones that
              became nothing.{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              Look, just because I don't be givin' no man a foot massage don't make it right for
              Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the
              nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll
              kill the motherfucker, know what I'm sayin'?{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              My money's in that office, right? If she start giving me some bullshit about it ain't
              there, and we got to go someplace else and get it, I'm gonna shoot you in the head
              then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my
              goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you,
              motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in
              there, you the first motherfucker to get shot. You understand?{' '}
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
        <ScrollIntoView>
          <IntersectionWrapper id="Media">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Features#Media"
            >
              Media Types
            </SectionHeading>

            <p>Videos, Images, Fonts etc..</p>
            <p>
              Well, the way they make shows is, they make one show. That show's called a pilot. Then
              they show that show to the people who make shows, and on the strength of that one show
              they decide if they're going to make more shows. Some pilots get picked and become
              television programs. Some don't, become nothing. She starred in one of the ones that
              became nothing.{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              Look, just because I don't be givin' no man a foot massage don't make it right for
              Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the
              nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll
              kill the motherfucker, know what I'm sayin'?{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              My money's in that office, right? If she start giving me some bullshit about it ain't
              there, and we got to go someplace else and get it, I'm gonna shoot you in the head
              then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my
              goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you,
              motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in
              there, you the first motherfucker to get shot. You understand?{' '}
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
        <ScrollIntoView>
          <IntersectionWrapper id="Testing">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Features#Testing"
            >
              Testing
            </SectionHeading>

            <p>
              Well, the way they make shows is, they make one show. That show's called a pilot. Then
              they show that show to the people who make shows, and on the strength of that one show
              they decide if they're going to make more shows. Some pilots get picked and become
              television programs. Some don't, become nothing. She starred in one of the ones that
              became nothing.{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              Look, just because I don't be givin' no man a foot massage don't make it right for
              Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the
              nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll
              kill the motherfucker, know what I'm sayin'?{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              My money's in that office, right? If she start giving me some bullshit about it ain't
              there, and we got to go someplace else and get it, I'm gonna shoot you in the head
              then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my
              goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you,
              motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in
              there, you the first motherfucker to get shot. You understand?{' '}
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
        <ScrollIntoView>
          <IntersectionWrapper id="BundleSplitting">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Features#BundleSplitting"
            >
              Bundle Splitting
            </SectionHeading>

            <p>
              Well, the way they make shows is, they make one show. That show's called a pilot. Then
              they show that show to the people who make shows, and on the strength of that one show
              they decide if they're going to make more shows. Some pilots get picked and become
              television programs. Some don't, become nothing. She starred in one of the ones that
              became nothing.{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              Look, just because I don't be givin' no man a foot massage don't make it right for
              Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the
              nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll
              kill the motherfucker, know what I'm sayin'?{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              My money's in that office, right? If she start giving me some bullshit about it ain't
              there, and we got to go someplace else and get it, I'm gonna shoot you in the head
              then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my
              goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you,
              motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in
              there, you the first motherfucker to get shot. You understand?{' '}
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
        <ScrollIntoView>
          <IntersectionWrapper id="Development">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Features#Development"
            >
              Development Mode
            </SectionHeading>

            <p>
              Well, the way they make shows is, they make one show. That show's called a pilot. Then
              they show that show to the people who make shows, and on the strength of that one show
              they decide if they're going to make more shows. Some pilots get picked and become
              television programs. Some don't, become nothing. She starred in one of the ones that
              became nothing.{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              Look, just because I don't be givin' no man a foot massage don't make it right for
              Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the
              nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll
              kill the motherfucker, know what I'm sayin'?{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              My money's in that office, right? If she start giving me some bullshit about it ain't
              there, and we got to go someplace else and get it, I'm gonna shoot you in the head
              then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my
              goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you,
              motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in
              there, you the first motherfucker to get shot. You understand?{' '}
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
        <ScrollIntoView>
          <IntersectionWrapper id="Production">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Features#Production"
            >
              Production Build
            </SectionHeading>

            <p>
              Well, the way they make shows is, they make one show. That show's called a pilot. Then
              they show that show to the people who make shows, and on the strength of that one show
              they decide if they're going to make more shows. Some pilots get picked and become
              television programs. Some don't, become nothing. She starred in one of the ones that
              became nothing.{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              Look, just because I don't be givin' no man a foot massage don't make it right for
              Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the
              nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll
              kill the motherfucker, know what I'm sayin'?{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              My money's in that office, right? If she start giving me some bullshit about it ain't
              there, and we got to go someplace else and get it, I'm gonna shoot you in the head
              then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my
              goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you,
              motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in
              there, you the first motherfucker to get shot. You understand?{' '}
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
        <ScrollIntoView>
          <IntersectionWrapper id="Deployment">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/Features#Deployment"
            >
              Deployment
            </SectionHeading>

            <p>
              Well, the way they make shows is, they make one show. That show's called a pilot. Then
              they show that show to the people who make shows, and on the strength of that one show
              they decide if they're going to make more shows. Some pilots get picked and become
              television programs. Some don't, become nothing. She starred in one of the ones that
              became nothing.{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              Look, just because I don't be givin' no man a foot massage don't make it right for
              Marsellus to throw Antwone into a glass motherfuckin' house, fuckin' up the way the
              nigger talks. Motherfucker do that shit to me, he better paralyze my ass, 'cause I'll
              kill the motherfucker, know what I'm sayin'?{' '}
            </p>

            <p>
              Normally, both your asses would be dead as fucking fried chicken, but you happen to
              pull this shit while I'm in a transitional period so I don't wanna kill you, I wanna
              help you. But I can't give you this case, it don't belong to me. Besides, I've already
              been through too much shit this morning over this case to hand it over to your dumb
              ass.{' '}
            </p>

            <p>
              My money's in that office, right? If she start giving me some bullshit about it ain't
              there, and we got to go someplace else and get it, I'm gonna shoot you in the head
              then and there. Then I'm gonna shoot that bitch in the kneecaps, find out where my
              goddamn money is. She gonna tell me too. Hey, look at me when I'm talking to you,
              motherfucker. You listen: we go in there, and that nigga Winston or anybody else is in
              there, you the first motherfucker to get shot. You understand?{' '}
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
      </div>
    </>
  )
}

export default Features

import React from 'react'
import Lightbox from '@perpetualsummer/catalyst-elements/Lightbox'
import IntersectionWrapper from '../common/intersectionWrapper/intersectionWrapper'
import ScrollIntoView from '../common/scrollIntoView/scrollIntoView'
import styles from './quickStart.module'
import PageHeadingSplash from '../common/pageHeadingSplash/pageHeadingSplash'

import LegoOrangeImageXS from '../../public/images/splash/legoOrangeXS_400.jpg'
import LegoOrangeImageSM from '../../public/images/splash/legoOrangeSM_600.jpg'
import LegoOrangeImageMD from '../../public/images/splash/legoOrangeMD_900.jpg'
import LegoOrangeImageLG from '../../public/images/splash/legoOrangeLG_1200.jpg'
import LegoOrangeImageXL from '../../public/images/splash/legoOrangeXL_1920.jpg'
// import LegoOrangeImage from '../../public/images/splash/legoOrange.jpg'
import SectionHeading from '../common/sectionHeading/sectionHeading'

import CatalystScreenshot from '../../public/images/Catalyst_StartPage.jpg'
import { Link } from 'react-router-dom'
/**
 * A Content component for the 'Getting Started' Page
 *
 * @component
 * @example
 * return (
 *  <QuickStart />
 * )
 */

const QuickStart = () => {
  const imageSet = {
    xs: { url: LegoOrangeImageXS },
    sm: { url: LegoOrangeImageSM },
    md: { url: LegoOrangeImageMD },
    lg: { url: LegoOrangeImageLG },
    xl: { url: LegoOrangeImageXL },
    default: { url: LegoOrangeImageMD },
  }

  return (
    <>
      <PageHeadingSplash imageSet={imageSet}>
        <h1>
          <strong>Quick Start</strong>
        </h1>
      </PageHeadingSplash>
      <div className={styles.contentContainer}>
        <ScrollIntoView>
          <IntersectionWrapper id="Requirements">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/#Requirements"
            >
              Requirements
            </SectionHeading>
            <p>
              These instructions assume a basic understanding of git version control and your
              development system's command line.
            </p>
            <p>
              In order to install and utilise the React Catalyst development environment you will
              need to download and install the following:
            </p>
            <ul>
              <li>Git - WIN / MacOS / Linux Download</li>
              <li>Node - WIN / MacOS / Linux Download</li>
            </ul>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="Installation">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/#Installation"
            >
              Installation
            </SectionHeading>
            <p>
              First, create a working directory folder, this will be the root of you project, you
              can create a folder with the following command:
            </p>
            <code>mkdir MyProject</code>

            <p>
              Replace 'MyProject' with the directory name of your choice. We will refer to this
              directory as the root folder going forwards in this documentation.
            </p>
            <p>Navigate into your project folder with:</p>
            <code>cd MyProject</code>

            <p>Now we will use Git to download the development environment:</p>
            <code>git clone https://bitbucket.org/c0un7z3r0/react-catalyst-v2.git .</code>

            <p>
              Once that has downloaded, your root folder should have some new files and directories.
              We now need to install the dependencies of the development environment, for that we
              use the Node Package Manager (NPM) that came with your installation of Node.
            </p>
            <p>To install the dependencies, type the following:</p>
            <code>npm i</code>

            <p>
              Congratulations, your development environment is now setup and ready to start! Happy
              Coding
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>

        <ScrollIntoView>
          <IntersectionWrapper id="Usage">
            <SectionHeading
              tooltipAlignment="right"
              tooltipContent={'Copy Link To Clipboard'}
              clipboardContent="https://react-catalyst.perpetualsummer.ltd/#Usage"
            >
              Usage
            </SectionHeading>
            <p>
              React Catalyst development environment comes pre-configured with React Hot Reloading.
              This enables you to see your changes happen in real-time. To start in 'Development'
              mode simply type:
            </p>
            <code>npm start</code>

            <Lightbox image={CatalystScreenshot} float="right">
              <img className={styles.catalystStartupImage} src={CatalystScreenshot} />
            </Lightbox>
            <p>
              After a short time, the development environment will initialise and open the base
              application in your default browser. If you do not have a default browser setup you
              can navigate to it manually; the address of the Hot Reloading Devlopment environment
              is, by default: <code>localhost:3000</code>
            </p>
            <p>In your browser you should now see the start page as pictured here:</p>

            <p>
              You are now ready to begin developing your application. Click{' '}
              <Link to={'/Tutorial'}>here</Link> for a practical walk-through tutorial or read on
              for more information on the features available in React Catalyst.
            </p>
            <hr />
          </IntersectionWrapper>
        </ScrollIntoView>
      </div>
    </>
  )
}

export default QuickStart

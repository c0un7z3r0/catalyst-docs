export const OS_OPTIONS = {
  WINDOWS: 'Windows',
  MACOS: 'MacOS',
  LINUX: 'Linux',
  UNIX: 'Unix',
}

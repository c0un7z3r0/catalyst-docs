import { combineReducers } from 'redux'
import counter from './counterReducer'
import options from './optionsReducer'
import navMenu from './navMenuReducer'
import { routerReducer as routing } from 'react-router-redux'

// The name you chose for your reducer is what is reflected in your components. ie
//    courses = state.courses
//    rhubarb = state.rhubarb
// See mapStateToProps in coursesPage.js for example.

const rootReducer = combineReducers({
  counter,
  options,
  navMenu,
  routing,
})

export default rootReducer

import * as types from '../actions/actionTypes'
import initialState from './initialState'

export default function optionsReducer(state = initialState.options, action) {
  switch (action.type) {
    case types.SELECT_OS:
      return {
        selectedOS: action.selectedOS,
      }

    default:
      return state
  }
}

import * as types from '../actions/actionTypes'
import initialState from './initialState'

export default function navMenuReducer(state = initialState.navMenu, action) {
  switch (action.type) {
    case types.UPDATE_INTERSECTION_DATA: {
      return {
        ...state,
        viewportVisibility: {
          ...state.viewportVisibility,
          [action.payload.id]: {
            id: action.payload.id,
            intersectionRatio: action.payload.intersectionRatio,
            alignment: action.payload.alignment,
            percentFromTop: action.payload.percentFromTop,
            percentFromBottom: action.payload.percentFromBottom,
          },
        },
      }
    }

    case types.FLUSH_INTERSECTION_DATA:
      return {
        ...state,
        viewportVisibility: {},
      }

    default:
      return state
  }
}

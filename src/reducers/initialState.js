import { getOperatingSystem } from '../utilities/utilities'

export default {
  value: 0,
  options: {
    selectedOS: getOperatingSystem(),
  },
  navMenu: {
    viewportVisibility: {
      // object with a bunch of these:
      // {
      //   id: 'SomeNavMenuElementId',  // element Id
      //   intersectionRatio: 10,      // % its in the viewport
      //   alignment: 'top',            // or 'bottom'
      // }
    },
  },
}

import { OS_OPTIONS } from '../constants/options'
import { useContext } from 'react'
import { VIEWPORT_SIZES } from '../constants/viewport'
import { ViewportContext } from '../context/viewportContextProvider'

/**
 * Returns a string that describes the Operating System of the user
 * @returns {string} A string that describes the operating system the app is running on.
 */
export const getOperatingSystem = () => {
  let operatingSystem = 'Unknown'
  if (navigator.appVersion.indexOf('Win') != -1) operatingSystem = OS_OPTIONS.WINDOWS
  if (navigator.appVersion.indexOf('Mac') != -1) operatingSystem = OS_OPTIONS.MACOS
  if (navigator.appVersion.indexOf('X11') != -1) operatingSystem = OS_OPTIONS.UNIX
  if (navigator.appVersion.indexOf('Linux') != -1) operatingSystem = OS_OPTIONS.LINUX

  return operatingSystem
}

/**
 * Converts and return an object of objects into a array of objects
 * @param {string} objects An object of objects
 * @returns {array} An array of objects
 */
export const objectsToArray = (objects) => {
  return Object.keys(objects).map((key) => {
    return objects[key]
  })
}

/**
 * Get screen size based on the width value passed in
 * @param width Width (in pixeld) of the screen or container
 * @returns A string, one of: 'xs', 'sm', 'md', 'lg' or 'xl'
 */
export const getScreenSize = (width) => {
  switch (true) {
    case width <= 599:
      return 'xs'
    case width >= 600 && width < 900:
      return 'sm'
    case width >= 900 && width < 1200:
      return 'md'
    case width >= 1200 && width < 1800:
      return 'lg'
    case width >= 1800:
      return 'xl'
    default:
      return 'default'
  }
}

/**
 * Opens a link in a new tab.
 */
export const openInNewTab = (url) => {
  var win = window.open(url, '_blank')
  if (win != null) {
    win.focus()
  }
}

/**
 * Check to see if the current viewport is smaller than that passed into this function
 * @param screenSizeToCheck Width as a string ('xs', 'sm', 'md', 'lg' or 'xl')
 * @returns true or false
 */
const useScreenSmallerThan = (screenSizeToCheck) => {
  const currentScreenSize = useContext(ViewportContext)
  return VIEWPORT_SIZES[currentScreenSize.screenSize] < VIEWPORT_SIZES[screenSizeToCheck]
}
export { useScreenSmallerThan as isScreenSmallerThan }

/**
 * Check to see if the current viewport is larger than that passed into this function
 * @param screenSizeToCheck Width as a string ('xs', 'sm', 'md', 'lg' or 'xl')
 * @returns true or false
 */
const useScreenLargerThan = (screenSizeToCheck) => {
  const currentScreenSize = useContext(ViewportContext)
  return VIEWPORT_SIZES[currentScreenSize.screenSize] > VIEWPORT_SIZES[screenSizeToCheck]
}
export { useScreenLargerThan as isScreenLargerThan }
